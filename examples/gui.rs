use nexen::core::{Context, AppState, InitArgs, Application, event::Event};
use nexen::gui::{Button, Container, Label};
use nexen::graphics::Renderer2D;

struct MainState {}

impl AppState for MainState {
    fn on_init(&mut self, ctx: &mut Context) {
        ctx.window.set_vsync(true);
        
        let gui_entity = ctx.world().create_entity();

        let mut container = Container::new();

        let mut button = Button::new();
        button.label = "Hello".to_owned();
        button.on_pressed = || {
            println!("Button clicked!");
        };

        container.add_child(Label::new());
        container.add_child(button);

        ctx.world().add_component(gui_entity, container);
    }

    fn on_update(&mut self, _ctx: &mut Context, _delta: f32) {}

    fn on_draw(&mut self, _ctx: &mut Renderer2D) {}

    fn on_event(&mut self, _ctx: &mut Context, _event: &Event) {}
}

fn main() {
    nexen::logger::init();

    let args = InitArgs {
        app_name: "Testbed".to_string(),
        window_size: nexen::common::Size2D::new(1280, 720),
        fullscreen: false,
    };

    let mut app = Application::new(Some(args));

    app.push_state(Box::new(MainState{}));
    app.run();
}
