use nexen::core::{Context, AppState, InitArgs, Application, event::Event};
use nexen::animation::{AnimationKey, AnimationProperty, AnimationComponent, AnimationTrack};
use nexen::graphics::{TransformComponent, Text2DComponent, colors, Font, DefaultFontLoader, Renderer2D};

struct MainState;

impl AppState for MainState {
    fn on_init(&mut self, ctx: &mut Context) {
    	ctx.window.set_vsync(true);

		let font = ctx.resource_manager()
			.get_storage_for::<Font>()
			.get::<DefaultFontLoader>("res/fonts/mono.ttf")
			.unwrap();
		
		(*font.borrow_mut()).set_texture_aliasing(true);

    	let entity = ctx.world().create_entity();

		let mut text = Text2DComponent::new(font);
		
		text.text = "Hello Nexen!".to_string();
		text.color = colors::WHITE;
		text.size = 80;

    	let mut animation = AnimationComponent::new();

    	let mut track = AnimationTrack::new();

		track.set_target_type::<Text2DComponent>();
		track.property = AnimationProperty::Color;
    	track.animation_keys.push((0.0, AnimationKey::FloatColor(colors::WHITE.into())));
    	track.animation_keys.push((1.0, AnimationKey::FloatColor((1.0, 1.0, 1.0, 0.0).into())));
    	track.animation_keys.push((2.0, AnimationKey::FloatColor(colors::WHITE.into())));

		animation.length = 2.0;

		animation.add_track(track);
		animation.repeat = true;
		animation.play();

		ctx.world().add_component(entity, TransformComponent::new());
    	ctx.world().add_component(entity, text);
    	ctx.world().add_component(entity, animation);
    }

    fn on_update(&mut self, _ctx: &mut Context, _delta: f32) {
	}
	
	fn on_draw(&mut self, _ctx: &mut Renderer2D) {
		
	}

    fn on_event(&mut self, _ctx: &mut Context, _event: &Event) {

    }
}

fn main() {
    nexen::logger::init();

    let args = InitArgs {
        app_name: "Hello Nexen!".to_string(),
        window_size: nexen::common::Size2D::new(1920, 1080),
        fullscreen: false,
    };

    let mut app = Application::new(Some(args));

    app.push_state(Box::new(MainState));
    app.run();
}
