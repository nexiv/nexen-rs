use nexen::core::{Context, AppState, InitArgs, Application, event::Event};
use nexen::graphics::Renderer2D;

struct MainState;

impl AppState for MainState {
    fn on_init(&mut self, ctx: &mut Context) {
    	ctx.window.set_vsync(true);
    }

    fn on_update(&mut self, _ctx: &mut Context, _delta: f32) {
    }

    fn on_draw(&mut self, _ctx: &mut Renderer2D) {
        
    }

    fn on_event(&mut self, _ctx: &mut Context, _event: &Event) {

    }
}

fn main() {
    nexen::logger::init();

    let args = InitArgs {
        app_name: "Testbed".to_string(),
        window_size: nexen::common::Size2D::new(1280, 720),
        fullscreen: false,
    };

    let mut app = Application::new(Some(args));

    app.push_state(Box::new(MainState));
    app.run();
}
