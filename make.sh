#!/bin/bash

RUST_BACKTRACE=true
RUST_LOG=debug
DIR="$(dirname "$0")"

if cargo "$@"; then
    [ -d "$DIR/target/debug" ] && cp -r "$DIR/res" "$DIR/target/debug/examples/res"
    [ -d "$DIR/target/release" ] && cp -r "$DIR/res" "$DIR/target/release/examples/res"
fi
