/*************************************************************************/
/*  animation.rs                                                         */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::core::DataBundle;
use crate::core::event::Event;
use crate::common::FloatRect;
use crate::graphics::{
    color::FloatColor,
    Sprite2DComponent,
    Polygon2DComponent,
    Text2DComponent,
    TransformComponent
};
use crate::core::application::InitArgs;
use crate::ecs::{System, Component};
use crate::glm;
use serde::Deserialize;
use log::{error};

use std::{
    any::TypeId,
    ops::{Sub, Add, Mul},
};

//pub mod interpolation;

#[derive(Clone, Copy, Debug, PartialEq, PartialOrd)]
pub enum AnimationKey {
    Empty,
    Boolean(bool),
    Float(f32),
    FloatRect(FloatRect),
    FloatColor(FloatColor),
    Vec2(glm::Vec2),
    Int(f32),
}

#[derive(Debug, Clone, Copy, Deserialize)]
pub enum AnimationProperty {
    None,
    Scale,
    Translation,
    Color,
    Rect,
    RectIndex,
}

#[derive(Debug, Clone, Copy, Deserialize)]
pub enum AnimationInterpolation {
    Linear,
    Sine,
}

fn mismatch_error() -> AnimationKey {
    error!("Mismatched animation property");
    AnimationKey::Empty
}

impl Sub for AnimationKey {
    type Output = AnimationKey;

    fn sub(self, other: Self) -> Self {
        match self {
            AnimationKey::FloatRect(rect) => match other {
                AnimationKey::FloatRect(other_rect) => {
                    AnimationKey::FloatRect(rect - other_rect)
                },
                _ => mismatch_error(),
            },
            AnimationKey::FloatColor(color) => match other {
                AnimationKey::FloatColor(other_color) => {
                    AnimationKey::FloatColor(color - other_color)
                },
                _ => mismatch_error(),
            }
            AnimationKey::Float(float) => match other {
                AnimationKey::Float(other_float) => {
                    AnimationKey::Float(float - other_float)
                },
                _ => mismatch_error(),
            },
            AnimationKey::Vec2(vec2) => match other {
                AnimationKey::Vec2(other_vec2) => {
                    AnimationKey::Vec2(vec2 - other_vec2)
                },
                _ => mismatch_error(),
            },
            AnimationKey::Int(i) => match other {
                AnimationKey::Int(other_i) => {
                    AnimationKey::Int(i - other_i)
                },
                _ => mismatch_error(),
            },
            _ => unimplemented!(),
        }
    }
}

impl Add for AnimationKey {
    type Output = AnimationKey;

    fn add(self, other: Self) -> Self {
        match self {
            AnimationKey::FloatRect(rect) => match other {
                AnimationKey::FloatRect(other_rect) => {
                    AnimationKey::FloatRect(rect + other_rect)
                },
                _ => mismatch_error(),
            },
            AnimationKey::FloatColor(color) => match other {
                AnimationKey::FloatColor(other_color) => {
                    AnimationKey::FloatColor(color + other_color)
                },
                _ => mismatch_error(),
            }
            AnimationKey::Float(float) => match other {
                AnimationKey::Float(other_float) => {
                    AnimationKey::Float(float + other_float)
                },
                _ => mismatch_error(),
            },
            AnimationKey::Vec2(vec2) => match other {
                AnimationKey::Vec2(other_vec2) => {
                    AnimationKey::Vec2(vec2 + other_vec2)
                },
                _ => mismatch_error(),
            },
            AnimationKey::Int(i) => match other {
                AnimationKey::Int(other_i) => {
                    AnimationKey::Int(i + other_i)
                },
                _ => mismatch_error(),
            },
            _ => unimplemented!(),
        }
    }
}

impl Mul for AnimationKey {
    type Output = AnimationKey;

    fn mul(self, other: Self) -> Self {
        match self {
            AnimationKey::FloatRect(rect) => match other {
                AnimationKey::FloatRect(other_rect) => {
                    AnimationKey::FloatRect(rect * other_rect)
                },
                _ => mismatch_error(),
            },
            AnimationKey::FloatColor(color) => match other {
                AnimationKey::FloatColor(other_color) => {
                    AnimationKey::FloatColor(color * other_color)
                },
                _ => mismatch_error(),
            }
            AnimationKey::Float(float) => match other {
                AnimationKey::Float(other_float) => {
                    AnimationKey::Float(float * other_float)
                },
                _ => mismatch_error(),
            },
            AnimationKey::Vec2(vec2) => match other {
                AnimationKey::Vec2(other_vec2) => {
                    AnimationKey::Vec2(glm::vec2(vec2.x * other_vec2.x, vec2.y * other_vec2.y))
                },
                _ => mismatch_error(),
            },
            AnimationKey::Int(i) => match other {
                AnimationKey::Int(other_i) => {
                    AnimationKey::Int(i * other_i)
                },
                _ => mismatch_error(),
            },
            _ => unimplemented!(),
        }
    }
}

impl Mul<f32> for AnimationKey {
    type Output = AnimationKey;

    fn mul(self, other: f32) -> Self {
        match self {
            AnimationKey::FloatRect(rect) => {
                AnimationKey::FloatRect(rect * other)
            },
            AnimationKey::Float(float) => {
                AnimationKey::Float(float * other)
            },
            AnimationKey::FloatColor(color) => {
                AnimationKey::FloatColor(color * other)
            },
            AnimationKey::Vec2(vec2) => {
                AnimationKey::Vec2(glm::vec2(vec2.x * other, vec2.y * other))
            },
            AnimationKey::Int(i) => {
                AnimationKey::Int(i * other)
            }
            _ => unimplemented!(),
        }
    }
}

#[derive(Clone)]
pub struct AnimationTrack {
    pub target_component_type: TypeId,
    pub property: AnimationProperty,
    pub interpolation: AnimationInterpolation,
    pub animation_keys: Vec<(f32, AnimationKey)>,
    current_key: (f32, AnimationKey),
    next_key: (f32, AnimationKey),
    interpolated: f32,
}

impl AnimationTrack {
    pub fn new() -> Self {
        AnimationTrack {
            target_component_type: TypeId::of::<()>(),
            property: AnimationProperty::None,
            interpolation: AnimationInterpolation::Linear,
            animation_keys: Vec::new(),
            current_key: (0.0, AnimationKey::Empty),
            next_key: (0.0, AnimationKey::Empty),
            interpolated: 0.0,
        }
    }

    pub fn set_target_type<C: Component>(&mut self) {
        self.target_component_type = TypeId::of::<C>();
    }

    fn update(&mut self, elapsed: f32) {
        if self.animation_keys.len() == 0 {
            return;
        }
        self.current_key = self.animation_keys[0];
        self.next_key = self.animation_keys[0];

        for key in &self.animation_keys {
            if key.0 <= elapsed {
                self.current_key = *key;
                continue;
            } else {
                self.next_key = *key;
                break;
            }
        }

        match self.interpolation {
            AnimationInterpolation::Linear => {
                self.interpolated = (elapsed - self.current_key.0) /
                    (self.next_key.0 - self.current_key.0);
            },
            AnimationInterpolation::Sine => {
                self.interpolated = (elapsed - self.current_key.0) /
                    (self.next_key.0 - self.current_key.0);
                self.interpolated = self.interpolated.sin();
            }
        }
    }

    fn get_value(&self) -> AnimationKey {
        if self.animation_keys.len() == 0 {
            return AnimationKey::Empty;
        }

        if self.next_key.1 != self.current_key.1 {
            self.current_key.1 + ((self.next_key.1 - self.current_key.1) * self.interpolated)
        } else {
            self.next_key.1
        }
    }
}

pub struct AnimationComponent {
    pub tracks: Vec<AnimationTrack>,
    pub length: f32,
    pub reverse: bool,
    pub repeat: bool,
    playing: bool,
    elapsed: f32,
}

impl AnimationComponent {
    pub fn new() -> Self {
        AnimationComponent {
            tracks: Vec::new(),
            length: 0.0,
            playing: false,
            reverse: false,
            repeat: false,
            elapsed: 0.0,
        }
    }

    pub fn play(&mut self) {
        self.playing = true;
    }

    pub fn pause(&mut self) {
        self.playing = false;
    }

    pub fn stop(&mut self) {
        self.elapsed = 0.0;
        self.playing = false;
    }

    pub fn add_track(&mut self, track: AnimationTrack) {
        self.tracks.push(track);
    }

    pub fn playing(&self) -> bool {
        self.playing
    }

    pub fn set_running_time(&mut self, timestamp: f32) {
        self.elapsed = timestamp;
    }

    pub fn elapsed(&self) -> f32 {
        self.elapsed
    }

    fn update(&mut self, time: f32) {
        if self.reverse {
            self.elapsed -= time;

            if self.elapsed < 0.0 {
                if self.repeat {
                    self.elapsed = self.length;
                } else {
                    self.elapsed = 0.0;
                    self.playing = false;
                }
            }
        } else {
            self.elapsed += time;

            if self.elapsed > self.length {
                if self.repeat {
                    self.elapsed = 0.0;
                } else {
                    self.elapsed = self.length;
                    self.playing = false;
                }
            }
        }

        if self.playing {
            for track in &mut self.tracks {
                track.update(self.elapsed);
            }
        }
    }
}

impl Component for AnimationComponent {}

pub struct AnimationSystem;

impl Default for AnimationSystem {
    fn default() -> Self {
        AnimationSystem
    }
}

impl System for AnimationSystem {
    fn init(&mut self, _bundle: &mut DataBundle, _args: &InitArgs) {}

    fn update(&mut self, bundle: &DataBundle, delta: f32) {
        let world = bundle.world();
        let entities = world.entities();
        let animations = world.component_storage::<AnimationComponent>();

        for entity in entities {
            if let Some(mut animation) = animations.write(*entity) {
                if !animation.playing {
                    continue;
                }
                animation.update(delta);

                for track in &mut animation.tracks {
                    if track.target_component_type == TypeId::of::<Sprite2DComponent>() {
                        if let Some(mut sprite) = world.get_write_component::<Sprite2DComponent>(*entity) {
                            match track.property {
                                AnimationProperty::Rect => match track.get_value() {
                                    AnimationKey::FloatRect(rect) => {
                                        sprite.rect = rect;
                                    },
                                    _ => unimplemented!(),
                                }
                                AnimationProperty::RectIndex => match track.get_value() {
                                    AnimationKey::Int(i) => {
                                        sprite.rect_index = i as u32;
                                    },
                                    _ => unimplemented!(),
                                }
                                AnimationProperty::Color => match track.get_value() {
                                    AnimationKey::FloatColor(fcolor) => {
                                        sprite.color = fcolor.into();
                                    }
                                    _ => unimplemented!(),
                                }
                                _ => unimplemented!(),
                            }
                        }
                    }
                    if track.target_component_type == TypeId::of::<Polygon2DComponent>() {
                        if let Some(mut polygon) = world.get_write_component::<Polygon2DComponent>(*entity) {
                            match track.property {
                                AnimationProperty::Color => match track.get_value() {
                                    AnimationKey::FloatColor(fcolor) => {
                                        polygon.color = fcolor.into();
                                        polygon.border_color = fcolor.into();
                                    }
                                    _ => unimplemented!(),
                                }
                                _ => unimplemented!(),
                            }
                        }
                    }
                    if track.target_component_type == TypeId::of::<Text2DComponent>() {
                        if let Some(mut text) = world.get_write_component::<Text2DComponent>(*entity) {
                            match track.property {
                                AnimationProperty::Color => match track.get_value() {
                                    AnimationKey::FloatColor(fcolor) => {
                                        text.color = fcolor.into();
                                    },
                                    _ => unimplemented!(),
                                },
                                _ => unimplemented!(),
                            }
                        }
                    }
                    if track.target_component_type == TypeId::of::<TransformComponent>() {
                        if let Some(mut transform) = world.get_write_component::<TransformComponent>(*entity) {
                            match track.property {
                                AnimationProperty::Scale => match track.get_value() {
                                    AnimationKey::Vec2(scale) => {
                                        transform.set_scale(scale);
                                    },
                                    _ => unimplemented!(),
                                },
                                AnimationProperty::Translation => match track.get_value() {
                                    AnimationKey::Vec2(pos) => {
                                        transform.set_translation(pos);
                                    },
                                    _ => unimplemented!(),
                                },
                                _ => unimplemented!(),
                            }
                        }
                    }
                }
            }
        }
    }

    fn handle_events(&mut self, _bundle: &mut DataBundle, _event: &Event) {}
}

