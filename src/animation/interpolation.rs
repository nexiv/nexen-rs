/*************************************************************************/
/*  interpolation.rs                                                     */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
/*use std::{
    ops::{Sub, Add, Mul},
};*/

pub enum Interpolation<T: InterpolationPrimitive> {
    Linear,
    Function(fn(f32, &[(f32, T)], bool) -> T),
}

impl<T: InterpolationPrimitive> Interpolation<T> {
    fn _interpolate(&self, time: f32, keyframes: &[(f32, T)], normalize: bool) -> T {
        match self {
            Interpolation::Linear => _linear_interpolate(time, keyframes, normalize),
            Interpolation::Function(func) => func(time, keyframes, normalize),
        }
    }
}

fn _linear_interpolate<T: InterpolationPrimitive>(time: f32, keyframes: &[(f32, T)], _normalize: bool) -> T {
    let mut current_key = (0.0, T::default());
    let mut next_key = (0.0, T::default());

    for key in keyframes {
        if key.0 <= time {
            current_key = key.clone();
            continue;
        } else {
            next_key = key.clone();
            break;
        }
    }

    let d = (time - current_key.0) / (next_key.0 - current_key.0);
    if next_key.1 != current_key.1 {
        current_key.1.add(&((next_key.1.sub(&current_key.1)).mul(d)))
    } else {
        next_key.1
    }
}

pub trait InterpolationPrimitive: Sized + Clone + PartialOrd + Default {
    fn add(&self, other: &Self) -> Self;
    fn sub(&self, other: &Self) -> Self;
    fn mul(&self, scalar: f32) -> Self;
}