/*************************************************************************/
/*  audio_system.rs                                                      */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::core::application::InitArgs;
use crate::core::event::Event;
use crate::core::DataBundle;
use crate::ecs::{System, AsyncSystem};
use super::sound_source::{SoundSourceComponent, SoundSourceStatus};
//use rodio::Device;
use rodio::decoder::Decoder;
use std::io::BufReader;

pub struct AudioSystem {
    //device: Device,
}

impl Default for AudioSystem {
    fn default() -> Self {
        AudioSystem {
            //device: rodio::Host::default_output_device().unwrap(),
        }
    }
}

impl System for AudioSystem {
    fn init(&mut self, _bundle: &mut DataBundle, _args: &InitArgs) {}

    fn update(&mut self, bundle: &DataBundle, _delta: f32) {
        let sound_sources = bundle.world().component_storage::<SoundSourceComponent>();
        for sound_source in &mut *sound_sources.data_mut() {
            if sound_source.sink.is_none() {
                sound_source.sink = Some(rodio::Sink::new_idle().0);
            }
            if sound_source.status == SoundSourceStatus::Playing &&
                sound_source.previous_status == SoundSourceStatus::Stopped {
                let file_handle = sound_source.stream.borrow().file_handle.try_clone().unwrap();
                sound_source.sink.as_ref().unwrap().append(
                    Decoder::new(BufReader::new(file_handle)).unwrap()
                );
                sound_source.sink.as_ref().unwrap().play();
                sound_source.previous_status = SoundSourceStatus::Playing;
            }
        }
    }

    fn handle_events(&mut self, _bundle: &mut DataBundle, _event: &Event) {}
}

impl AsyncSystem for AudioSystem {}
