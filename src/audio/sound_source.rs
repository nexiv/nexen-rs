/*************************************************************************/
/*  sound_source.rs                                                      */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use super::sound_stream::SoundStream;
use crate::ecs::Component;
use std::sync::Arc;
use atomic_refcell::AtomicRefCell;

#[derive(PartialEq, Eq)]
pub enum SoundSourceStatus {
    Stopped,
    Paused,
    Playing,
}

impl Component for SoundSourceComponent {}

pub struct SoundSourceComponent {
    pub stream: Arc<AtomicRefCell<SoundStream>>,
    pub status: SoundSourceStatus,
    pub previous_status: SoundSourceStatus,
    pub sink: Option<rodio::Sink>,
}

impl SoundSourceComponent {
    pub fn new(stream: Arc<AtomicRefCell<SoundStream>>) -> Self {
        SoundSourceComponent {
            stream,
            status: SoundSourceStatus::Stopped,
            previous_status: SoundSourceStatus::Stopped,
            sink: None,
        }
    }
}
