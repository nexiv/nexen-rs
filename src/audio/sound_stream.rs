/*************************************************************************/
/*  sound_stream.rs                                                      */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::resources::{ResourceLoader, Resource};

use std::fs::File;

pub struct SoundStream {
    pub file_handle: File,
}

impl Resource for SoundStream {}

impl SoundStream {
    fn from_file(path: &str) -> Self {
        let file_handle = File::open(path).unwrap();
        SoundStream {
            file_handle,
        }
    }
}

#[derive(Default)]
pub struct DefaultSoundStreamLoader;

impl ResourceLoader<SoundStream> for DefaultSoundStreamLoader {
    fn from_file(&self, path: &str) -> SoundStream {
        SoundStream::from_file(&path)
    }
} 
