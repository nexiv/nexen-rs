/*************************************************************************/
/*  common.rs                                                            */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use std::ops::{Add, Mul, Sub};

pub type Point2D = glm::I32Vec2;
pub type Size2D = glm::U32Vec2;

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Rect {
    pub x: i32,
    pub y: i32,
    pub w: u32,
    pub h: u32,
}

impl Rect {
    pub fn new(x: i32, y: i32, w: u32, h: u32) -> Self {
        Rect { x, y, w, h }
    }

    pub fn point(&self) -> Point2D {
        Point2D::new(self.x, self.y)
    }

    pub fn size(&self) -> Size2D {
        Size2D::new(self.w, self.h)
    }
}

impl From<(i32, i32, u32, u32)> for Rect {
    fn from((x, y, w, h): (i32, i32, u32, u32)) -> Rect {
        Rect { x, y, w, h }
    }
}

impl Add for Rect {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Rect {
            x: self.x + other.x,
            y: self.y + other.y,
            w: self.w + other.w,
            h: self.h + other.h,
        }
    }
}

impl Sub for Rect {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Rect {
            x: self.x - other.x,
            y: self.y - other.y,
            w: self.w - other.w,
            h: self.h - other.h,
        }
    }
}

impl Mul for Rect {
    type Output = Self;

    fn mul(self, other: Self) -> Self {
        Rect {
            x: self.x * other.x,
            y: self.y * other.y,
            w: self.w * other.w,
            h: self.h * other.h,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, serde::Deserialize)]
pub struct FloatRect {
    pub x: f32,
    pub y: f32,
    pub w: f32,
    pub h: f32,
}

impl FloatRect {
    pub fn new(x: f32, y: f32, w: f32, h: f32) -> Self {
        FloatRect { x, y, w, h }
    }

    pub fn zero() -> Self {
        FloatRect {
            x: 0.0,
            y: 0.0,
            w: 0.0,
            h: 0.0,
        }
    }

    pub fn point(&self) -> glm::Vec2 {
        glm::Vec2::new(self.x, self.y)
    }

    pub fn size(&self) -> glm::Vec2 {
        glm::Vec2::new(self.w, self.h)
    }

    pub fn round(&mut self) {
        self.x = self.x.round();
        self.y = self.y.round();
        self.w = self.w.round();
        self.h = self.h.round();
    }

    pub fn intersects(&self, other: &FloatRect) -> bool {
        !(other.x + other.w < self.x ||
        self.x + self.w < other.x ||
        other.y + other.h < self.y ||
        self.y + self.h < other.y)
    }

    pub fn contains(&self, other: &FloatRect) -> bool {
        other.x < self.x + self.w && other.y < self.y + self.h &&
        other.x < self.x + self.w && other.y + other.h > self.y &&
        other.x + other.w > self.x && other.y + other.h > self.y &&
        other.x + other.w > self.x && other.y < self.y + self.h
    }
}

impl From<(f32, f32, f32, f32)> for FloatRect {
    fn from((x, y, w, h): (f32, f32, f32, f32)) -> FloatRect {
        FloatRect { x, y, w, h }
    }
}

impl Add for FloatRect {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        FloatRect {
            x: self.x + other.x,
            y: self.y + other.y,
            w: self.w + other.w,
            h: self.h + other.h,
        }
    }
}

impl Sub for FloatRect {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        FloatRect {
            x: self.x - other.x,
            y: self.y - other.y,
            w: self.w - other.w,
            h: self.h - other.h,
        }
    }
}

impl Mul for FloatRect {
    type Output = Self;

    fn mul(self, other: Self) -> Self {
        FloatRect {
            x: self.x * other.x,
            y: self.y * other.y,
            w: self.w * other.w,
            h: self.h * other.h,
        }
    }
}

impl Mul<f32> for FloatRect {
    type Output = Self;

    fn mul(self, other: f32) -> Self {
        FloatRect {
            x: self.x * other,
            y: self.y * other,
            w: self.w * other,
            h: self.h * other,
        }
    }
}
