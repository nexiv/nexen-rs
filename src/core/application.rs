/*************************************************************************/
/*  application.rs                                                       */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::common::Size2D;
use super::context::{Context, ContextBuilder};
use super::event::Event;
use crate::physics::PhysicsWorld;
use crate::graphics::Renderer2D;

pub struct InitArgs {
    pub app_name: String,
    pub window_size: Size2D,
    pub fullscreen: bool,
}

impl Default for InitArgs {
    fn default() -> Self {
        InitArgs {
            app_name: "Sun".to_string(),
            window_size: Size2D::new(800, 600),
            fullscreen: false,
        }
    }
}

pub struct Application {
    context: Context,
    states: Vec<Box<dyn AppState>>,
}

impl Application {
    pub fn new(args: Option<InitArgs>) -> Self {
        let context = if let Some(args) = args {
            ContextBuilder::from_args(args).build()
        } else {
            ContextBuilder::new().build()
        };
        Application {
            context,
            states: Vec::new(),
        }
    }

    pub fn push_state(&mut self, state: Box<dyn AppState>) {
        self.states.push(state);
    }

    pub fn run(&mut self) {
        if self.states.len() == 0{
            return;
        }

        while !self.context.window.exit_request() && self.states.len() > 0 {
            let mut state = self.states.pop().unwrap();

            let dt : f32 = 1.0 / 60.0;
            let mut current_time = std::time::Instant::now();
            let mut accumulator = 0.0;

            state.on_init(&mut self.context);

            while !state.finish() && !self.context.window.exit_request() {
                let new_time = std::time::Instant::now();
                let frame_time = new_time.duration_since(current_time);
                current_time = new_time;
                accumulator += frame_time.as_secs_f32();

                let mut events = self.context.window.poll_events();
                events.append(&mut self.context.system_mut::<PhysicsWorld>().poll_events());

                for event in events {
                    state.on_event(&mut self.context, &event);
                    self.context.handle_events(&event);
                }

                while accumulator >= dt {
                    self.context.process(frame_time.as_secs_f32());
                    state.on_update(&mut self.context, frame_time.as_secs_f32());

                    accumulator -= dt;
                }

                self.context.render();
                state.on_draw(&mut self.context.renderer);
                self.context.display();
                self.context.window.swap_buffers();
                //println!("exit request {}", self.context.window.exit_request());
            }
        }
    }
}

pub trait AppState {
    fn on_init(&mut self, ctx: &mut Context);

    fn on_update(&mut self, ctx: &mut Context, delta: f32);

    fn on_draw(&mut self, ctx: &mut Renderer2D);

    fn on_event(&mut self, ctx: &mut Context, event: &Event);

    fn finish(&self) -> bool { false }
}
