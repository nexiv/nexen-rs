/*************************************************************************/
/*  context.rs                                                           */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use super::application::InitArgs;
use super::event::Event;
use super::DataBundle;
use crate::ecs::{System, system::StageScheduler, World};
use crate::animation::{AnimationSystem, AnimationComponent};
use crate::platform::window::SDLWindow;
use crate::resources::ResourceManager;
use crate::audio::{
    AudioSystem,
    SoundSourceComponent,
};
use crate::graphics::{
    Renderer2D,
    Camera2DComponent,
    Parallax2DComponent,
    SpriteBatch2DComponent,
    Sprite2DComponent,
    Polygon2DComponent,
    Text2DComponent,
    TransformComponent,
    TransformSystem,
    systems::{Sprite2DSystem, Polygon2DSystem, Camera2DSystem, Text2DSystem},
};
use crate::physics::{
    PhysicsWorld,
    RigidBodyComponent,
    JointComponent,
    RaycastComponent,
};
use crate::script::{
    ScriptContext,
    ScriptComponent,
};
use crate::gui::{GUISystem, Button, Container};
use crate::time::{TimerComponent, TimerSystem};

use std::collections::HashMap;
use std::any::{Any, TypeId};

pub struct Context {
    pub window: SDLWindow,
    pub bundle: DataBundle,
    pub renderer: Renderer2D,
    pub systems: HashMap<TypeId, Box<dyn Any>>,
    staged_systems: StageScheduler,

    last_frame_delta: f32,
}

impl Context {
    pub fn init(&mut self, args: &InitArgs) {
        if args.fullscreen {
            self.window.set_fullscreen(true);
        }

        let mut camera_system = Camera2DSystem::default();
        camera_system.init(&mut self.bundle, args);
        let mut script_context = ScriptContext::default();
        script_context.init(&mut self.bundle, args);

        self.staged_systems.add_system_to("SCRIPT_UPDATE", script_context);
        self.staged_systems.add_system_to("ANIM_UPDATE", AnimationSystem::default());
        self.staged_systems.add_async_system_to("BASE_UPDATE", TimerSystem::default());
        self.staged_systems.add_async_system_to("BASE_UPDATE", AudioSystem::default());
        //self.staged_systems.add_system_to("BASE_UPDATE", PhysicsWorld::default());
        self.register_system(PhysicsWorld::default());
        self.staged_systems.add_system_to("TRANSFORM_UPDATE", TransformSystem::default());
        self.staged_systems.add_async_system_to("GFX_UPDATE", camera_system);
        self.staged_systems.add_async_system_to("GFX_UPDATE", Sprite2DSystem::default());
        self.staged_systems.add_async_system_to("GFX_UPDATE", Polygon2DSystem::default());
        self.staged_systems.add_async_system_to("GFX_UPDATE", Text2DSystem::default());
        self.staged_systems.add_async_system_to("GFX_UPDATE", GUISystem::default());

        self.renderer.init(&mut self.bundle, &args);

        let mut world = self.bundle.world.borrow_mut();

        world.register::<TimerComponent>();
        world.register::<Camera2DComponent>();
        world.register::<Parallax2DComponent>();
        world.register::<Sprite2DComponent>();
        world.register::<SpriteBatch2DComponent>();
        world.register::<Polygon2DComponent>();
        world.register::<Text2DComponent>();
        world.register::<TransformComponent>();
        world.register::<RigidBodyComponent>();
        world.register::<JointComponent>();
        world.register::<RaycastComponent>();
        world.register::<SoundSourceComponent>();
        world.register::<AnimationComponent>();
        world.register::<ScriptComponent>();
        world.register::<Button>();
        world.register::<Container>();
    }

    pub fn handle_events(&mut self, event: &Event) {
        //self.systems.get_mut(&TypeId::of::<GUISystem>()).unwrap().downcast_mut::<GUISystem>().unwrap().handle_events(&mut self.bundle, event);
        self.staged_systems.run_event_stage("SCRIPT_UPDATE", &mut self.bundle, event);

        #[cfg(debug_assertions)] {
            self.staged_systems.run_event_stage("GFX_UPDATE", &mut self.bundle, event);
            self.systems.get_mut(&TypeId::of::<PhysicsWorld>()).unwrap().downcast_mut::<PhysicsWorld>().unwrap().handle_events(&mut self.bundle, event);
        }
    }

    pub fn process(&mut self, delta: f32) {
        self.last_frame_delta = delta;
        self.staged_systems.run_stage("SCRIPT_UPDATE", &mut self.bundle, delta);
        self.staged_systems.run_stage("ANIM_UPDATE", &mut self.bundle, delta);
        self.staged_systems.run_async_stage("BASE_UPDATE", &mut self.bundle, delta);
        self.staged_systems.run_stage("TRANSFORM_UPDATE", &mut self.bundle, delta);
        self.systems.get_mut(&TypeId::of::<PhysicsWorld>()).unwrap().downcast_mut::<PhysicsWorld>().unwrap().update(&mut self.bundle, delta); // Base update
        self.staged_systems.run_async_stage("GFX_UPDATE", &mut self.bundle, delta);
    }

    pub fn render(&mut self) {
        #[cfg(feature = "profile")]
        let mut profiler = crate::profile::Profiler::new("RENDER");

        self.staged_systems.run_render_stage("GFX_UPDATE", &self.bundle, &mut self.renderer);
        self.staged_systems.run_render_stage("SCRIPT_UPDATE", &self.bundle, &mut self.renderer);
        #[cfg(debug_assertions)]
        self.systems.get_mut(&TypeId::of::<PhysicsWorld>()).unwrap().downcast_mut::<PhysicsWorld>().unwrap().render(&mut self.bundle, &mut self.renderer);

        #[cfg(feature = "profile")]
        profiler.stamp();
    }

    pub fn display(&mut self) {
        #[cfg(feature = "profile")]
        let mut profiler = crate::profile::Profiler::new("RENDER_DISPLAY");

        self.renderer.render();

        #[cfg(feature = "profile")]
        profiler.stamp();
    }

    pub fn register_system<T: 'static + System>(&mut self, system: T) {
        self.systems.insert(TypeId::of::<T>(), Box::new(system));
    }

    pub fn system_mut<T: 'static + System>(&mut self) -> &mut T {
        self.systems.get_mut(&TypeId::of::<T>()).unwrap().downcast_mut::<T>().unwrap()
    }

    pub fn world(&mut self) -> atomic_refcell::AtomicRefMut<World> {
        self.bundle.world_mut()
    }

    pub fn resource_manager(&mut self) -> atomic_refcell::AtomicRefMut<ResourceManager> {
        self.bundle.resource_manager_mut()
    }

    pub fn bundle(&mut self) -> &mut DataBundle {
        &mut self.bundle
    }

    pub fn fps(&self) -> f32 {
        1.0 / self.last_frame_delta
    }
}

pub struct ContextBuilder {
    init_args: InitArgs,
}

impl ContextBuilder {
    pub fn new() -> Self {
        ContextBuilder{ init_args: Default::default() }
    }

    pub fn from_args(init_args: InitArgs) -> Self {
        ContextBuilder{ init_args }
    }

    pub fn with_args(&mut self, args: InitArgs) -> &mut Self {
        self.init_args = args;
        self
    }

    pub fn build(&self) -> Context {
        let mut context = Context {
            window: SDLWindow::new(
                self.init_args.app_name.as_str(),
                self.init_args.window_size,
            ),
            systems: HashMap::new(),
            staged_systems: StageScheduler::new(),
            bundle: DataBundle {
                world: std::sync::Arc::new(atomic_refcell::AtomicRefCell::new(World::new())),
                resource_manager: std::sync::Arc::new(atomic_refcell::AtomicRefCell::new(ResourceManager::new())),
            },
            renderer: Renderer2D::new(),
            last_frame_delta: 0.0,
        };
        context.init(&self.init_args);
        context
    }
}
