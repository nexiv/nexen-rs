/*************************************************************************/
/*  data_bundle.rs                                                       */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::ecs::World;
use crate::resources::ResourceManager;

use std::sync::Arc;
use atomic_refcell::{AtomicRefCell, AtomicRef, AtomicRefMut};

pub struct DataBundle {
    pub world: Arc<AtomicRefCell<World>>,
    pub resource_manager: Arc<AtomicRefCell<ResourceManager>>,
}

impl DataBundle {
    pub fn world_shared(&self) -> Arc<AtomicRefCell<World>> {
        self.world.clone()
    }

    pub fn world(&self) -> AtomicRef<World> {
        self.world.borrow()
    }

    pub fn world_mut(&self) -> AtomicRefMut<World> {
        self.world.borrow_mut()
    }

    pub fn resource_manager(&self) -> AtomicRef<ResourceManager> {
        self.resource_manager.borrow()
    }

    pub fn resource_manager_mut(&self) -> AtomicRefMut<ResourceManager> {
        self.resource_manager.borrow_mut()
    }
}
