/*************************************************************************/
/*  event.rs                                                             */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::platform::WindowEvent;
use crate::platform::joystick::JoystickEvent;
use crate::physics::PhysicsEvent;

#[derive(Debug, Clone)]
pub enum Event {
    None,
    Window(WindowEvent),
    Physics(PhysicsEvent),
    Joystick(JoystickEvent),
}

impl rlua::UserData for Event {
    fn add_methods<'lua, M: rlua::UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method("type", |_, this, ()| {
            match this {
                Event::Window(_) => Ok("Window".to_string()),
                Event::Physics(_) => Ok("Physics".to_string()),
                Event::Joystick(_) => Ok("Joystick".to_string()),
                Event::None => Ok("Undefined".to_string())
            }
        });

        methods.add_method("window", |_, this, ()| {
            match this {
                Event::Window(window_event) => Ok(window_event.clone()),
                Event::Physics(_) => Err(rlua::Error::UserDataTypeMismatch),
                Event::Joystick(_) => Err(rlua::Error::UserDataTypeMismatch),
                Event::None => Err(rlua::Error::UserDataTypeMismatch)
            }
        });

        methods.add_method("physics", |_, this, ()| {
            match this {
                Event::Window(_) => Err(rlua::Error::UserDataTypeMismatch),
                Event::Physics(physics_event) => Ok(physics_event.clone()),
                Event::Joystick(_) => Err(rlua::Error::UserDataTypeMismatch),
                Event::None => Err(rlua::Error::UserDataTypeMismatch)
            }
        });

        methods.add_method("joystick", |_, this, ()| {
            match this {
                Event::Window(_) => Err(rlua::Error::UserDataTypeMismatch),
                Event::Physics(_) => Err(rlua::Error::UserDataTypeMismatch),
                Event::Joystick(joystick_event) => Ok(joystick_event.clone()),
                Event::None => Err(rlua::Error::UserDataTypeMismatch)
            }
        });
    }
}