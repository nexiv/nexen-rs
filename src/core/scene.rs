/*************************************************************************/
/*  scene.rs                                                             */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use super::data_bundle::DataBundle;
use crate::animation::{
    AnimationInterpolation,
    AnimationComponent,
    AnimationKey,
    AnimationProperty,
    AnimationTrack
};
use crate::audio::{
    DefaultSoundStreamLoader,
    SoundSourceComponent,
    //SoundSourceStatus,
    SoundStream,
};
use crate::common::FloatRect;
use crate::ecs::{Entity, components::NavigationComponent};
use crate::graphics::{
    camera2d::CameraMode, Camera2DComponent, Parallax2DComponent, Polygon2DComponent,
    Sprite2DComponent, SpriteBatch2DComponent, SpriteBatch2DRect, Text2DComponent,
    TransformComponent,
};
use crate::graphics::{
    color::FloatColor, Color, DefaultFontLoader, DefaultTextureLoader, Font, Polygon2DShape,
    Shader, ShaderProgram, ShaderType, Texture,
};
use crate::physics::{
    RaycastComponent,
    RigidBodyComponent,
    RigidBodyType,
    JointComponent,
    JointType,
};
use crate::script::ScriptComponent;
use crate::time::TimerComponent;
use atomic_refcell::AtomicRefCell;
use ron::de::{from_reader, from_str};
use serde::Deserialize;
use std::fs::File;
use std::sync::Arc;

#[derive(Debug, Clone, Copy, Deserialize)]
pub enum RONAnimationKey {
    Empty,
    Boolean(bool),
    Float(f32),
    FloatRect(FloatRect),
    FloatColor(FloatColor),
    Vec2((f32, f32)),
    Int(i32),
}

#[derive(Debug, Clone, Deserialize)]
pub struct RONAnimationTrack {
    pub target_component_type: String,
    pub property: AnimationProperty,
    pub interpolation: Option<AnimationInterpolation>,
    pub animation_keys: Vec<(f32, RONAnimationKey)>,
}

#[derive(Debug, Deserialize, PartialEq)]
enum RONComponentType {
    Text2DComponent,
    Sprite2DComponent,
    SpriteBatch2DComponent,
    Camera2DComponent,
    Parallax2DComponent,
    RigidBodyComponent,
    JointComponent,
    RaycastComponent,
    Polygon2DComponent,
    TransformComponent,
    ScriptComponent,
    AnimationComponent,
    SoundSourceComponent,
    TimerComponent,
    NavigationComponent,
}

#[derive(Debug, Deserialize)]
struct RONComponent {
    component_type: RONComponentType,

    inherit_parent_rotation: Option<bool>,
    inherit_parent_scale: Option<bool>,
    translation: Option<(f32, f32)>,
    scale: Option<(f32, f32)>,
    z_index: Option<i16>,

    rect: Option<FloatRect>,
    spritesheet_columns: Option<u32>,
    rects: Option<Vec<SpriteBatch2DRect>>,
    color: Option<Color>,
    border_color: Option<Color>,
    texture: Option<String>,
    border: Option<f32>,

    font: Option<String>,
    text: Option<String>,
    path: Option<String>,

    nav_path: Option<Vec<f32>>,

    shader: Option<String>,
    layer: Option<i8>,
    factor: Option<(f32, f32)>,

    // RigidBody
    mesh: Option<Vec<f32>>,
    body_type: Option<RigidBodyType>,
    friction: Option<f32>,
    mass: Option<f32>,
    angular_inertia: Option<f32>,
    max_angular_velocity: Option<f32>,
    max_linear_velocity: Option<f32>,
    linear_velocity: Option<(f32, f32)>,
    angular_velocity: Option<f32>,
    enable_ccd: Option<bool>,
    sensor: Option<bool>,
    one_way_collision: Option<bool>,
    passive: Option<bool>,

    // Joint
    joint_type: Option<JointType>,
    limit: Option<f32>,

    // Raycast
    direction: Option<(f32, f32)>,

    // Animation
    animation_tracks: Option<Vec<RONAnimationTrack>>,
    length: Option<f32>,
    reverse: Option<bool>,
    repeat: Option<bool>,
    autoplay: Option<bool>,

    // Camera
    mode: Option<CameraMode>,
    follow_active_range: Option<(f32, f32)>,
    round_camera_coords: Option<bool>,
    active: Option<bool>,
    layers: Option<std::ops::Range<i8>>,
    limits: Option<FloatRect>,
    follow_acceleration: Option<(f32, f32)>,

    // SoundSource
    stream: Option<String>,
}

impl RONComponent {
    fn build(&self, entity: Entity, bundle: &mut DataBundle) -> Result<(), String> {
        match self.component_type {
            RONComponentType::TimerComponent => {
                let mut timer_component = TimerComponent::new();

                if let Some(length) = self.length {
                    timer_component.length = length;
                }

                bundle.world_mut().add_component(entity, timer_component);
            }
            RONComponentType::Text2DComponent => {
                if let Some(ron_font) = &self.font {
                    let font = bundle
                        .resource_manager_mut()
                        .get_storage_for::<Font>()
                        .get::<DefaultFontLoader>(ron_font.as_str())
                        .unwrap();
                    let mut text_component = Text2DComponent::new(font);

                    if let Some(text) = &self.text {
                        text_component.text = text.clone();
                    }

                    if let Some(z_index) = &self.z_index {
                        text_component.z_index = *z_index;
                    }

                    bundle.world_mut().add_component(entity, text_component);
                } else {
                    return Err("Missing Font property in Text2DComponent".to_string());
                }
            }
            RONComponentType::Sprite2DComponent => {
                if let Some(ron_texture) = &self.texture {
                    let texture = bundle
                        .resource_manager_mut()
                        .get_storage_for::<Texture>()
                        .get::<DefaultTextureLoader>(ron_texture.as_str())
                        .unwrap();
                    let mut sprite = Sprite2DComponent::new(texture);

                    if let Some(shader_path) = &self.shader {
                        let frag_shader = Shader::from_path(
                            format!("{}{}", shader_path, ".frag").as_str(),
                            ShaderType::Fragment,
                        )?;
                        let vert_shader = Shader::from_path(
                            format!("{}{}", shader_path, ".vert").as_str(),
                            ShaderType::Vertex,
                        )?;

                        sprite.shader = Some(Arc::new(AtomicRefCell::new(
                            ShaderProgram::from_shaders(&[frag_shader, vert_shader])?,
                        )));
                    }

                    if let Some(rect) = self.rect {
                        sprite.rect = rect;
                    }

                    if let Some(color) = self.color {
                        sprite.color = color;
                    }

                    if let Some(z_index) = self.z_index {
                        sprite.z_index = z_index;
                    }

                    if let Some(ron_layer) = self.layer {
                        sprite.layer = ron_layer;
                    }

                    if let Some(ron_columns) = self.spritesheet_columns {
                        sprite.spritesheet_columns = ron_columns;
                    }

                    bundle.world_mut().add_component(entity, sprite);
                } else {
                    return Err("Missing Texture property in Sprite2DComponent".to_string());
                }
            }
            RONComponentType::SpriteBatch2DComponent => {
                if let Some(ron_texture) = &self.texture {
                    let texture = bundle
                        .resource_manager_mut()
                        .get_storage_for::<Texture>()
                        .get::<DefaultTextureLoader>(ron_texture.as_str())
                        .unwrap();
                    let mut sprite_batch = SpriteBatch2DComponent::new(texture);

                    if let Some(rects) = &self.rects {
                        sprite_batch.rects = Arc::new(rects.clone());
                    }

                    if let Some(color) = self.color {
                        sprite_batch.color = color;
                    }

                    if let Some(z_index) = self.z_index {
                        sprite_batch.z_index = z_index;
                    }

                    if let Some(layer) = self.layer {
                        sprite_batch.layer = layer;
                    }

                    bundle.world_mut().add_component(entity, sprite_batch);
                } else {
                    return Err("Missing Texture property in SpriteBatch2DComponent".to_string());
                }
            }
            RONComponentType::Camera2DComponent => {
                let mut camera2d = Camera2DComponent::new();

                if let Some(ron_mode) = self.mode {
                    camera2d.mode = ron_mode;
                }

                if let Some(ron_round_coords) = self.round_camera_coords {
                    camera2d.round_camera_coords = ron_round_coords;
                }

                if let Some(ron_follow_accell) = self.follow_acceleration {
                    camera2d.follow_acceleration =
                        glm::vec2(ron_follow_accell.0, ron_follow_accell.1);
                }

                if let Some(active) = self.active {
                    camera2d.active = active;
                }

                if let Some(layers) = &self.layers {
                    camera2d.layers = layers.clone();
                }

                if let Some(limits) = self.limits {
                    camera2d.limits = Some(limits);
                }
                if let Some(follow_active_range) = self.follow_active_range {
                    camera2d.follow_active_range = glm::vec2(follow_active_range.0, follow_active_range.1);
                }

                bundle.world_mut().add_component(entity, camera2d);
            }
            RONComponentType::Parallax2DComponent => {
                let mut parallax = Parallax2DComponent::new();

                if let Some(layer) = self.layer {
                    parallax.layer = layer;
                }

                if let Some(factor) = self.factor {
                    parallax.factor = glm::vec2(factor.0, factor.1);
                }

                bundle.world_mut().add_component(entity, parallax);
            }
            RONComponentType::AnimationComponent => {
                let mut animation = AnimationComponent::new();

                if let Some(ron_tracks) = &self.animation_tracks {
                    for ron_track in ron_tracks {
                        let mut track = AnimationTrack::new();

                        track.property = ron_track.property;
                        if let Some(interpolation) = ron_track.interpolation {
                            track.interpolation = interpolation;
                        }
                        track.target_component_type = match ron_track.target_component_type.as_str()
                        {
                            "Sprite2D" => std::any::TypeId::of::<Sprite2DComponent>(),
                            "Transform" => std::any::TypeId::of::<TransformComponent>(),
                            "Polygon2D" => std::any::TypeId::of::<Polygon2DComponent>(),
                            _ => {
                                use log::warn;
                                warn!("RONComponent: Undefined target_component_type in AnimationComponent");
                                std::any::TypeId::of::<()>()
                            }
                        };

                        for key in &ron_track.animation_keys {
                            let val = match key.1 {
                                RONAnimationKey::Empty => AnimationKey::Empty,
                                RONAnimationKey::Boolean(b) => AnimationKey::Boolean(b),
                                RONAnimationKey::Float(f) => AnimationKey::Float(f),
                                RONAnimationKey::FloatColor(c) => AnimationKey::FloatColor(c),
                                RONAnimationKey::FloatRect(r) => AnimationKey::FloatRect(r),
                                RONAnimationKey::Vec2((x, y)) => {
                                    AnimationKey::Vec2(glm::vec2(x, y))
                                }
                                RONAnimationKey::Int(i) => AnimationKey::Int(i as f32),
                            };

                            track.animation_keys.push((key.0, val));
                        }

                        animation.add_track(track);
                    }
                }

                if let Some(ron_length) = self.length {
                    animation.length = ron_length;
                }

                if let Some(ron_reverse) = self.reverse {
                    animation.reverse = ron_reverse;
                }

                if let Some(ron_repeat) = self.repeat {
                    animation.repeat = ron_repeat;
                }

                if let Some(ron_autoplay) = self.autoplay {
                    if ron_autoplay {
                        animation.play();
                    }
                }

                bundle.world_mut().add_component(entity, animation);
            }
            RONComponentType::RigidBodyComponent => {
                let mut body = RigidBodyComponent::new();
                body.entity = entity;

                if let Some(ron_mesh) = &self.mesh {
                    let mut mesh: Vec<glm::Vec2> = Vec::with_capacity(ron_mesh.len() / 2);
                    for i in 0..ron_mesh.len() / 2 {
                        mesh.push(glm::vec2(ron_mesh[i * 2], ron_mesh[i * 2 + 1]));
                    }
                    body.set_mesh(mesh);
                } else {
                    return Err("Missing Mesh property in RigidBodyComponent".to_string());
                }

                if let Some(ron_body_type) = self.body_type {
                    body.body_type = ron_body_type;
                }

                if let Some(ron_friction) = self.friction {
                    body.friction = ron_friction;
                }

                if let Some(angular_inertia) = self.angular_inertia {
                    body.angular_inertia = angular_inertia;
                }

                if let Some(mass) = self.mass {
                    body.mass = mass;
                }

                if let Some(ron_max_angular) = self.max_angular_velocity {
                    body.max_angular_velocity = ron_max_angular;
                }

                if let Some(ron_max_linear) = self.max_linear_velocity {
                    body.max_linear_velocity = ron_max_linear;
                }

                if let Some(ron_ccd) = self.enable_ccd {
                    body.enable_ccd = ron_ccd;
                }

                if let Some(ron_linear_vel) = self.linear_velocity {
                    body.velocity.linear = glm::vec2(ron_linear_vel.0, ron_linear_vel.1);
                }

                if let Some(ron_angular_vel) = self.angular_velocity {
                    body.velocity.angular = ron_angular_vel;
                }

                if let Some(ron_sensor) = self.sensor {
                    body.sensor = ron_sensor;
                }

                if let Some(ron_one_way) = self.one_way_collision {
                    body.one_way_collision = ron_one_way;
                }

                if let Some(passive) = self.passive {
                    body.passive = passive;
                }

                bundle.world_mut().add_component(entity, body);
            }
            RONComponentType::JointComponent => {
                let mut joint = JointComponent::new();

                if let Some(joint_type) = self.joint_type {
                    joint.joint_type = joint_type;
                }

                if let Some(limit) = self.limit {
                    joint.limit = limit;
                }

                bundle.world_mut().add_component(entity, joint);
            }
            RONComponentType::RaycastComponent => {
                let mut raycast = RaycastComponent::new();

                if let Some(ron_direction) = self.direction {
                    raycast.direction = glm::vec2(ron_direction.0, ron_direction.1);
                }

                if let Some(ron_length) = self.length {
                    raycast.length = ron_length;
                }

                bundle.world_mut().add_component(entity, raycast);
            }
            RONComponentType::Polygon2DComponent => {
                let mut polygon = Polygon2DComponent::new();

                if let Some(ron_mesh) = &self.mesh {
                    let mut mesh: Vec<glm::Vec2> = Vec::with_capacity(ron_mesh.len() / 2);
                    for i in 0..ron_mesh.len() / 2 {
                        mesh.push(glm::vec2(ron_mesh[i * 2], ron_mesh[i * 2 + 1]));
                    }
                    polygon.shape = Polygon2DShape::Convex(mesh);
                } else {
                    return Err("Missing Mesh property in Polygon2DComponent".to_string());
                }

                if let Some(ron_color) = self.color {
                    polygon.color = ron_color;
                }

                if let Some(ron_bcolor) = self.border_color {
                    polygon.border_color = ron_bcolor;
                }

                if let Some(ron_border) = self.border {
                    polygon.border = ron_border;
                }

                if let Some(z_index) = self.z_index {
                    polygon.z_index = z_index;
                }

                if let Some(ron_layer) = self.layer {
                    polygon.layer = ron_layer;
                }

                bundle.world_mut().add_component(entity, polygon);
            }
            RONComponentType::TransformComponent => {
                let mut transform = TransformComponent::new();

                if let Some(ron_translation) = &self.translation {
                    transform.set_translation(glm::vec2(ron_translation.0, ron_translation.1));
                }

                if let Some(ron_scale) = &self.scale {
                    transform.set_scale(glm::vec2(ron_scale.0, ron_scale.1));
                }

                if let Some(inherit_rotation) = &self.inherit_parent_rotation {
                    transform.inherit_parent_rotation = *inherit_rotation;
                }

                if let Some(inherit_scale) = &self.inherit_parent_scale {
                    transform.inherit_parent_scale = *inherit_scale
                }
                bundle.world_mut().add_component(entity, transform);
            }
            RONComponentType::SoundSourceComponent => {
                if let Some(ron_stream) = &self.stream {
                    let stream = bundle
                        .resource_manager_mut()
                        .get_storage_for::<SoundStream>()
                        .get::<DefaultSoundStreamLoader>(ron_stream.as_str())
                        .unwrap();

                    let sound = SoundSourceComponent::new(stream);

                    bundle.world_mut().add_component(entity, sound);
                } else {
                    return Err(
                        "RONComponent: Missing stream property in SoundSourceComponent".to_owned(),
                    );
                }
            }
            RONComponentType::ScriptComponent => {
                let mut script = ScriptComponent::new();

                if let Some(path) = &self.path {
                    script.path = path.clone();
                }

                bundle.world_mut().add_component(entity, script);
            }
            RONComponentType::NavigationComponent => {
                if let Some(nav_path) = &self.nav_path {
                    let mut path: Vec<glm::Vec2> = Vec::with_capacity(nav_path.len() / 2);
                    for i in 0..nav_path.len() {
                        if i % 2 == 0 {
                            path.push(glm::vec2(0.0, 0.0));
                            path[i / 2].x = nav_path[i];
                        } else {
                            path[i / 2].y = nav_path[i];
                        }
                    }
                    bundle.world_mut().add_component(entity, NavigationComponent{
                        path,
                    });
                }
            }
        };
        Ok(())
    }
}

#[derive(Debug, Deserialize)]
struct RONEntity {
    children: Vec<RONEntity>,
    components: Vec<RONComponent>,
    prefab: Option<String>,
    tags: Option<Vec<String>>,
}

impl RONEntity {
    fn build(&self, bundle: &mut DataBundle) -> Result<Entity, String> {
        let this_entity;
        if let Some(prefab) = &self.prefab {
            let mut prefab_scene = Scene::from_file(prefab)?;
            if let Some(tags) = &self.tags {
                this_entity = prefab_scene.build_with_tags(bundle, tags.clone())?;
            } else {
                this_entity = prefab_scene.build(bundle)?;
            }
        } else {
            if let Some(tags) = &self.tags {
                let mut str_tags = Vec::with_capacity(tags.len());

                for tag in tags {
                    str_tags.push(tag.as_str());
                }

                this_entity = bundle
                    .world_mut()
                    .create_entity_with_tags(str_tags.as_ref());
            } else {
                this_entity = bundle.world_mut().create_entity();
            }
        }

        for ron_component in &self.components {
            ron_component.build(this_entity, bundle)?;
        }

        for ron_child in &self.children {
            let child_entity = ron_child.build(bundle)?;

            bundle.world_mut().attach_entity(this_entity, child_entity);
        }
        Ok(this_entity)
    }
}

#[derive(Debug, Deserialize)]
pub struct Scene {
    root: RONEntity,
}

impl Scene {
    pub fn from_data(ron_data: &str) -> Result<Self, String> {
        match from_str(ron_data) {
            Ok(scene) => Ok(scene),
            Err(error) => Err(error.to_string()),
        }
    }

    pub fn from_file(ron_file: &str) -> Result<Self, String> {
        let file = File::open(&ron_file);
        match file {
            Ok(file) => match from_reader(file) {
                Ok(scene) => Ok(scene),
                Err(error) => Err(error.to_string()),
            },
            Err(error) => Err(error.to_string()),
        }
    }

    pub fn build(&self, bundle: &mut DataBundle) -> Result<Entity, String> {
        self.root.build(bundle)
    }

    pub fn build_with_tags(&mut self, bundle: &mut DataBundle, mut tags: Vec<String>) -> Result<Entity, String> {
        if let Some(root_tags) = &mut self.root.tags {
            root_tags.append(&mut tags);
        } else {
            self.root.tags = Some(tags);
        }
        self.root.build(bundle)
    }
}

mod tests {
    #[allow(unused_imports)]
    use super::RONComponentType;
    #[allow(unused_imports)]
    use super::Scene;
    #[test]
    fn scene_from_data() {
        let ron_data = "
        /*
         * RON now has multi-line (C-style) block comments!
         * They can be freely nested:
         * /* This is a nested comment */
         * If you just want a single-line comment,
         * do it like here:
        // Just put two slashes before the comment and the rest of the line
        // can be used freely!
        */
        // Note that block comments can not be started in a line comment
        // (Putting a /* here will have no effect)
        Scene(
            root:
            (
                children: [
                    (
                        children: [],
                        components: [],
                    )
                ],
                components: [
                    (
                        component_type: TextComponent,
                    )
                ],
            ),
        )
        ";

        let scene = Scene::from_data(ron_data).unwrap();
        assert_eq!(scene.root.children.len(), 1);
        assert_eq!(scene.root.children[0].children.len(), 0);
        assert_eq!(
            scene.root.components[0].component_type,
            RONComponentType::Text2DComponent
        );
    }

    #[test]
    fn scene_load_error() {
        let ron_data = "garbage d4ta";

        let result = Scene::from_data(ron_data);
        assert_eq!(result.is_err(), true);

        let result = Scene::from_file("g4rb4g3 f!l3");
        assert_eq!(result.is_err(), true);
    }
}
