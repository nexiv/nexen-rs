/*************************************************************************/
/*  ecs.rs                                                               */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use std::any::Any;

pub mod world;
pub use world::World;
pub mod component_storage;
pub use component_storage::AnyComponentStorage;
pub use component_storage::ComponentStorage;
pub mod system;
pub use system::AsyncSystem;
pub use system::System;
pub trait Component: Any + Sync + Send {}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub struct Entity {
    pub(crate) id: usize,
}

impl Entity {
    pub fn null() -> Self {
        Entity { id: 0 }
    }
}

pub mod components {
    use super::Component;
    use super::Entity;
    use smallvec::SmallVec;

    #[derive(Clone)]
    pub struct HierarchyComponent {
        pub entity: Entity,
        pub parent: Entity,
        pub depth: usize,
        pub children: SmallVec<[Entity; 8]>,
    }

    pub struct NavigationComponent {
        pub path: Vec<glm::Vec2>,
    }

    impl Component for HierarchyComponent {}
    impl Component for NavigationComponent {}
}
