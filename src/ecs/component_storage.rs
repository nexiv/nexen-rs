/*************************************************************************/
/*  component_storage.rs                                                 */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use super::{Entity, Component};
use std::{
    any::Any,
    collections::HashMap,
    ops::{Deref, DerefMut},
    sync::Arc,
};
use atomic_refcell::{
    AtomicRefCell,
    AtomicRef,
    AtomicRefMut,
};

struct Store<T> {
    data: AtomicRefCell<Vec<T>>,
    data_map: AtomicRefCell<HashMap<usize, usize>>,
}

pub struct ComponentStorage<C: Component> {
    store: Arc<Store<C>>,
}

pub trait AnyComponentStorage: Any + Send + Sync {
    fn as_any(&self) -> &(dyn Any + Send + Sync);

    fn remove(&mut self, entity: Entity);

    fn clear(&mut self);
}

impl<C: Component> Clone for ComponentStorage<C> {
    fn clone(&self) -> Self {
        ComponentStorage {
            store: self.store.clone(),
        }
    }
}

impl<'a, C: Component> ComponentStorage<C> {
    pub fn new() -> Self {
        ComponentStorage {
            store: Arc::new(Store{
                data: AtomicRefCell::new(Vec::new()),
                data_map: AtomicRefCell::new(HashMap::new()),
            }),
        }
    }

    pub fn insert(&mut self, entity: Entity, component: C) {
        let mut data = (*self.store).data.borrow_mut();
        let data_map = &mut (*self.store).data_map.borrow_mut();

        if let Some(id) = data_map.get(&entity.id) {
            use log::warn;
            warn!("ComponentStorage::insert: Overwriting existing component for Entity {}",
                entity.id);
            data[*id] = component;
        } else {
            data.push(component);
            data_map.insert(entity.id, data.len() - 1);
        }
    }

    pub fn data(&self) -> AtomicRef<Vec<C>> {
        (*self.store).data.borrow()
    }

    pub fn data_mut(&self) -> AtomicRefMut<Vec<C>> {
        (*self.store).data.borrow_mut()
    }

    pub fn len(&self) -> usize {
        (*self.store).data.borrow().len()
    }

    pub fn read(&'a self, entity: Entity) -> Option<Read<'a, C>> {
        let data = (*self.store).data.borrow();
        let data_map = (*self.store).data_map.borrow();
        if let Some(id) = data_map.get(&entity.id) {
            Some(Read {
                inner: AtomicRef::map(data, |t| {
                    &t[*id]
                }),
            })
        } else {
            None
        }        
    }

    pub fn write(&'a self, entity: Entity) -> Option<Write<'a, C>> {
        let data = (*self.store).data.borrow_mut();
        let data_map = (*self.store).data_map.borrow();
        if let Some(id) = data_map.get(&entity.id) {
            Some(Write {
                inner: AtomicRefMut::map(data, |t| {
                    &mut t[*id]
                }),
            })
        } else {
            None
        }
    }

    pub fn component_ref(&'a self, entity: Entity) -> Option<Ref<C>> {
        let data_map = (*self.store).data_map.borrow();
        if let Some(id) = data_map.get(&entity.id) {
            Some(Ref {
                inner: self.store.clone(),
                id: *id,
            })
        } else {
            None
        }
    }

    pub fn join_read_both<B: Component>(&'a self, other_storage: &'a ComponentStorage<B>, entity: Entity) -> Option<(Read<'a, C>, Read<'a, B>)>{
        if let Some(comp_a) = self.read(entity) {
            if let Some(comp_b) = other_storage.read(entity) {
                Some((comp_a, comp_b))
            } else {
                None
            }
        } else {
            None
        }
    }

    pub fn join_write_both<B: Component>(&'a self, other_storage: &'a ComponentStorage<B>, entity: Entity) -> Option<(Write<'a, C>, Write<'a, B>)>{
        if let Some(comp_a) = self.write(entity) {
            if let Some(comp_b) = other_storage.write(entity) {
                Some((comp_a, comp_b))
            } else {
                None
            }
        } else {
            None
        }
    }

    pub fn join_read_write<B: Component>(&'a self, other_storage: &'a ComponentStorage<B>, entity: Entity) -> Option<(Read<'a, C>, Write<'a, B>)>{
        if let Some(comp_a) = self.read(entity) {
            if let Some(comp_b) = other_storage.write(entity) {
                Some((comp_a, comp_b))
            } else {
                None
            }
        } else {
            None
        }
    }

    pub fn join_write_read<B: Component>(&'a self, other_storage: &'a ComponentStorage<B>, entity: Entity) -> Option<(Write<'a, C>, Read<'a, B>)>{
        if let Some(comp_a) = self.write(entity) {
            if let Some(comp_b) = other_storage.read(entity) {
                Some((comp_a, comp_b))
            } else {
                None
            }
        } else {
            None
        }
    }
}

impl<C: Component> AnyComponentStorage for ComponentStorage<C> {
    fn as_any(&self) -> &(dyn Any + Send + Sync) {
        self
    }

    fn remove(&mut self, entity: Entity) {
        let mut data_map = (*self.store).data_map.borrow_mut();
        let mut data = (*self.store).data.borrow_mut();
        let last_id = data.len() - 1;

        if let Some(id) = data_map.get(&entity.id) {
            data.swap_remove(*id);
        } else {
            return;
        }

        if data.len() > 0 {
            let last_entity = *data_map
                .iter()
                .find_map(|(k, v)| {
                    if *v == last_id {
                        Some(k)
                    } else {
                        None
                    }
                })
                .unwrap();
            *data_map.get_mut(&last_entity).unwrap() = entity.id;
        }

        data_map.remove(&entity.id);
    }

    fn clear(&mut self) {
        (*self.store).data_map.borrow_mut().clear();
        (*self.store).data.borrow_mut().clear();
    }
}

pub struct Ref<C: Component> {
    inner: Arc<Store<C>>,
    id: usize,
}

pub struct Read<'a, C: Component> {
    inner: AtomicRef<'a, C>,
}

pub struct Write<'a, C: Component> {
    inner: AtomicRefMut<'a, C>,
}

impl<'a, C: Component> Ref<C> {
    pub fn read(&'a self) -> Read<'a, C> {
        let data = (*self.inner).data.borrow();
        Read {
            inner: AtomicRef::map(data, |t| {
                &t[self.id]
            })
        }
    }

    pub fn write(&'a self) -> Write<'a, C> {
        let data = (*self.inner).data.borrow_mut();
        Write {
            inner: AtomicRefMut::map(data, |t| {
                &mut t[self.id]
            })
        }
    }
}

impl<'a, C: Component> Clone for Ref<C> {
    fn clone(&self) -> Self {
        Ref {
            inner: self.inner.clone(),
            id: self.id,
        }
    }
}

impl<'a, C: Component> Deref for Read<'a, C> {
    type Target = C;
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<'a, C: Component> Clone for Read<'a, C> {
    fn clone(&self) -> Self {
        Read {
            inner: AtomicRef::clone(&self.inner),
        }
    }
}

impl<'a, C: Component> Deref for Write<'a, C> {
    type Target = C;
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<'a, C: Component> DerefMut for Write<'a, C> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}