/*************************************************************************/
/*  system.rs                                                            */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::core::DataBundle;
use crate::core::application::InitArgs;
use crate::core::event::Event;
use crate::graphics::Renderer2D;
use rayon::iter::{
    IntoParallelRefMutIterator,
    ParallelIterator,
};
use std::collections::HashMap;
use log::error;

#[cfg(feature = "profile")]
use crate::profile::Profiler;

pub trait System {

    fn init(&mut self, _bundle: &mut DataBundle, _args: &InitArgs) {}
    fn update(&mut self, bundle: &DataBundle, delta: f32);
    fn render(&self, _bundle: &DataBundle, _renderer: &mut Renderer2D) {}
    fn handle_events(&mut self, _bundle: &mut DataBundle, _event: &Event) {}
}

pub trait AsyncSystem: System + Sync + Send {}

type SystemGroup = Vec<Box<dyn System>>;
type AsyncSystemGroup = Vec<Box<dyn AsyncSystem>>;

pub struct StageScheduler {
    systems: HashMap<&'static str, SystemGroup>,
    async_systems: HashMap<&'static str, AsyncSystemGroup>,
    //thread_pool: std::sync::Arc<rayon::ThreadPool>,
}

impl StageScheduler {
    pub fn new() -> Self {
        StageScheduler {
            systems: HashMap::new(),
            async_systems: HashMap::new(),
            /*thread_pool: std::sync::Arc::new(rayon::ThreadPoolBuilder::new()
                .num_threads(12)
                .build()
                .unwrap()),*/
        }
    }

    pub fn add_system_to<T: 'static + System>(&mut self, stage: &'static str, system: T)
    {
        let group = self.systems
            .entry(stage)
            .or_insert(Vec::new());

        group.push(Box::new(system));
    }

    pub fn add_async_system_to<T: 'static + AsyncSystem>(&mut self, stage: &'static str, system: T)
    {
        let group = self.async_systems
            .entry(stage)
            .or_insert(Vec::new());

        group.push(Box::new(system));
    }

    pub fn run_stage(&mut self, stage: &'static str, bundle: &DataBundle, delta: f32) {
        if !self.systems.contains_key(stage) {
            error!("StageScheduler::run_stage: Stage '{}' was not added.", stage);
            return;
        }

        #[cfg(feature = "profile")]
        let profiler_tag = format!("Stage '{}' Update", stage);
        #[cfg(feature = "profile")]
        let mut profiler = Profiler::new(profiler_tag.as_str());

        let group = self.systems.get_mut(stage).unwrap();

        for system in group {
            system.update(bundle, delta);
        }

        #[cfg(feature = "profile")]
        profiler.stamp()
    }

    pub fn run_async_stage(&mut self, stage: &'static str, bundle: &DataBundle, delta: f32) {
        if !self.async_systems.contains_key(stage) {
            error!("StageScheduler::run_async_stage: Stage '{}' was not added.", stage);
            return;
        }

        #[cfg(feature = "profile")]
        let profiler_tag = format!("Stage '{}' Update", stage);
        #[cfg(feature = "profile")]
        let mut profiler = Profiler::new(profiler_tag.as_str());

        let group = self.async_systems.get_mut(stage).unwrap();

        group.par_iter_mut().for_each(|system| {
            system.update(bundle, delta);
        });

        #[cfg(feature = "profile")]
        profiler.stamp()
    }

    pub fn run_event_stage(&mut self, stage: &'static str, bundle: &mut DataBundle, event: &Event) {
        if let Some(group) = self.systems.get_mut(stage) {
            #[cfg(feature = "profile")]
            let profiler_tag = format!("Stage '{}' Event", stage);
            #[cfg(feature = "profile")]
            let mut profiler = Profiler::new(profiler_tag.as_str());

            for system in group {
                system.handle_events(bundle, event);
            }

            #[cfg(feature = "profile")]
            profiler.stamp();
            return;
        }
        if let Some(async_group) = self.async_systems.get_mut(stage) {
            #[cfg(feature = "profile")]
            let profiler_tag = format!("Stage '{}' Event", stage);
            #[cfg(feature = "profile")]
            let mut profiler = Profiler::new(profiler_tag.as_str());

            for system in async_group {
                system.handle_events(bundle, event);
            }

            #[cfg(feature = "profile")]
            profiler.stamp();
            return;
        }

        error!("StageScheduler::run_event_stage: Stage '{}' was not added.", stage);
    }

    pub fn run_render_stage(&mut self, stage: &'static str, bundle: &DataBundle, renderer: &mut Renderer2D) {
        if let Some(group) = self.systems.get_mut(stage) {
            for system in group {
                system.render(bundle, renderer);
            }
            return;
        }
        if let Some(async_group) = self.async_systems.get_mut(stage) {
            for system in async_group {
                system.render(bundle, renderer);
            }
            return;
        }

        error!("StageScheduler::run_render_stage: Stage '{}' was not added.", stage);
    }
}
