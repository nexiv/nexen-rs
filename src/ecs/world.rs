/*************************************************************************/
/*  world.rs                                                             */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use super::{
    component_storage::{Read, Ref, Write},
    components::{HierarchyComponent, NavigationComponent},
    AnyComponentStorage, Component, ComponentStorage, Entity,
};
use std::any::TypeId;
use std::collections::HashMap;

pub struct World {
    storages: HashMap<TypeId, Box<dyn AnyComponentStorage>>,
    entities: Vec<Entity>,
    tag_groups: HashMap<String, Vec<Entity>>,
    entity_tags: HashMap<usize, Vec<String>>,
}

impl<'a> World {
    pub fn new() -> Self {
        let mut world = World {
            storages: HashMap::new(),
            entities: Vec::new(),
            tag_groups: HashMap::new(),
            entity_tags: HashMap::new(),
        };
        world.register::<HierarchyComponent>();
        world.register::<NavigationComponent>();
        world
    }

    pub fn register<C: Component>(&mut self) {
        self.storages
            .insert(TypeId::of::<C>(), Box::new(ComponentStorage::<C>::new()));
    }

    pub fn component_storage<C: Component>(&self) -> ComponentStorage<C> {
        let storage = self.storages.get(&TypeId::of::<C>()).unwrap();
        let storage = storage
            .as_any()
            .downcast_ref::<ComponentStorage<C>>()
            .unwrap();
        (*storage).clone()
    }

    pub fn get_component_ref<C: Component>(&self, entity: Entity) -> Option<Ref<C>> {
        let storage = self.storages.get(&TypeId::of::<C>()).unwrap();
        let storage = storage
            .as_any()
            .downcast_ref::<ComponentStorage<C>>()
            .unwrap();
        (*storage).component_ref(entity)
    }

    pub fn get_read_component<C: Component>(&self, entity: Entity) -> Option<Read<C>> {
        let storage = self.storages.get(&TypeId::of::<C>()).unwrap();
        let storage = storage
            .as_any()
            .downcast_ref::<ComponentStorage<C>>()
            .unwrap();
        (*storage).read(entity)
    }

    pub fn get_write_component<C: Component>(&self, entity: Entity) -> Option<Write<C>> {
        let storage = self.storages.get(&TypeId::of::<C>()).unwrap();
        let storage = storage
            .as_any()
            .downcast_ref::<ComponentStorage<C>>()
            .unwrap();
        (*storage).write(entity)
    }

    pub fn create_entity(&mut self) -> Entity {
        let entity = Entity {
            id: self.entities.len() + 1,
        };
        self.entities.push(entity);
        entity
    }

    pub fn create_entity_with_tags(&mut self, tags: &[&str]) -> Entity {
        let entity = self.create_entity();

        let mut entity_tags = Vec::with_capacity(tags.len());

        for tag in tags {
            if let Some(tag_group) = self.tag_groups.get_mut(&(*tag).to_owned()) {
                tag_group.push(entity);
            } else {
                self.tag_groups.insert((*tag).to_owned(), vec![entity]);
            }

            entity_tags.push((*tag).to_owned());
        }

        self.entity_tags.insert(entity.id, entity_tags);

        entity
    }

    pub fn remove_entity(&mut self, entity: Entity) {
        for storage in self.storages.values_mut() {
            storage.remove(entity);
        }
    }

    pub fn clear(&mut self) {
        for storage in self.storages.values_mut() {
            storage.clear();
        }
    }

    pub fn attach_entity(&mut self, parent: Entity, child: Entity) {
        if parent == child {
            use log::warn;
            warn!("World::attach_entity: parent and child are the same");
            return;
        }

        let hierarchies = self.component_storage::<HierarchyComponent>();

        // If parent is already on tree
        let mut parent_depth = 0;
        if let Some(mut hierarchy) = hierarchies.write(parent) {
            hierarchy.children.push(child);
            parent_depth = hierarchy.depth;
        } else {
            // If parent is not on tree, create new HierarchyComponent
            let children = smallvec::SmallVec::from_vec(vec![child]);
            self.add_component(
                parent,
                HierarchyComponent {
                    entity: parent,
                    parent: Entity::null(),
                    depth: 0,
                    children,
                },
            );
        }

        // If child is already on tree
        if let Some(mut hierarchy) = hierarchies.write(child) {
            hierarchy.parent = parent;
            hierarchy.depth = parent_depth + 1;
        } else {
            // If child is not on tree
            self.add_component(
                child,
                HierarchyComponent {
                    entity: child,
                    parent: parent,
                    depth: parent_depth + 1,
                    children: smallvec::SmallVec::new(),
                },
            );
        };

        /*hierarchies.data_mut().sort_by(|hierarchy_a, hierarchy_b| {
            hierarchy_a.depth.partial_cmp(&hierarchy_b.depth).unwrap()
        });*/
    }

    pub fn entity_parent(&self, entity: Entity) -> Option<Entity> {
        if let Some(hierarchy) = self.get_read_component::<HierarchyComponent>(entity) {
            Some(hierarchy.parent)
        } else {
            None
        }
    }

    pub fn entity_children(&self, entity: Entity) -> Vec<Entity> {
        let hierarchies = self.component_storage::<HierarchyComponent>();

        // Retrieve child count from HierarchyComponent
        let vec = if let Some(hierarchy) = hierarchies.read(entity) {
            hierarchy.children.to_vec().clone()
            /*hierarchy.children.len()
            let mut children = Vec::with_capacity(hierarchy.children.len());
            for child in hierarchy.children {
                if let Some(hierarchy) = hierarchies.read(*e) {
                    if hierarchy.parent == entity {
                        children.push(*e);
                    }
                    if children.len() == child_count {
                        break;
                    }
                }
            }*/
        } else {
            Vec::new()
        };

        vec
    }

    pub fn entity_children_by_tag(&self, entity: Entity, tag: &str) -> Option<Vec<Entity>> {
        let hierarchies = self.component_storage::<HierarchyComponent>();

        if let Some(children) = self.entities_with_tag(tag) {
            let mut return_vec = Vec::new();
            for child in children {
                if hierarchies.read(child).unwrap().parent == entity {
                    return_vec.push(child);
                }
            }

            if return_vec.len() > 0 {
                Some(return_vec)
            } else {
                None
            }
        } else {
            None
        }
    }

    pub fn entity_tags(&self, entity: Entity) -> Option<Vec<String>> {
        if let Some(tags) = self.entity_tags.get(&entity.id) {
            Some(tags.clone())
        } else {
            None
        }
    }

    pub fn entities_with_tag(&self, tag: &str) -> Option<Vec<Entity>> {
        if let Some(tag_group) = self.tag_groups.get(&tag.to_owned()) {
            Some(tag_group.clone())
        } else {
            None
        }
    }

    pub fn add_component<C: Component>(&mut self, entity: Entity, component: C) {
        self.component_storage::<C>().insert(entity, component);
    }

    pub fn entities(&self) -> &[Entity] {
        self.entities.as_slice()
    }
}
