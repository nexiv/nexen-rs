/*************************************************************************/
/*  graphics.rs                                                          */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
pub mod systems;

pub mod render_device;
pub use render_device::RenderDevice;
pub use render_device::GLRenderDevice;

pub mod render_queue;

pub mod color;
pub use color::colors as colors;
pub use color::Color as Color;

pub mod camera2d;
pub use camera2d::Camera2DComponent as Camera2DComponent;
pub use camera2d::Parallax2DComponent as Parallax2DComponent;
pub use camera2d::CameraMode as CameraMode;

pub mod canvas_layer;
pub use canvas_layer::CanvasLayer as CanvasLayer;

pub mod shader;
pub use shader::ShaderType as ShaderType;
pub use shader::Shader as Shader;
pub use shader::ShaderProgram as ShaderProgram;

pub mod texture;
pub use texture::Texture as Texture;
pub use texture::DefaultTextureLoader as DefaultTextureLoader;

pub mod buffer;
pub use buffer::VertexBuffer as VertexBuffer;
pub use buffer::IndexBuffer as IndexBuffer;

pub mod framebuffer;

pub use framebuffer::Framebuffer as Framebuffer;

pub mod vertex_layout;
pub use vertex_layout::VertexDataType as VertexDataType;

pub mod image;
pub use image::Image as Image;

pub mod font;
pub use font::Font as Font;
pub use font::DefaultFontLoader as DefaultFontLoader;

pub mod renderer2d;
pub use renderer2d::Renderer2D as Renderer2D;

pub mod sprite2d;
pub use sprite2d::Sprite2DComponent as Sprite2DComponent;

pub mod sprite_batch2d;
pub use sprite_batch2d::SpriteBatch2DComponent as SpriteBatch2DComponent;
pub use sprite_batch2d::SpriteBatch2DRect as SpriteBatch2DRect;

pub mod polygon2d;
pub use polygon2d::Polygon2DShape as Polygon2DShape;
pub use polygon2d::Polygon2DComponent as Polygon2DComponent;

pub mod text2d;
pub use text2d::Text2DComponent as Text2DComponent;

pub mod transform;
pub use transform::TransformComponent as TransformComponent;
pub use transform::TransformSystem as TransformSystem;
