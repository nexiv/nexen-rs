/*************************************************************************/
/*  buffer.rs                                                            */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use super::vertex_layout::{VertexElement, VertexLayout};

pub struct VertexBuffer {
    id: gl::types::GLuint,
    element_size: u32,
    element_count: u32,
    vertex_layout: VertexLayout,
}

impl VertexBuffer {
    pub fn new(element_size: u32) -> Self {
        let mut id: gl::types::GLuint = 0;
        unsafe { gl::GenBuffers(1, &mut id); }
        VertexBuffer { id, element_size, element_count: 0, vertex_layout: VertexLayout::new() }
    }

    pub fn with_size(element_size: u32, size: u32) -> Self {
        let mut vbo = VertexBuffer::new(element_size);
        vbo.resize(size);
        vbo
    }

    pub fn from_data<T>(element_size: u32, data: &[T]) -> Self {
        let mut vb = VertexBuffer::new(element_size);
        vb.set_data(data);
        vb
    }

    pub fn set_data<T>(&mut self, data: &[T]) {
        self.element_count = data.len() as u32 / self.element_size;
        unsafe {
            gl::BindBuffer(gl::ARRAY_BUFFER, self.id);
            gl::BufferData(gl::ARRAY_BUFFER,
                (data.len() * std::mem::size_of::<T>()) as gl::types::GLsizeiptr,
                data.as_ptr() as *const gl::types::GLvoid, gl::STATIC_DRAW);
            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
        }
    }

    pub fn fill_data<T>(&mut self, offset: u32, data: &[T]) {
        unsafe {
            gl::BindBuffer(gl::ARRAY_BUFFER, self.id);
            gl::BufferSubData(gl::ARRAY_BUFFER,
                (offset * std::mem::size_of::<T>() as u32) as gl::types::GLintptr,
                (data.len() * std::mem::size_of::<T>()) as gl::types::GLsizeiptr,
                data.as_ptr() as *const gl::types::GLvoid);
            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
        }
        //TODO: This is wrong
        self.element_count += (data.len() / std::mem::size_of::<T>()) as u32;
    }

    pub fn resize(&mut self, count: u32) {
        self.element_count = 0;
        unsafe {
            gl::BindBuffer(gl::ARRAY_BUFFER, self.id);
            gl::BufferData(gl::ARRAY_BUFFER,
                (count * self.element_size) as gl::types::GLsizeiptr,
                std::ptr::null() as *const gl::types::GLvoid, gl::STATIC_DRAW);
            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
        }
    }

    pub fn set_vertex_layout(&mut self, elements: &[VertexElement]) {
        self.vertex_layout.set_from_elements(&elements);
    }

    pub fn bind(&self) {
        unsafe {
            gl::BindBuffer(gl::ARRAY_BUFFER, self.id);
        }
    }

    pub fn vertex_layout(&self) -> &VertexLayout {
        &self.vertex_layout
    }

    pub fn count(&self) -> u32 {
        self.element_count
    }
}

impl Drop for VertexBuffer {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteBuffers(1, &self.id);
        }
    }
}

pub struct IndexBuffer {
    id: gl::types::GLuint,
    count: u32,
}

impl IndexBuffer {
    pub fn new() -> Self {
        let mut id: gl::types::GLuint = 0;
        unsafe { gl::GenBuffers(1, &mut id); }
        IndexBuffer { id, count: 0 }
    }

    pub fn with_size(size: u32) -> Self {
        let mut ibo = IndexBuffer::new();
        ibo.resize(size);
        ibo
    }

    pub fn from_data(data: &[u32]) -> Self {
        let mut ib = IndexBuffer::new();
        ib.set_data(data);
        ib
    }

    pub fn set_data(&mut self, data: &[u32]) {
        self.count = data.len() as u32;
        unsafe {
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.id);
            gl::BufferData(gl::ELEMENT_ARRAY_BUFFER,
                (data.len() * std::mem::size_of::<u32>()) as gl::types::GLsizeiptr,
                data.as_ptr() as *const gl::types::GLvoid, gl::STATIC_DRAW);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
        }
    }

    pub fn fill_data(&mut self, offset: u32, data: &[u32]) {
        unsafe {
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.id);
            gl::BufferSubData(gl::ELEMENT_ARRAY_BUFFER,
                (offset * std::mem::size_of::<u32>() as u32) as gl::types::GLintptr,
                (data.len() * std::mem::size_of::<u32>()) as gl::types::GLsizeiptr,
                data.as_ptr() as *const gl::types::GLvoid);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
        }
        self.count += data.len() as u32;
    }

    pub fn resize(&mut self, count: u32) {
        self.count = 0;
        unsafe {
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.id);
            gl::BufferData(gl::ELEMENT_ARRAY_BUFFER,
                (count * std::mem::size_of::<u32>() as u32) as gl::types::GLsizeiptr,
                std::ptr::null() as *const gl::types::GLvoid, gl::STATIC_DRAW);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
        }
    }

    pub fn bind(&self) {
        unsafe {
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.id);
        }
    }

    pub fn count(&self) -> u32 {
        self.count
    }
}
