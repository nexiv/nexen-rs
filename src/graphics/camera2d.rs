/*************************************************************************/
/*  camera.rs                                                            */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::common::{Size2D, FloatRect};
use crate::ecs::Component;
use serde::Deserialize;
use std::ops::Range;

#[derive(Debug, Clone, Copy, Deserialize)]
pub enum CameraMode {
    Immediate,
    FollowEntity,
}

pub struct Camera2DComponent {
    pub viewport: Size2D,
    pub follow_active_range: glm::Vec2,
    pub mode: CameraMode,
    pub follow_acceleration: glm::Vec2,
    pub round_camera_coords: bool,
    pub limits: Option<FloatRect>,
    pub layers: Range<i8>,
    pub active: bool,
    pub(crate) position: glm::Vec2,
}

pub struct Parallax2DComponent {
    pub layer: i8,
    pub factor: glm::Vec2,
}

impl Camera2DComponent {
    pub fn new() -> Self {
        Camera2DComponent {
            viewport: Size2D::new(0, 0),
            follow_active_range: glm::vec2(0.8,0.8),
            position: glm::vec2(0.0, 0.0),
            follow_acceleration: glm::vec2(1.0, 1.0),
            mode: CameraMode::Immediate,
            round_camera_coords: false,
            active: false,
            layers: Range {start: 0, end: 0},
            limits: None,
        }
    }
}

impl Parallax2DComponent {
    pub fn new() -> Self {
        Parallax2DComponent {
            layer: 0,
            factor: glm::vec2(1.0, 1.0),
        }
    }
}

impl Component for Camera2DComponent {}
impl Component for Parallax2DComponent {}
