/*************************************************************************/
/*  canvas_layer.rs                                                      */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::common::Size2D;
use super::render_queue::{RenderCommand, RenderQueue};
use super::ShaderProgram;
use std::rc::Rc;
use std::cell::RefCell;

pub struct CanvasLayer {
    pub(super) queue: RenderQueue,
    pub(super) viewport_size: Size2D,
    pub(super) shader: Option<Rc<RefCell<ShaderProgram>>>,
    //pub(super) camera_position: glm::Vec2,
}

impl CanvasLayer {
    pub fn new(viewport_size: Size2D) -> Self {
        CanvasLayer {
            queue: RenderQueue::with_capacity(500),
            viewport_size,
            shader: None,
            //camera_position: glm::vec2(0.0, 0.0),
        }
    }

    pub fn push_command(&mut self, command: RenderCommand, z_index: i16) {
        self.queue.push((command, z_index));
    }

    pub fn drain_commands(&mut self) -> RenderQueue {
        let mut queue: RenderQueue = self.queue.drain(..).collect();
        queue.sort_unstable_by(|a, b| {
            a.1.cmp(&b.1)
        });
        queue
    }
}