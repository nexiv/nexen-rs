/*************************************************************************/
/*  color.rs                                                             */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use serde::Deserialize;
use std::ops::{Sub, Add, Mul};
use std::num::Wrapping;

pub mod colors {
    use super::Color;

    pub const RED:      Color = Color{ r: 255, g: 0, b: 0, a: 255 };
    pub const GREEN:    Color = Color{ r: 0, g: 255, b: 0, a: 255 };
    pub const BLUE:     Color = Color{ r: 0, g: 0, b: 255, a: 255 };
    pub const YELLOW:   Color = Color{ r: 255, g: 255, b: 0, a: 255 };
    pub const MAGENTA:  Color = Color{ r: 255, g: 0, b: 255, a: 255 };
    pub const CYAN:     Color = Color{ r: 0, g: 255, b: 255, a: 255 };
    pub const WHITE:    Color = Color{ r: 255, g: 255, b: 255, a: 255 };
    pub const BLACK:    Color = Color{ r: 0, g: 0, b: 0, a: 255 };
}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Deserialize)]
pub struct FloatColor {
    pub r: f32,
    pub g: f32,
    pub b: f32,
    pub a: f32,
}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, serde::Deserialize)]
pub struct Color {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

impl FloatColor {
    pub fn new(r: f32, g: f32, b: f32, a: f32) -> Self {
        FloatColor { r, g, b, a }
    }
}

impl Color {
    pub fn new(r: u8, g: u8, b: u8, a: u8) -> Self {
        Color { r, g, b, a }
    }
}

impl Sub for Color {
    type Output = Color;

    fn sub(self, other: Self) -> Self {
        Color::new((Wrapping(self.r) - Wrapping(other.r)).0,
            (Wrapping(self.g) - Wrapping(other.g)).0,
            (Wrapping(self.b) - Wrapping(other.b)).0,
            (Wrapping(self.a) - Wrapping(other.a)).0)
    }
}

impl Add for Color {
    type Output = Color;

    fn add(self, other: Self) -> Self {
        Color::new((Wrapping(self.r) + Wrapping(other.r)).0,
            (Wrapping(self.g) + Wrapping(other.g)).0,
            (Wrapping(self.b) + Wrapping(other.b)).0,
            (Wrapping(self.a) + Wrapping(other.a)).0)
    }
}

impl Mul for Color {
    type Output = Color;

    fn mul(self, other: Self) -> Self {
        Color::new((Wrapping(self.r) * Wrapping(other.r)).0,
            (Wrapping(self.g) * Wrapping(other.g)).0,
            (Wrapping(self.b) * Wrapping(other.b)).0,
            (Wrapping(self.a) * Wrapping(other.a)).0)
    }
}

impl Sub for FloatColor {
    type Output = FloatColor;

    fn sub(self, other: Self) -> Self {
        FloatColor::new(self.r - other.r, self.g - other.g, self.b - other.b, self.a - other.a)
    }
}

impl Add for FloatColor {
    type Output = FloatColor;

    fn add(self, other: Self) -> Self {
        FloatColor::new(self.r + other.r, self.g + other.g, self.b + other.b, self.a + other.a)
    }
}

impl Mul for FloatColor {
    type Output = FloatColor;

    fn mul(self, other: Self) -> Self {
        FloatColor::new(self.r * other.r, self.g * other.g, self.b * other.b, self.a * other.a)
    }
}

impl Sub<f32> for FloatColor {
    type Output = FloatColor;

    fn sub(self, other: f32) -> Self {
        FloatColor::new(self.r - other, self.g - other, self.b - other, self.a - other)
    }
}

impl Add<f32> for FloatColor {
    type Output = FloatColor;

    fn add(self, other: f32) -> Self {
        FloatColor::new(self.r + other, self.g + other, self.b + other, self.a + other)
    }
}

impl Mul<f32> for FloatColor {
    type Output = FloatColor;

    fn mul(self, other: f32) -> Self {
        FloatColor::new(self.r * other, self.g * other, self.b * other, self.a * other)
    }
}

impl From<(u8, u8, u8, u8)> for Color {
    fn from((r, g, b, a): (u8, u8, u8, u8)) -> Self {
        Color { r, g, b, a }
    }
}

impl From<&(u8, u8, u8, u8)> for Color {
    fn from((r, g, b, a): &(u8, u8, u8, u8)) -> Self {
        Color { r: *r, g: *g, b: *b, a: *a }
    }
}

impl From<[u8;4]> for Color {
    fn from(slice: [u8;4]) -> Self {
        Color { r: slice[0], g: slice[1], b: slice[2], a: slice[3] }
    }
}

impl From<FloatColor> for Color {
    fn from(fcolor: FloatColor) -> Self {
        Color {
            r: (fcolor.r * 255.0) as u8,
            g: (fcolor.g * 255.0) as u8,
            b: (fcolor.b * 255.0) as u8,
            a: (fcolor.a * 255.0) as u8,
        }
    }
}

impl From<&FloatColor> for Color {
    fn from(fcolor: &FloatColor) -> Self {
        Color {
            r: (fcolor.r * 255.0) as u8,
            g: (fcolor.g * 255.0) as u8,
            b: (fcolor.b * 255.0) as u8,
            a: (fcolor.a * 255.0) as u8,
        }
    }
}


impl From<(f32, f32, f32, f32)> for FloatColor {
    fn from((r, g, b, a): (f32, f32, f32, f32)) -> Self {
        FloatColor { r, g, b, a }
    }
}

impl From<[f32;4]> for FloatColor {
    fn from(slice: [f32;4]) -> Self {
        FloatColor { r: slice[0], g: slice[1], b: slice[2], a: slice[3] }
    }
}

impl From<Color> for FloatColor {
    fn from(color: Color) -> Self {
        FloatColor {
            r: (color.r as f32) / 255.0,
            g: (color.g as f32) / 255.0,
            b: (color.b as f32) / 255.0,
            a: (color.a as f32) / 255.0,
        }
    }
}

impl From<&Color> for FloatColor {
    fn from(color: &Color) -> Self {
        FloatColor {
            r: (color.r as f32) / 255.0,
            g: (color.g as f32) / 255.0,
            b: (color.b as f32) / 255.0,
            a: (color.a as f32) / 255.0,
        }
    }
}
