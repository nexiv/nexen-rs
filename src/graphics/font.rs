/*************************************************************************/
/*  font.rs                                                              */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::resources::{ResourceLoader, Resource};
use crate::common::{Rect, FloatRect};
use super::color::Color;
use super::texture::Texture;
use super::image::Image;

use std::{
    collections::HashMap,
    fs::File,
    io::Read,
};

struct PageRow {
    width: u32,
    height: u32,
    top: u32,
}

struct FontPage {
    texture: Texture,
    glyphs: HashMap<char, Glyph>,
    rows: Vec<PageRow>,
    next_row: u32,
}

pub struct Glyph {
    pub advance: f32,
    pub rect: FloatRect,
    pub tex_coords: Rect,
}

#[derive(Default)]
pub struct DefaultFontLoader;

impl ResourceLoader<Font> for DefaultFontLoader {
    fn from_file(&self, path: &str) -> Font {
        Font::from_file(&path)
    }
}

pub struct Font {
    rtype_font: rusttype::Font<'static>,
    pages: HashMap<u32, FontPage>,
    texture_aliasing: bool,
}

impl Resource for Font {}

impl Font {
    pub fn from_file(path: &str) -> Font {
        let mut file = File::open(path).unwrap();

        let mut data = Vec::new();

        file.read_to_end(&mut data).unwrap();

        Font {
            rtype_font: rusttype::Font::try_from_vec(data).unwrap(),
            pages: HashMap::new(),
            texture_aliasing: true,
        }
    }

    pub fn set_texture_aliasing(&mut self, aliasing: bool) {
        self.texture_aliasing = aliasing;
        if aliasing {
            for page in &mut self.pages {
                page.1.texture.set_filter_mode(super::texture::FilterMode::Nearest);
            }
        } else {    
            for page in &mut self.pages {
                page.1.texture.set_filter_mode(super::texture::FilterMode::Bilinear);
            }
        }
    }

    pub fn get_glyph(&mut self, char_code: char, char_size: u32) -> &Glyph {
        if !self.pages.contains_key(&char_size) {
            self.generate_page(char_size);
            self.load_glyph(char_size, char_code);
        } else {
            let page = self.pages.get(&char_size).unwrap();

            if !page.glyphs.contains_key(&char_code) {
                self.load_glyph(char_size, char_code);
            }
        }

        self.pages.get(&char_size).unwrap()
            .glyphs.get(&char_code).unwrap()
    }

    pub fn get_kerning(&self, char_a: char, char_b: char, char_size: u32) -> f32 {
        self.rtype_font.pair_kerning(rusttype::Scale::uniform((char_size * 2) as f32), char_a, char_b)
    }

    pub fn get_line_spacing(&self, char_size: u32) -> f32 {
        let metrics = self.rtype_font.v_metrics(rusttype::Scale::uniform((char_size * 2) as f32));
        metrics.ascent - metrics.descent
    }

    pub fn get_page_texture(&self, char_size: u32) -> &Texture {
        &self.pages.get(&char_size).unwrap().texture
    }

    fn load_glyph(&mut self, char_size: u32, char_code: char) {
        let page = self.pages.get_mut(&char_size).unwrap();

        let rt_glyph = self.rtype_font.glyph(char_code);

        let mut rect = FloatRect::new(0.0, 0.0, 0.0, 0.0);
        let tex_coords: Rect;
        let advance;

        let rt_glyph = rt_glyph.scaled(rusttype::Scale::uniform((char_size * 2) as f32));
        advance = rt_glyph.h_metrics().advance_width;

        let rt_glyph = rt_glyph.positioned(rusttype::point(0.0, 0.0));

        rect.x = rt_glyph.pixel_bounding_box().unwrap().min.x as f32;
        rect.y = rt_glyph.pixel_bounding_box().unwrap().min.y as f32;
        rect.w = rt_glyph.pixel_bounding_box().unwrap().width() as f32;
        rect.h = rt_glyph.pixel_bounding_box().unwrap().height() as f32;

        let width = rect.w as u32 + 2;
        let height = rect.h as u32 + 2;

        let mut best_ratio: f32 = 0.0;
        let mut idx: i32 = -1;
        let mut row_idx: i32 = idx;

        for row in &page.rows {
            idx += 1;
            let ratio = height as f32 / row.height as f32;

            if ratio < 0.7 || ratio > 1.0 {
                continue;
            }

            if width > page.texture.width() - row.width {
                continue;
            }

            if ratio < best_ratio {
                continue;
            }

            best_ratio = ratio;
            row_idx = idx;
        };

        if row_idx == -1 {
            let row_height = height + height / 10;

            while page.next_row + row_height >= page.texture.height() ||
                width >= page.texture.width()
            {
                //TODO: check texture max size
                let mut image = Image::new();
                image.set_depth(32);
                page.texture.map_to_image(&mut image);
                page.texture.resize(page.texture.width() * 2, page.texture.height() * 2);
                page.texture.fill(0, 0, &image);
            }
            page.rows.push(PageRow {
                width: 0,
                height: row_height,
                top: page.next_row,
            });
            page.next_row += row_height;
            row_idx = page.rows.len() as i32 - 1;
        }

        let mut row = &mut page.rows[row_idx as usize];
        tex_coords = Rect::new(row.width as i32 + 1, row.top as i32 + 1, width - 2, height - 2);
        row.width += width;

        let mut image = Image::with_dimensions(rect.w as u32, rect.h as u32, 32);

        rt_glyph.draw(|x, y, v| {
            image.set_pixel(x as u32, y as u32, &Color::new(255, 255, 255, (v * 255.0) as u8));
        });

        page.texture.fill(tex_coords.x as u32, tex_coords.y as u32, &image);

        let glyph = Glyph {
            advance,
            rect,
            tex_coords,
        };

        page.glyphs.insert(char_code, glyph);
    }

    fn generate_page(&mut self, char_size: u32) {
        let mut texture = Texture::with_size(128, 128);

        if self.texture_aliasing {
            texture.set_filter_mode(super::texture::FilterMode::Bilinear);
        }

        self.pages.insert(char_size, FontPage {
            texture,
            glyphs: HashMap::new(),
            rows: Vec::new(),
            next_row: 3,
        });
    }
}
