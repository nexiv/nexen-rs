/*************************************************************************/
/*  framebuffer.rs                                                       */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use super::Texture;
use log::error;

#[derive(Clone, Copy)]
pub enum Target {
    Read,
    Draw,
    ReadAndDraw,
}

#[derive(PartialEq)]
pub enum Status {
    Null,
    Incomplete,
    Complete,
}

pub struct Framebuffer {
    fbo: gl::types::GLuint,
    target: Target,
    status: Status,
}

impl Framebuffer {
    pub fn new(target: Target) -> Self {
        let mut fbo: gl::types::GLuint = 0;
        unsafe {
            gl::GenFramebuffers(1, &mut fbo);
        }
        let status = Status::Incomplete;

        Framebuffer {
            fbo,
            target,
            status,
        }
    }

    pub fn bind(&mut self) -> &mut Self {
        if self.status == Status::Null {
            error!("Failed to bind Framebuffer: Invalid State");
        }
        unsafe {
            gl::BindFramebuffer(get_gl_type(self.target), self.fbo);
        }
        self
    }

    pub fn unbind(&mut self) -> &mut Self {
        unsafe {
            gl::BindFramebuffer(get_gl_type(self.target), 0);
        }
        self
    }

    pub fn attach_texture(&mut self, texture: &Texture) -> &mut Self {
        unsafe {
            gl::FramebufferTexture2D(
                get_gl_type(self.target),
                gl::COLOR_ATTACHMENT0,
                gl::TEXTURE_2D,
                texture.internal_id(),
                0
            );

            if gl::CheckFramebufferStatus(get_gl_type(self.target)) !=
                gl::FRAMEBUFFER_COMPLETE
            {
                error!("Framebuffer failed to attach Texture");
                self.status = Status::Incomplete;
                return self
            }
        }
        self.status = Status::Complete;
        self
    }

    pub fn detach_texture(&mut self) -> &mut Self {
        unsafe {
            gl::FramebufferTexture2D(
                get_gl_type(self.target),
                gl::COLOR_ATTACHMENT0,
                gl::TEXTURE_2D,
                0, 0
            );
        }
        self.status = Status::Incomplete;
        self
    }

    pub fn release(&mut self) {
        if self.fbo == 0 {
            return;
        }

        self.detach_texture();
        self.status = Status::Null;
        unsafe {
            gl::DeleteFramebuffers(1, &mut self.fbo);
        }
    }
}

impl Drop for Framebuffer {
    fn drop(&mut self) {
        self.release();
    }
}

fn get_gl_type(target: Target) -> gl::types::GLenum {
    match target {
        Target::Draw => gl::DRAW_FRAMEBUFFER,
        Target::Read => gl::READ_FRAMEBUFFER,
        Target::ReadAndDraw => gl::FRAMEBUFFER,
    }
}