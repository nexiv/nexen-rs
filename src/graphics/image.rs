/*************************************************************************/
/*  image.rs                                                             */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use super::color::Color;

use log::error;

pub struct Image {
    width: u32,
    height: u32,
    depth: u8,
    data: Vec<u8>,
}

impl Image {
    pub fn new() -> Self {
        Image {
            width: 0,
            height: 0,
            depth: 0,
            data: Vec::new(),
        }
    }

    pub fn from_file(path: &str) -> Self {
        let mut img = Image::new();
        img.load_from_file(path);
        img
    }

    pub fn with_dimensions(width: u32, height: u32, depth: u8) -> Self {
        Image {
            width,
            height,
            depth,
            data: vec![0 ; (width * height * (depth / 8) as u32) as usize],
        }
    }

    fn load_from_file(&mut self, path: &str) {
        match stb_image::image::load(path) {
            stb_image::image::LoadResult::ImageU8(image) => {
                self.width = image.width as u32;
                self.height = image.height as u32;
                self.depth = image.depth as u8;
                self.data = image.data;
            },
            stb_image::image::LoadResult::Error(error) => {
                error!("Error loading image \"{}\": {}", path, error);
            }
            _ => {
                error!("Unsupported image format.");
            }
        }
    }

    pub fn resize(&mut self, width: u32, height: u32) {
        self.width = width;
        self.height = height;
        self.data.resize_with((width * height * (self.depth / 8) as u32) as usize, Default::default);
    }

    pub fn set_pixel(&mut self, x: u32, y: u32, color: &Color) {
        let idx = ((y * self.width) + x) * (self.depth / 8) as u32;
        if idx + ((self.depth / 8) as u32 - 1) > self.data.len() as u32 {
            error!("Error setting pixel in image: Out of Bounds.");
            return;
        }

        self.data[idx as usize] = color.r;
        self.data[(idx + 1) as usize] = color.g;
        self.data[(idx + 2) as usize] = color.b;
        self.data[(idx + 3) as usize] = color.a;
    }

    pub fn set_depth(&mut self, depth: u8) {
        self.depth = depth;
    }

    pub fn height(&self) -> u32 {
        self.height
    }

    pub fn width(&self) -> u32 {
        self.width
    }

    pub fn mut_data(&mut self) -> &mut [u8]{
        self.data.as_mut_slice()
    }

    pub fn data(&self) -> &[u8] {
        self.data.as_slice()
    }
}
