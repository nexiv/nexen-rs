/*************************************************************************/
/*  shape2d.rs                                                           */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::ecs::Component;
use super::color::{Color, colors};

pub enum Polygon2DShape {
    Rectangle(glm::Vec2),
    Circle(f32),
    Convex(Vec<glm::Vec2>),
}

impl Polygon2DShape {
    pub fn vertices(&self) -> Vec<glm::Vec2> {
        match self {
            Polygon2DShape::Rectangle(size) => {
                vec![
                    glm::vec2(0.0, 0.0),
                    glm::vec2(size.x, 0.0),
                    glm::vec2(size.x, size.y),
                    glm::vec2(0.0, size.y),
                ]
            },
            Polygon2DShape::Convex(vertices) => {
                vertices.clone()
            },
            _ => { unimplemented!(); }
        }
    }
}

pub struct Polygon2DComponent {
    pub shape: Polygon2DShape,
    pub color: Color,
    pub border_color: Color,
    pub border: f32,
    pub z_index: i16,
    pub layer: i8,
}

impl Polygon2DComponent {
    pub fn new() -> Self {
        Polygon2DComponent {
            shape: Polygon2DShape::Rectangle(glm::vec2(0.0, 0.0)),
            color: colors::WHITE,
            border_color: colors::MAGENTA,
            border: 0.0,
            z_index: 0,
            layer: 0,
        }
    }
}

impl Component for Polygon2DComponent {}
