
/*************************************************************************/
/*  render_device.rs                                                     */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use super::color::FloatColor;
use super::buffer::{VertexBuffer, IndexBuffer};
use super::vertex_layout;
use std::ffi::CStr;
use log::info;

#[derive(Clone, Copy, PartialEq)]
pub enum DrawMode {
    Triangles,
    TriangleFan,
    Point,
    Line,
}

pub trait RenderDevice {
    fn new() -> Self;

    fn init_context(&mut self);

    fn set_viewport(&self, width: u32, height: u32);

    fn set_draw_mode(&mut self, draw_mode: DrawMode);

    fn set_clear_color(&self, color: &FloatColor);

    fn clear(&self);

    fn draw_vertex_buffer(&self, buffer: &VertexBuffer, count: u32);

    fn draw_indexed_buffer(&self, vb: &VertexBuffer, ib: &IndexBuffer);
}

#[derive(PartialEq, Clone, Copy)]
pub enum GLExtension {
    ARBExplicitAttribLocation,
    ARBSeparateShaderObjects,
}

pub struct GLRenderDevice {
    _vao: gl::types::GLuint,
    draw_mode: DrawMode,
    extensions: Vec<GLExtension>,
}

impl RenderDevice for GLRenderDevice {
    fn new() -> Self {
        let mut vao: gl::types::GLuint = 0;
        unsafe {
            gl::GenVertexArrays(1, &mut vao);
            gl::BindVertexArray(vao);
        }
        GLRenderDevice { _vao: vao, draw_mode: DrawMode::Triangles, extensions: Vec::new() }
    }

    fn init_context(&mut self) {
        unsafe {
            let mut extenstions_n = 0;
            gl::GetIntegerv(gl::NUM_EXTENSIONS, &mut extenstions_n);

            for i in 0..extenstions_n {
                #[cfg(target_arch = "aarch64")]
                let c_str = CStr::from_ptr(gl::GetStringi(gl::EXTENSIONS, i as u32) as *const u8);
                #[cfg(not(target_arch = "aarch64"))]
                let c_str = CStr::from_ptr(gl::GetStringi(gl::EXTENSIONS, i as u32) as *const i8);
                match c_str.to_str().unwrap() {
                    "GL_ARB_explicit_attrib_location" => self.extensions.push(GLExtension::ARBExplicitAttribLocation),
                    "GL_ARB_separate_shader_objects" => self.extensions.push(GLExtension::ARBSeparateShaderObjects),
                    _ => {},
                }
            }

            gl::Enable(gl::BLEND);
            gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);


            #[cfg(target_arch = "aarch64")]
            let vendor = CStr::from_ptr(gl::GetString(gl::VENDOR) as *const u8);
            #[cfg(not(target_arch = "aarch64"))]
            let vendor = CStr::from_ptr(gl::GetString(gl::VENDOR) as *const i8);

            #[cfg(target_arch = "aarch64")]
            let renderer = CStr::from_ptr(gl::GetString(gl::RENDERER) as *const u8);
            #[cfg(not(target_arch = "aarch64"))]
            let renderer = CStr::from_ptr(gl::GetString(gl::RENDERER) as *const i8);

            #[cfg(target_arch = "aarch64")]
            let version = CStr::from_ptr(gl::GetString(gl::VERSION) as * const u8);
            #[cfg(not(target_arch = "aarch64"))]
            let version = CStr::from_ptr(gl::GetString(gl::VERSION) as * const i8);

            info!("OpenGL Render Device initialized:\nVendor: {}\nRenderer: {}\nVersion: {}",
                vendor.to_str().unwrap(),
                renderer.to_str().unwrap(),
                version.to_str().unwrap()
            )
        }
    }

    fn set_draw_mode(&mut self, draw_mode: DrawMode) {
        self.draw_mode = draw_mode;
    }

    fn set_viewport(&self, width: u32, height: u32) {
        unsafe {
            gl::Viewport(0, 0, width as i32, height as i32);
        }
    }

    fn set_clear_color(&self, color: &FloatColor) {
        unsafe {
            gl::ClearColor(color.r, color.g, color.b, color.a);
        }
    }

    fn clear(&self) {
        unsafe {
            gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
        }
    }

    fn draw_vertex_buffer(&self, buffer: &VertexBuffer, count: u32) {
        buffer.bind();

        let mut i = 0;
        for element in buffer.vertex_layout().elements() {
            unsafe {
                gl::EnableVertexAttribArray(i);
                gl::VertexAttribPointer(
                    i,
                    element.component_count() as i32,
                    vertex_layout::get_gl_data_type(element.data_type),
                    element.normalized as gl::types::GLboolean,
                    buffer.vertex_layout().stride() as gl::types::GLint,
                    element.offset as *const gl::types::GLvoid,
                );
            }
            i += 1;
        }
        unsafe {
            gl::DrawArrays(_get_gl_draw_mode(self.draw_mode), 0, count as i32);
        }
    }

    fn draw_indexed_buffer(&self, vb: &VertexBuffer, ib: &IndexBuffer) {
        vb.bind();
        ib.bind();

        let mut i = 0;
        for element in vb.vertex_layout().elements() {
            unsafe {
                gl::EnableVertexAttribArray(i);
                gl::VertexAttribPointer(
                    i,
                    element.component_count() as i32,
                    vertex_layout::get_gl_data_type(element.data_type),
                    element.normalized as gl::types::GLboolean,
                    vb.vertex_layout().stride() as gl::types::GLint,
                    element.offset as *const gl::types::GLvoid,
                );
            }
            i += 1;
        }
        unsafe {
            gl::DrawElements(_get_gl_draw_mode(self.draw_mode),
                ib.count() as i32,
                gl::UNSIGNED_INT,
                std::ptr::null());
        }
    }
}

impl GLRenderDevice {
    pub fn extensions(&self) -> &[GLExtension] {
        &self.extensions
    }
}

const fn _get_gl_draw_mode(draw_mode: DrawMode) -> gl::types::GLenum {
    match draw_mode {
        DrawMode::Triangles => gl::TRIANGLES,
        DrawMode::TriangleFan => gl::TRIANGLE_FAN,
        DrawMode::Line => gl::LINES,
        DrawMode::Point => gl::POINT,
    }
}
