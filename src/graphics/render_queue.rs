/*************************************************************************/
/*  render_queue.rs                                                      */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use super::{Color, Font, ShaderProgram, Texture, sprite_batch2d::SpriteBatch2DRect};
use crate::common::FloatRect;
use atomic_refcell::AtomicRefCell;
use std::sync::Arc;

#[derive(Clone)]
pub enum RenderCommand {
    ViewportTransform(glm::Mat4),
    DrawTexturedQuad(glm::Mat4, FloatRect, Arc<AtomicRefCell<Texture>>, Color),
    DrawTexturedQuadWithShader(
        glm::Mat4,
        FloatRect,
        Arc<AtomicRefCell<Texture>>,
        Color,
        Arc<AtomicRefCell<ShaderProgram>>,
    ),
    DrawPolygon(glm::Mat4, Vec<glm::Vec2>, Color),
    DrawLine(glm::Mat4, glm::Vec2, glm::Vec2, Color),
    DrawText(glm::Mat4, String, u32, Arc<AtomicRefCell<Font>>, Color),
    DrawTextDefaultFont(glm::Mat4, String, u32, Color),
    BeginSpriteBatch(glm::Mat4, Arc<AtomicRefCell<Texture>>),
    AddBatchQuad(f32, f32, FloatRect, Color),
    DrawSpriteBatch(glm::Mat4, Arc<AtomicRefCell<Texture>>, Arc<Vec<SpriteBatch2DRect>>, Color),
    DispatchSpriteBatch(),
}

pub type RenderQueue = Vec<(RenderCommand, i16)>;
