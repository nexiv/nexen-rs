/*************************************************************************/
/*  renderer.rs                                                          */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use super::buffer::{IndexBuffer, VertexBuffer};
use super::color::{colors, Color, FloatColor};
use super::framebuffer::Target;
use super::render_device::{DrawMode, GLExtension, GLRenderDevice, RenderDevice};
use super::render_queue::{RenderCommand, RenderQueue};
use super::shader::{Shader, ShaderProgram, ShaderType};
use super::vertex_layout::VertexDataType;
use super::{CanvasLayer, Font, Framebuffer, Texture};
use crate::common::{FloatRect, Size2D};
use crate::core::application::InitArgs;
use crate::core::DataBundle;

use log::error;
use std::collections::BTreeMap;

#[derive(PartialEq, Debug)]
enum InternalShader {
    None,
    Default,
    Flat,
}

pub struct Renderer2D {
    default_shader: ShaderProgram,
    flat_shader: ShaderProgram,
    current_shader: InternalShader,
    render_device: GLRenderDevice,
    main_vbo: VertexBuffer,
    main_ibo: IndexBuffer,
    font: Font,

    model_matrix: glm::Mat4,
    view_matrix: glm::Mat4,
    projection_matrix: glm::Mat4,

    batch_vbo: VertexBuffer,
    batch_ibo: IndexBuffer,
    batch_vertex_offset: u32,
    batch_index_offset: u32,
    batch_index_val_offset: u32,
    batch_texture_size: Size2D,

    clear_color: FloatColor,
    display_size: Size2D,
    screen_framebuffer: Framebuffer,
    screen_texture: Texture,

    canvas_map: BTreeMap<i8, CanvasLayer>,
}

impl Default for Renderer2D {
    fn default() -> Self {
        Renderer2D::new()
    }
}

impl Renderer2D {
    pub fn new() -> Self {
        let mut render_device = GLRenderDevice::new();
        render_device.init_context();

        let default_shader;
        let flat_shader;

        if render_device
            .extensions()
            .contains(&GLExtension::ARBExplicitAttribLocation)
            && render_device
                .extensions()
                .contains(&GLExtension::ARBSeparateShaderObjects)
        {
            default_shader = ShaderProgram::from_shaders(&[
                Shader::from_str(include_str!("shaders/basic.vert.glsl"), ShaderType::Vertex)
                    .unwrap(),
                Shader::from_str(
                    include_str!("shaders/basic.frag.glsl"),
                    ShaderType::Fragment,
                )
                .unwrap(),
            ])
            .unwrap();
            flat_shader = ShaderProgram::from_shaders(&[
                Shader::from_str(include_str!("shaders/flat.vert.glsl"), ShaderType::Vertex)
                    .unwrap(),
                Shader::from_str(include_str!("shaders/flat.frag.glsl"), ShaderType::Fragment)
                    .unwrap(),
            ])
            .unwrap();
        } else {
            default_shader = ShaderProgram::from_shaders(&[
                Shader::from_str(
                    include_str!("shaders/basic.vert-no-ext.glsl"),
                    ShaderType::Vertex,
                )
                .unwrap(),
                Shader::from_str(
                    include_str!("shaders/basic.frag-no-ext.glsl"),
                    ShaderType::Fragment,
                )
                .unwrap(),
            ])
            .unwrap();
            flat_shader = ShaderProgram::from_shaders(&[
                Shader::from_str(
                    include_str!("shaders/flat.vert-no-ext.glsl"),
                    ShaderType::Vertex,
                )
                .unwrap(),
                Shader::from_str(
                    include_str!("shaders/flat.frag-no-ext.glsl"),
                    ShaderType::Fragment,
                )
                .unwrap(),
            ])
            .unwrap();
        }
        Renderer2D {
            render_device,
            default_shader,
            flat_shader,
            current_shader: InternalShader::None,
            main_vbo: VertexBuffer::new((std::mem::size_of::<f32>() * 9) as u32),
            main_ibo: IndexBuffer::new(),
            font: Font::from_file("res/fonts/mono.ttf"),

            model_matrix: glm::identity(),
            view_matrix: glm::identity(),
            projection_matrix: glm::identity(),

            batch_vbo: VertexBuffer::with_size((std::mem::size_of::<f32>() * 9) as u32, 1024 * 4),
            batch_ibo: IndexBuffer::with_size(6 * 1024),
            batch_vertex_offset: 0,
            batch_index_offset: 0,
            batch_index_val_offset: 0,
            batch_texture_size: Size2D::new(0, 0),

            clear_color: FloatColor::new(0.2, 0.2, 0.2, 1.0),
            display_size: Size2D::new(800, 600),
            screen_framebuffer: Framebuffer::new(Target::ReadAndDraw),
            screen_texture: Texture::with_size(800, 600),
            canvas_map: BTreeMap::new(),
        }
    }

    pub fn init(&mut self, _bundle: &mut DataBundle, args: &InitArgs) {
        self.set_display_size(args.window_size);

        self.font.set_texture_aliasing(false);

        self.default_shader.send_uniform_int("tex", 0);
        self.update_projection(
            0.0,
            args.window_size.x as f32,
            args.window_size.y as f32,
            0.0,
        );

        self.main_vbo.set_vertex_layout(&[
            (VertexDataType::Float3, false).into(),
            (VertexDataType::Float2, false).into(),
        ]);
        self.batch_vbo.set_vertex_layout(&[
            (VertexDataType::Float3, false).into(),
            (VertexDataType::Float4, false).into(),
            (VertexDataType::Float2, false).into(),
        ]);

        self.screen_framebuffer
            .bind()
            .attach_texture(&self.screen_texture)
            .unbind();
    }

    pub fn gl_extensions(&self) -> &[GLExtension] {
        &self.render_device.extensions()
    }

    pub fn set_default_font(&mut self, path: &str) {
        self.font = Font::from_file(path);
        self.font.set_texture_aliasing(false);
    }

    pub fn set_display_size(&mut self, size: Size2D) {
        self.display_size = size;
        self.screen_texture.resize(size.x, size.y);
        self.render_device.set_viewport(size.x, size.y);
    }

    pub fn set_layer_viewport_size(&mut self, viewport_size: Size2D, layer: i8) {
        self.canvas_map
            .entry(layer)
            .or_insert(CanvasLayer::new(viewport_size))
            .viewport_size = viewport_size;
    }

    pub fn set_layer_shader(&mut self, shader: ShaderProgram, layer: i8) {
        self.canvas_map
            .entry(layer)
            .or_insert(CanvasLayer::new(self.display_size))
            .shader = Some(std::rc::Rc::new(std::cell::RefCell::new(shader)));
    }

    pub fn push_render_command(&mut self, command: RenderCommand, z_index: i16, layer: i8) {
        self.canvas_map
            .entry(layer)
            .or_insert(CanvasLayer::new(self.display_size))
            .queue
            .push((command, z_index));
    }

    pub fn render(&mut self) {
        self.render_device.set_clear_color(&self.clear_color);
        self.render_device.clear();

        let mut canvas_data: Vec<(Size2D, RenderQueue, Option<std::rc::Rc<std::cell::RefCell<ShaderProgram>>>)> = Vec::with_capacity(self.canvas_map.len());
        for canvas in self.canvas_map.values_mut() {
            canvas_data.push((canvas.viewport_size, canvas.drain_commands(), canvas.shader.clone()));
        }

        //let mut commands_profiler = crate::profile::Profiler::new("Render Commands");
        //let mut render_profiler = crate::profile::Profiler::new("Render Canvases");
        for data in canvas_data {
            self.render_canvas_layer(&data.0, &data.1, data.2, /*&mut commands_profiler*/);
        }
        //commands_profiler.stamp_collected();
        //render_profiler.stamp();
    }

    pub fn render_canvas_layer(&mut self, viewport_size: &Size2D, queue: &RenderQueue, shader: Option<std::rc::Rc<std::cell::RefCell<ShaderProgram>>>, /*profiler: &mut crate::profile::Profiler*/) {
        self.update_view(&glm::identity());
        if *viewport_size != self.display_size || shader.is_some() {
            self.render_device
                .set_viewport(viewport_size.x, viewport_size.y);
            self.update_projection(0.0, viewport_size.x as f32, viewport_size.y as f32, 0.0);
            self.screen_texture.resize(viewport_size.x, viewport_size.y);
            self.screen_framebuffer.bind();
            self.render_device
                .set_clear_color(&FloatColor::new(0.0, 0.0, 0.0, 0.0));
            self.render_device.clear();
        } else {
            self.render_device
                .set_viewport(self.display_size.x, self.display_size.y);
            self.update_projection(
                0.0,
                self.display_size.x as f32,
                self.display_size.y as f32,
                0.0,
            );
        }

        let mut culling_rect = FloatRect::new(
            0.0,
            0.0,
            viewport_size.x as f32,
            viewport_size.y as f32
        );

        //profiler.reset();
        for command in queue {
            match &command.0 {
                RenderCommand::ViewportTransform(transform) => {
                    //let translation = glm::vec2(-transform[12], -transform[13]);
                    culling_rect.x = -transform[12];
                    culling_rect.y = -transform[13];
                    self.update_view(&transform);
                }
                RenderCommand::BeginSpriteBatch(transform, texture) => {
                    self.update_transform(&transform);
                    self.begin_batch_render(&*(texture.borrow()));
                }
                RenderCommand::AddBatchQuad(x, y, rect, color) => {
                    self.add_batch_quad(*x, *y, rect, &color);
                }
                RenderCommand::DispatchSpriteBatch() => {
                    self.dispatch_batch_render();
                }
                RenderCommand::DrawSpriteBatch(transform, texture, rects, color) => {
                    self.update_transform(&transform);
                    self.begin_batch_render(&*(texture.borrow()));
                    for rect in &*(*rects) {
                        if culling_rect.intersects(&FloatRect::new(
                            -transform[12] + rect.x,
                            -transform[13] + rect.y,
                            rect.rect.w,
                            rect.rect.h
                        )) {
                            self.add_batch_quad(rect.x, rect.y, &rect.rect, &color);
                        }
                    }
                    self.dispatch_batch_render();
                }
                RenderCommand::DrawTexturedQuad(transform, rect, texture, color) => {
                    if !culling_rect.intersects(&FloatRect::new(
                        -transform[12],
                        -transform[13],
                        rect.w,
                        rect.h
                    )) {
                        self.update_transform(&transform);
                        self.draw_textured_quad(rect, &*(texture.borrow()), &color, None);
                    }
                }
                RenderCommand::DrawTexturedQuadWithShader(
                    transform,
                    rect,
                    texture,
                    color,
                    shader,
                ) => {
                    if !culling_rect.intersects(&FloatRect::new(
                        -transform[12],
                        -transform[13],
                        rect.w,
                        rect.h
                    )) {
                        self.update_transform(&transform);
                        self.draw_textured_quad(rect, &*(texture.borrow()), &color, Some(&mut *(shader.borrow_mut())))
                    }
                }
                RenderCommand::DrawPolygon(transform, vertices, color) => {
                    self.update_transform(&transform);
                    self.draw_polygon(&vertices, &color);
                }
                RenderCommand::DrawLine(transform, begin, end, color) => {
                    self.update_transform(&transform);
                    self.draw_line(begin, end, &color);
                }
                RenderCommand::DrawText(transform, text, size, font, color) => {
                    self.update_transform(&transform);
                    self.draw_text(text.as_str(), *size, &color, Some(&mut font.borrow_mut()));
                }
                RenderCommand::DrawTextDefaultFont(transform, text, size, color) => {
                    self.update_transform(&transform);
                    self.draw_text_default(text.as_str(), *size, &color);
                }
            }
        }
        //profiler.collect();

        if *viewport_size != self.display_size || shader.is_some() {
            self.render_device
                .set_viewport(self.display_size.x, self.display_size.y);
            self.screen_framebuffer.unbind();
            self.screen_texture.bind();
            self.update_projection(
                0.0,
                self.display_size.x as f32,
                0.0,
                self.display_size.y as f32,
            );
            self.update_transform(&glm::identity());
            self.update_view(&glm::identity());

            self.main_vbo.set_vertex_layout(&[
                (VertexDataType::Float3, false).into(),
                (VertexDataType::Float4, false).into(),
                (VertexDataType::Float2, false).into(),
            ]);

            if let Some(shader) = shader {
                self.set_shader(&mut shader.borrow_mut());
            } else {
                self.default_shader.bind();
            }

            self.draw_screen_texture();
        }
    }

    pub fn set_clear_color(&mut self, color: &Color) {
        self.clear_color = color.into();
    }

    pub fn set_shader(&mut self, shader: &mut ShaderProgram) {
        shader.send_uniform_mat4("model", &self.model_matrix);
        shader.send_uniform_mat4("view", &self.view_matrix);
        shader.send_uniform_mat4("projection", &self.projection_matrix);
        shader.bind();
    }

    pub fn update_transform(&mut self, transform: &glm::Mat4) {
        if *transform != self.model_matrix {
            self.default_shader.send_uniform_mat4("model", transform);
            self.flat_shader.send_uniform_mat4("model", transform);
            self.model_matrix = *transform;

            match self.current_shader {
                InternalShader::Default => self.default_shader.bind(),
                InternalShader::Flat => self.flat_shader.bind(),
                _ => {},
            }
        }
    }

    pub fn update_view(&mut self, transform: &glm::Mat4) {
        if *transform != self.view_matrix {
            self.default_shader.send_uniform_mat4("view", transform);
            self.flat_shader.send_uniform_mat4("view", transform);
            self.view_matrix = *transform;

            match self.current_shader {
                InternalShader::Default => self.default_shader.bind(),
                InternalShader::Flat => self.flat_shader.bind(),
                _ => {},
            }
        }
    }

    pub fn update_projection(&mut self, left: f32, right: f32, bottom: f32, top: f32) {
        let new_projection = glm::ortho(left, right, bottom, top, 1.0, -1.0);
        if self.projection_matrix != new_projection {
            self.default_shader.send_uniform_mat4("projection", &new_projection);
            self.flat_shader.send_uniform_mat4("projection", &new_projection);
            self.projection_matrix = new_projection;

            match self.current_shader {
                InternalShader::Default => self.default_shader.bind(),
                InternalShader::Flat => self.flat_shader.bind(),
                _ => {},
            }
        }
    }

    pub fn draw_text_default(&mut self, text: &str, size: u32, color: &Color) {
        self.draw_text(&text, size, &color, None);
    }

    pub fn draw_text(&mut self, text: &str, size: u32, color: &Color, font_res: Option<&mut Font>) {
        let font;
        match font_res {
            Some(inner_font) => font = inner_font,
            None => font = &mut self.font,
        }

        if self.current_shader != InternalShader::Default {
            self.main_vbo.set_vertex_layout(&[
                (VertexDataType::Float3, false).into(),
                (VertexDataType::Float4, false).into(),
                (VertexDataType::Float2, false).into(),
            ]);
            self.default_shader.bind();
            self.current_shader = InternalShader::Default;
        }

        for c in text.chars() {
            if c == ' ' || c == '\t' || c == '\n' {
                continue;
            }
            font.get_glyph(c, size);
        }

        let page_texture = font.get_page_texture(size);
        page_texture.bind();
        let tex_size = (page_texture.width(), page_texture.height());

        self.main_vbo.resize(text.len() as u32 * 4);
        self.main_ibo.resize(text.len() as u32 * 6);

        let hspace = font.get_glyph('a', size).advance;
        let vspace = font.get_line_spacing(size);
        let mut x = 0.0;
        let mut y = vspace as f32;
        let mut vbo_offset = 0;
        let mut ibo_offset = 0;
        let mut ibo_val_offset = 0;
        let mut prev_char: u8 = 0;
        for c in text.chars() {
            x += font.get_kerning(prev_char as char, c, size);
            prev_char = c as u8;

            match c {
                ' ' => {
                    x += hspace as f32;
                    continue;
                }
                '\t' => {
                    x += hspace as f32 * 4.0;
                    continue;
                }
                '\n' => {
                    y += vspace as f32;
                    x = 0.0;
                    continue;
                }
                _ => {}
            }

            let glyph = &font.get_glyph(c, size);

            let fcolor: FloatColor = color.into();
            let pos_x = x + glyph.rect.x;
            let pos_y = y + glyph.rect.y;
            let pos_w = glyph.rect.w;
            let pos_h = glyph.rect.h;
            let tex_x = glyph.tex_coords.x as f32 / tex_size.0 as f32;
            let tex_y = glyph.tex_coords.y as f32 / tex_size.1 as f32;
            let tex_w = glyph.tex_coords.w as f32 / tex_size.0 as f32;
            let tex_h = glyph.tex_coords.h as f32 / tex_size.1 as f32;

            x += glyph.advance + glyph.rect.w / 6.0;

            let vertices: [f32; 36] = [
                pos_x,
                pos_y,
                0.0,
                fcolor.r,
                fcolor.g,
                fcolor.b,
                fcolor.a,
                tex_x,
                tex_y,
                pos_x + pos_w,
                pos_y,
                0.0,
                fcolor.r,
                fcolor.g,
                fcolor.b,
                fcolor.a,
                tex_x + tex_w,
                tex_y,
                pos_x + pos_w,
                pos_y + pos_h,
                0.0,
                fcolor.r,
                fcolor.g,
                fcolor.b,
                fcolor.a,
                tex_x + tex_w,
                tex_y + tex_h,
                pos_x,
                pos_y + pos_h,
                0.0,
                fcolor.r,
                fcolor.g,
                fcolor.b,
                fcolor.a,
                tex_x,
                tex_y + tex_h,
            ];
            let indices: [u32; 6] = [
                ibo_val_offset + 0,
                ibo_val_offset + 1,
                ibo_val_offset + 3,
                ibo_val_offset + 1,
                ibo_val_offset + 2,
                ibo_val_offset + 3,
            ];

            self.main_vbo.fill_data(vbo_offset, &vertices);
            self.main_ibo.fill_data(ibo_offset, &indices);

            vbo_offset += 36;
            ibo_offset += 6;
            ibo_val_offset += 4;
        }

        self.render_device.set_draw_mode(DrawMode::Triangles);
        self.render_device
            .draw_indexed_buffer(&self.main_vbo, &self.main_ibo);
    }

    pub fn draw_textured_quad(
        &mut self,
        rect: &FloatRect,
        texture: &Texture,
        color: &Color,
        shader: Option<&mut ShaderProgram>,
    ) {
        if self.current_shader != InternalShader::Default {
            self.main_vbo.set_vertex_layout(&[
                (VertexDataType::Float3, false).into(),
                (VertexDataType::Float4, false).into(),
                (VertexDataType::Float2, false).into(),
            ]);
            if let Some(shader) = shader {
                self.set_shader(shader);
            } else {
                self.default_shader.bind();
            }
            self.current_shader = InternalShader::Default;
        }

        let fcolor: FloatColor = color.into();
        let mut tex_rect = FloatRect {
            x: rect.x / texture.width() as f32,
            y: rect.y / texture.height() as f32,
            w: rect.w / texture.width() as f32,
            h: rect.h / texture.height() as f32,
        };

        if tex_rect.w == 0.0 && tex_rect.h == 0.0 {
            tex_rect.w = 1.0;
            tex_rect.h = 1.0;
        }

        let vertices: [f32; 36] = [
            //  X       Y       Z       RED       GREEN     BLUE      ALPHA     TexX                        TexY
            0.0,
            0.0,
            0.0,
            fcolor.r,
            fcolor.g,
            fcolor.b,
            fcolor.a,
            tex_rect.x,
            tex_rect.y,
            rect.w,
            0.0,
            0.0,
            fcolor.r,
            fcolor.g,
            fcolor.b,
            fcolor.a,
            tex_rect.x + tex_rect.w,
            tex_rect.y,
            rect.w,
            rect.h,
            0.0,
            fcolor.r,
            fcolor.g,
            fcolor.b,
            fcolor.a,
            tex_rect.x + tex_rect.w,
            tex_rect.y + tex_rect.h,
            0.0,
            rect.h,
            0.0,
            fcolor.r,
            fcolor.g,
            fcolor.b,
            fcolor.a,
            tex_rect.x,
            tex_rect.y + tex_rect.h,
        ];

        let indices: [u32; 6] = [0, 1, 3, 1, 2, 3];

        self.main_vbo.set_data(&vertices);
        self.main_ibo.set_data(&indices);

        texture.bind();
        self.render_device.set_draw_mode(DrawMode::Triangles);
        self.render_device
            .draw_indexed_buffer(&self.main_vbo, &self.main_ibo);
    }

    pub fn begin_batch_render(&mut self, texture: &Texture) {
        if self.current_shader != InternalShader::Default {
            self.default_shader.bind();
            self.current_shader = InternalShader::Default;
        }
        texture.bind();
        self.batch_texture_size = texture.size();
    }

    pub fn dispatch_batch_render(&mut self) {
        self.render_device.set_draw_mode(DrawMode::Triangles);
        self.render_device
            .draw_indexed_buffer(&self.batch_vbo, &self.batch_ibo);
        self.batch_vertex_offset = 0;
        self.batch_index_offset = 0;
        self.batch_index_val_offset = 0;
        self.batch_texture_size = Size2D::new(0, 0);
    }

    pub fn add_batch_quad(&mut self, x: f32, y: f32, rect: &FloatRect, color: &Color) {
        let fcolor: FloatColor = color.into();
        let tex_rect = FloatRect {
            x: rect.x / self.batch_texture_size.x as f32,
            y: rect.y / self.batch_texture_size.y as f32,
            w: rect.w / self.batch_texture_size.x as f32,
            h: rect.h / self.batch_texture_size.y as f32,
        };
        let vertices: [f32; 36] = [
            //  X           Y           Z       RED       GREEN     BLUE      ALPHA     TexX                        TexY
            x,
            y,
            0.0,
            fcolor.r,
            fcolor.g,
            fcolor.b,
            fcolor.a,
            tex_rect.x,
            tex_rect.y,
            x + rect.w,
            y,
            0.0,
            fcolor.r,
            fcolor.g,
            fcolor.b,
            fcolor.a,
            tex_rect.x + tex_rect.w,
            tex_rect.y,
            x + rect.w,
            y + rect.h,
            0.0,
            fcolor.r,
            fcolor.g,
            fcolor.b,
            fcolor.a,
            tex_rect.x + tex_rect.w,
            tex_rect.y + tex_rect.h,
            x,
            y + rect.h,
            0.0,
            fcolor.r,
            fcolor.g,
            fcolor.b,
            fcolor.a,
            tex_rect.x,
            tex_rect.y + tex_rect.h,
        ];

        let indices: [u32; 6] = [
            self.batch_index_val_offset,
            self.batch_index_val_offset + 1,
            self.batch_index_val_offset + 3,
            self.batch_index_val_offset + 1,
            self.batch_index_val_offset + 2,
            self.batch_index_val_offset + 3,
        ];

        self.batch_vbo
            .fill_data(self.batch_vertex_offset, &vertices);
        self.batch_ibo.fill_data(self.batch_index_offset, &indices);

        self.batch_vertex_offset += 36;
        self.batch_index_offset += 6;
        self.batch_index_val_offset += 4;
    }

    pub fn draw_screen_texture(&mut self) {
        /*if self.current_shader != InternalShader::Default {
            self.main_vbo.set_vertex_layout(&[
                (VertexDataType::Float3, false).into(),
                (VertexDataType::Float4, false).into(),
                (VertexDataType::Float2, false).into(),
            ]);
            self.default_shader.bind();
            self.current_shader = InternalShader::Default;
        }*/

        let rect = FloatRect::new(
            0.0,
            0.0,
            self.display_size.x as f32,
            self.display_size.y as f32,
        );
        let tex_rect = FloatRect::new(0.0, 0.0, 1.0, 1.0);
        let fcolor: FloatColor = colors::WHITE.into();

        let vertices: [f32; 36] = [
            //  X       Y       Z       RED       GREEN     BLUE      ALPHA     TexX                        TexY
            0.0,
            0.0,
            0.0,
            fcolor.r,
            fcolor.g,
            fcolor.b,
            fcolor.a,
            tex_rect.x,
            tex_rect.y,
            rect.w,
            0.0,
            0.0,
            fcolor.r,
            fcolor.g,
            fcolor.b,
            fcolor.a,
            tex_rect.x + tex_rect.w,
            tex_rect.y,
            rect.w,
            rect.h,
            0.0,
            fcolor.r,
            fcolor.g,
            fcolor.b,
            fcolor.a,
            tex_rect.x + tex_rect.w,
            tex_rect.y + tex_rect.h,
            0.0,
            rect.h,
            0.0,
            fcolor.r,
            fcolor.g,
            fcolor.b,
            fcolor.a,
            tex_rect.x,
            tex_rect.y + tex_rect.h,
        ];

        let indices: [u32; 6] = [0, 1, 3, 1, 2, 3];

        self.main_vbo.set_data(&vertices);
        self.main_ibo.set_data(&indices);

        self.render_device.set_draw_mode(DrawMode::Triangles);
        self.render_device
            .draw_indexed_buffer(&self.main_vbo, &self.main_ibo);
    }

    pub fn draw_line(&mut self, begin: &glm::Vec2, end: &glm::Vec2, color: &Color) {
        if self.current_shader != InternalShader::Flat {
            self.main_vbo.set_vertex_layout(&[
                (VertexDataType::Float3, false).into(),
                (VertexDataType::Float4, false).into(),
            ]);
            self.flat_shader.bind();
            self.current_shader = InternalShader::Flat;
        }

        let colorf: FloatColor = color.into();

        let vertices = [
            begin.x, begin.y, 0.0, colorf.r, colorf.g, colorf.b, colorf.a, end.x, end.y, 0.0,
            colorf.r, colorf.g, colorf.b, colorf.a,
        ];

        self.render_device.set_draw_mode(DrawMode::Line);
        self.main_vbo.set_data(&vertices);

        self.render_device.draw_vertex_buffer(&self.main_vbo, 2);
    }

    pub fn draw_polygon(&mut self, polygon: &Vec<glm::Vec2>, color: &Color) {
        if polygon.len() < 3 {
            error!("Can't draw polygon with less than 3 vertices");
            return;
        }
        if self.current_shader != InternalShader::Flat {
            self.main_vbo.set_vertex_layout(&[
                (VertexDataType::Float3, false).into(),
                (VertexDataType::Float4, false).into(),
            ]);
            self.flat_shader.bind();
            self.current_shader = InternalShader::Flat;
        }

        let colorf: FloatColor = color.into();
        let mut vertices = vec![0.0; polygon.len() * 7];

        for i in 0..polygon.len() {
            vertices[i * 7 + 0] = polygon[i].x;
            vertices[i * 7 + 1] = polygon[i].y;
            vertices[i * 7 + 2] = 0.0;
            vertices[i * 7 + 3] = colorf.r;
            vertices[i * 7 + 4] = colorf.g;
            vertices[i * 7 + 5] = colorf.b;
            vertices[i * 7 + 6] = colorf.a;
        }

        if polygon.len() > 4 {
            self.render_device.set_draw_mode(DrawMode::TriangleFan);
        } else {
            self.render_device.set_draw_mode(DrawMode::Triangles);
        }

        self.main_vbo.set_data(&vertices);

        if polygon.len() == 4 {
            let indices: [u32; 6] = [0, 1, 3, 1, 2, 3];
            self.main_ibo.set_data(&indices);
            self.render_device
                .draw_indexed_buffer(&self.main_vbo, &self.main_ibo);
        } else {
            self.render_device
                .draw_vertex_buffer(&self.main_vbo, polygon.len() as u32);
        }
    }
}
