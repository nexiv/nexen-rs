/*************************************************************************/
/*  shader.rs                                                            */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use log::debug;
use std::collections::HashMap;
use std::ffi::{CStr, CString};

#[derive(Debug)]
pub enum ShaderType {
    Vertex,
    Fragment,
}

pub struct Shader {
    id: gl::types::GLuint,
}

impl Shader {
    pub fn from_path(path: &str, kind: ShaderType) -> Result<Shader, String> {
        debug!("Loading {:?} Shader: {}", kind, path);

        match std::fs::read_to_string(path) {
            Ok(contents) => Shader::from_str(&contents, kind),
            Err(error) => Err(format!("{}", error)),
        }
    }

    pub fn from_str(src: &str, kind: ShaderType) -> Result<Shader, String> {
        let id = shader_from_source(&CString::new(src).unwrap(), kind)?;
        Ok(Shader { id })
    }

    pub fn id(&self) -> gl::types::GLuint {
        self.id
    }
}

impl Drop for Shader {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteShader(self.id);
        }
    }
}

pub struct ShaderProgram {
    id: gl::types::GLuint,
    last_location: (String, gl::types::GLint),
    location_cache: HashMap<String, gl::types::GLint>,
}

impl ShaderProgram {
    pub fn from_shaders(shaders: &[Shader]) -> Result<ShaderProgram, String> {
        let program_id = unsafe { gl::CreateProgram() };

        for shader in shaders {
            unsafe {
                gl::AttachShader(program_id, shader.id());
            }
        }

        unsafe {
            gl::LinkProgram(program_id);
        }

        let mut success: gl::types::GLint = 1;
        unsafe {
            gl::GetProgramiv(program_id, gl::LINK_STATUS, &mut success);
        }

        if success == 0 {
            let mut len: gl::types::GLint = 0;
            unsafe {
                gl::GetProgramiv(program_id, gl::INFO_LOG_LENGTH, &mut len);
            }

            let error = whitespace_cstring(len as usize);

            unsafe {
                gl::GetProgramInfoLog(
                    program_id,
                    len,
                    std::ptr::null_mut(),
                    error.as_ptr() as *mut gl::types::GLchar,
                );
            }

            return Err(error.to_string_lossy().into_owned());
        }

        for shader in shaders {
            unsafe {
                gl::DetachShader(program_id, shader.id());
            }
        }

        Ok(ShaderProgram {
            id: program_id,
            last_location: (String::new(), 0),
            location_cache: HashMap::new(),
        })
    }

    pub fn send_uniform_int(&mut self, name: &str, i: i32) {
        unsafe {
            gl::UseProgram(self.id);
            gl::Uniform1i(self.get_location(name), i);
            gl::UseProgram(0);
        }
    }

    pub fn send_uniform_mat4(&mut self, name: &str, matrix: &glm::Mat4) {
        unsafe {
            gl::UseProgram(self.id);
            gl::UniformMatrix4fv(
                self.get_location(name),
                1,
                gl::FALSE,
                matrix.as_ptr(),
            );
        }
    }

    fn get_location(&mut self, name: &str) -> gl::types::GLint {
        if name == self.last_location.0.as_str() {
            return self.last_location.1;
        }

        if let Some(location) = self.location_cache.get(&name.to_owned()) {
            self.last_location = (name.to_owned(), *location);
            return *location;
        }

        let location;
        let c_name = &CString::new(name).unwrap();
        unsafe {
            location = gl::GetUniformLocation(self.id, c_name.as_ptr());
        }

        self.last_location = (name.to_owned(), location);
        self.location_cache.insert(name.to_owned(), location);
        location
    }

    pub fn bind(&self) {
        unsafe {
            gl::UseProgram(self.id);
        }
    }
}

impl Drop for ShaderProgram {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteProgram(self.id);
        }
    }
}

fn shader_from_source(src: &CStr, kind: ShaderType) -> Result<gl::types::GLuint, String> {
    let gl_kind = match kind {
        ShaderType::Vertex => gl::VERTEX_SHADER,
        ShaderType::Fragment => gl::FRAGMENT_SHADER,
    };

    let id = unsafe { gl::CreateShader(gl_kind) };
    unsafe {
        gl::ShaderSource(id, 1, &src.as_ptr(), std::ptr::null());
        gl::CompileShader(id);
    }

    let mut success: gl::types::GLint = 1;
    unsafe {
        gl::GetShaderiv(id, gl::COMPILE_STATUS, &mut success);
    }

    if success == 0 {
        let mut len: gl::types::GLint = 0;
        unsafe {
            gl::GetShaderiv(id, gl::INFO_LOG_LENGTH, &mut len);
        }

        let error = whitespace_cstring(len as usize);

        unsafe {
            gl::GetShaderInfoLog(
                id,
                len,
                std::ptr::null_mut(),
                error.as_ptr() as *mut gl::types::GLchar,
            );
        }

        return Err(error.to_string_lossy().into_owned());
    }

    Ok(id)
}

fn whitespace_cstring(len: usize) -> CString {
    let mut buffer: Vec<u8> = Vec::with_capacity(len + 1);
    buffer.extend([b' '].iter().cycle().take(len));
    unsafe { CString::from_vec_unchecked(buffer) }
}
