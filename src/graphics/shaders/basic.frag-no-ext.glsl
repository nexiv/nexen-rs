#version 120

varying vec4 out_color;

uniform sampler2D tex;

void main()
{
    gl_FragColor = texture2D(tex, gl_TexCoord[0].st) * out_color;
}
