#version 130
#extension GL_ARB_explicit_attrib_location : enable
#extension GL_ARB_separate_shader_objects : enable

in vec2 out_uv;
in vec4 out_color;

out vec4 color;

uniform sampler2D tex;

void main()
{
    color = texture(tex, out_uv) * out_color;
}
