#version 130
#extension GL_ARB_explicit_attrib_location : enable
#extension GL_ARB_separate_shader_objects : enable

in vec4 out_color;

out vec4 color;

void main()
{
    color = out_color;
}
