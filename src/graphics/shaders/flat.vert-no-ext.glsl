#version 120

attribute vec3 att_pos;
attribute vec4 att_color;

varying vec4 out_color;

uniform mat4 projection = mat4(1.0);
uniform mat4 model = mat4(1.0);
uniform mat4 view = mat4(1.0);

void main()
{
    gl_Position = projection * view * model * vec4(att_pos, 1.0);
    out_color = att_color;
}
