#version 130
#extension GL_ARB_explicit_attrib_location : enable
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec3 att_pos;
layout (location = 1) in vec4 att_color;

out vec4 out_color;

uniform mat4 projection = mat4(1.0);
uniform mat4 model = mat4(1.0);
uniform mat4 view = mat4(1.0);

void main()
{
    gl_Position = projection * view * model * vec4(att_pos, 1.0);
    out_color = att_color;
}
