/*************************************************************************/
/*  sprite2d.rs                                                          */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use super::{
    color::{colors, Color},
    ShaderProgram, Texture,
};
use crate::common::FloatRect;
use crate::ecs::Component;
use atomic_refcell::AtomicRefCell;
use std::sync::Arc;

pub struct Sprite2DComponent {
    pub texture: Arc<AtomicRefCell<Texture>>,
    pub shader: Option<Arc<AtomicRefCell<ShaderProgram>>>,
    pub rect: FloatRect,
    pub rect_index: u32,
    pub spritesheet_rows: u32,
    pub spritesheet_columns: u32,
    pub color: Color,
    pub z_index: i16,
    pub layer: i8,
}

impl Sprite2DComponent {
    pub fn new(texture: Arc<AtomicRefCell<Texture>>) -> Self {
        let texture_size = texture.borrow().size();
        Sprite2DComponent {
            texture,
            shader: None,
            rect: FloatRect::new(0.0, 0.0, texture_size.x as f32, texture_size.y as f32),
            rect_index: 0,
            spritesheet_rows: 1,
            spritesheet_columns: 1,
            color: colors::WHITE,
            z_index: 0,
            layer: 0,
        }
    }
}

impl Component for Sprite2DComponent {}
