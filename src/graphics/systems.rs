/*************************************************************************/
/*  systems.rs                                                           */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
pub mod sprite2d_system;
pub use sprite2d_system::Sprite2DSystem as Sprite2DSystem;

pub mod polygon2d_system;
pub use polygon2d_system::Polygon2DSystem as Polygon2DSystem;

pub mod camera2d_system;
pub use camera2d_system::Camera2DSystem as Camera2DSystem;

pub mod text2d_system;
pub use text2d_system::Text2DSystem as Text2DSystem;