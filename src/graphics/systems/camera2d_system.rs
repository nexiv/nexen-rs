/*************************************************************************/
/*  camera2D_system.rs                                                   */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::common::Size2D;
use crate::core::event::Event;
use crate::core::DataBundle;
use crate::core::InitArgs;
use crate::ecs::{Entity, System, AsyncSystem};
use crate::graphics::{
    render_queue::RenderCommand, Camera2DComponent, CameraMode, Parallax2DComponent, Renderer2D,
    TransformComponent,
};

pub struct Camera2DSystem {
    screen_viewport: Size2D,
    current_camera: Entity,
    _view_transform: glm::Mat4,

    #[cfg(debug_assertions)]
    _debug_transform: glm::Mat4,
    #[cfg(debug_assertions)]
    _debug_controls: bool,
    #[cfg(debug_assertions)]
    _debug_mouse_clicked: bool,
}

impl Default for Camera2DSystem {
    fn default() -> Self {
        Camera2DSystem {
            screen_viewport: Size2D::new(1280, 720),
            current_camera: Entity::null(),
            _view_transform: glm::identity(),

            #[cfg(debug_assertions)]
            _debug_transform: glm::identity(),

            #[cfg(debug_assertions)]
            _debug_controls: false,

            #[cfg(debug_assertions)]
            _debug_mouse_clicked: false,
        }
    }
}

impl System for Camera2DSystem {
    fn init(&mut self, _bundle: &mut DataBundle, _init_args: &InitArgs) {
        //self.screen_viewport = init_args.window_size
    }

    fn update(&mut self, bundle: &DataBundle, delta: f32) {
        let world = bundle.world();
        let entities = world.entities();
        let transforms = world.component_storage::<TransformComponent>();
        let cameras = world.component_storage::<Camera2DComponent>();

        for entity in entities {
            if let Some(camera_and_transform) = cameras.join_write_read(&transforms, *entity) {
                let mut camera = camera_and_transform.0;
                let transform = camera_and_transform.1;

                camera.viewport = self.screen_viewport;

                if camera.active && entity.id != self.current_camera.id {
                    self.current_camera = *entity;
                }

                #[cfg(debug_assertions)]
                if self._debug_controls {
                    continue;
                }

                match camera.mode {
                    CameraMode::Immediate => {
                        if camera.round_camera_coords {
                            self._view_transform = glm::translation(&glm::vec3(
                                -transform.translation.x.round() + (camera.viewport.x / 2) as f32,
                                -transform.translation.y.round() + (camera.viewport.y / 2) as f32,
                                0.0,
                            ));
                        } else {
                            self._view_transform = glm::translation(&glm::vec3(
                                -transform.translation.x + (camera.viewport.x / 2) as f32,
                                -transform.translation.y + (camera.viewport.y / 2) as f32,
                                0.0,
                            ));
                        }
                    }
                    CameraMode::FollowEntity => {
                        if transform.translation().x
                            > camera.position.x + (camera.viewport.x as f32 * camera.follow_active_range.x)
                        {
                            let difference = transform.translation().x
                                - (camera.position.x + (camera.viewport.x as f32 * camera.follow_active_range.x));
                            camera.position.x += difference * delta * camera.follow_acceleration.x;
                        } else if transform.translation().x
                            < camera.position.x + (camera.viewport.x as f32 * (1.0 - camera.follow_active_range.x))
                        {
                            let difference = (camera.position.x + (camera.viewport.x as f32 * (1.0 - camera.follow_active_range.x)))
                                - transform.translation().x;
                            camera.position.x -= difference * delta * camera.follow_acceleration.x;
                        }
                        if transform.translation().y
                            > camera.position.y + (camera.viewport.y as f32 * camera.follow_active_range.y)
                        {
                            let difference = transform.translation().y
                                - (camera.position.y + (camera.viewport.y as f32 * camera.follow_active_range.y));
                            camera.position.y += difference * delta * camera.follow_acceleration.y;
                        } else if transform.translation().y
                            < camera.position.y + (camera.viewport.y as f32 * (1.0 - camera.follow_active_range.y))
                        {
                            let difference = (camera.position.y + (camera.viewport.y as f32 * (1.0 - camera.follow_active_range.y)))
                                - transform.translation().y;
                            camera.position.y -= difference * delta * camera.follow_acceleration.y;
                        }
                        // Apply limits if enabled
                        if let Some(limits) = camera.limits {
                            // Apply left limit
                            if camera.position.x < limits.x {
                                camera.position.x += limits.x - camera.position.x;
                            }
                            // Apply right limit
                            if camera.position.x + camera.viewport.x as f32 > limits.w {
                                camera.position.x -=
                                    (camera.position.x + camera.viewport.x as f32) - limits.w;
                            }
                            // Apply bottom limit
                            if camera.position.y + camera.viewport.y as f32 > limits.h {
                                camera.position.y -=
                                    (camera.position.y + camera.viewport.y as f32) - limits.h;
                            }
                        }

                        if camera.round_camera_coords {
                            self._view_transform = glm::translation(&glm::vec3(
                                -camera.position.x.round(),
                                -camera.position.y.round(),
                                0.0,
                            ));
                        } else {
                            self._view_transform = glm::translation(&glm::vec3(
                                -camera.position.x,
                                -camera.position.y,
                                0.0,
                            ));
                        }
                    }
                }
            }
        }
    }

    fn render(&self, bundle: &DataBundle, renderer: &mut Renderer2D) {
        if self.current_camera == Entity::null() {
            return;
        }

        #[cfg(debug_assertions)]
        {
            if self._debug_controls {
                for layer_idx in bundle
                    .world()
                    .get_write_component::<Camera2DComponent>(self.current_camera)
                    .unwrap()
                    .layers
                    .clone()
                {
                    renderer.push_render_command(
                        RenderCommand::ViewportTransform(self._debug_transform),
                        -500,
                        layer_idx,
                    );
                }
                renderer.push_render_command(
                    RenderCommand::ViewportTransform(self._debug_transform),
                    -500,
                    126,
                );
                return;
            }
        }

        let world = bundle.world();
        let camera = world
            .get_write_component::<Camera2DComponent>(self.current_camera)
            .unwrap();

        for layer_idx in camera.layers.clone() {
            renderer.push_render_command(
                RenderCommand::ViewportTransform(self._view_transform),
                -500,
                layer_idx,
            );
        }

        let parallaxes = bundle.world().component_storage::<Parallax2DComponent>();
        for parallax in &mut *parallaxes.data_mut() {
            let parallax_view = glm::translation(
                &glm::vec3(
                    (-camera.position.x * parallax.factor.x).round(),
                    (-camera.position.y * parallax.factor.y).round(),
                    0.0,
                ),
            );

            renderer.push_render_command(
                RenderCommand::ViewportTransform(parallax_view),
                -500,
                parallax.layer,
            );
        }

        renderer.push_render_command(
            RenderCommand::ViewportTransform(self._view_transform),
            -500,
            126,
        );
    }

    #[cfg(debug_assertions)]
    fn handle_events(&mut self, _bundle: &mut DataBundle, event: &Event) {
        use crate::platform::keyboard::Keycode;
        use crate::platform::mouse::MouseButton;
        use crate::platform::WindowEvent;
        match event {
            Event::Window(window_event) => match window_event {
                WindowEvent::MouseMoved(mouse_move) => {
                    if !self._debug_mouse_clicked {
                        return;
                    }
                    self._debug_transform = glm::translate(
                        &self._debug_transform,
                        &glm::vec3(
                            (mouse_move.x_rel as f32 * 0.5).round(),
                            (mouse_move.y_rel as f32 * 0.5).round(),
                            0.0,
                        ),
                    );
                }
                WindowEvent::MouseButtonPressed(mouse_btn, ..) => {
                    if *mouse_btn == MouseButton::Left {
                        self._debug_mouse_clicked = true;
                    }
                }
                WindowEvent::MouseButtonReleased(mouse_btn, ..) => {
                    if *mouse_btn == MouseButton::Left {
                        self._debug_mouse_clicked = false;
                    }
                }
                WindowEvent::MouseWheelMoved(_, _, y) => {
                    if *y > 0 {
                        self._debug_transform =
                            glm::scale(&self._debug_transform, &glm::vec3(1.1, 1.1, 1.0));
                    } else if *y < 0 {
                        self._debug_transform =
                            glm::scale(&self._debug_transform, &glm::vec3(0.9, 0.9, 1.0));
                    }
                }
                WindowEvent::KeyReleased(key) => {
                    if key.code == Keycode::F {
                        self._debug_controls = !self._debug_controls;

                        if self._debug_controls {
                            self._debug_transform = self._view_transform;
                        }
                    }
                }
                _ => {}
            },
            _ => {}
        }
    }
}

impl AsyncSystem for Camera2DSystem {}
