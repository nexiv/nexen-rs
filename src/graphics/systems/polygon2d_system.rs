/*************************************************************************/
/*  polygon2D_system.rs                                                  */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::core::event::Event;
use crate::core::DataBundle;
use crate::core::InitArgs;
use crate::ecs::{System, AsyncSystem};
use crate::graphics::{
    render_queue::RenderCommand, Polygon2DComponent, Renderer2D, TransformComponent,
};

pub struct Polygon2DSystem;

impl Default for Polygon2DSystem {
    fn default() -> Self {
        Polygon2DSystem
    }
}

impl System for Polygon2DSystem {
    fn init(&mut self, _bundle: &mut DataBundle, _args: &InitArgs) {}

    fn update(&mut self, _bundle: &DataBundle, _delta: f32) {}

    fn render(&self, bundle: &DataBundle, renderer: &mut Renderer2D) {
        let world = bundle.world();
        let entities = world.entities();
        let polygons = world.component_storage::<Polygon2DComponent>();
        let transforms = world.component_storage::<TransformComponent>();

        // Update shapes
        for entity in entities {
            if let Some(polygon) = polygons.read(*entity) {
                let mut model_transform = glm::identity();
                if let Some(transform) = transforms.read(*entity) {
                    model_transform = transform.matrix().clone();
                }

                if polygon.border == 0.0 {
                    renderer.push_render_command(
                        RenderCommand::DrawPolygon(
                            model_transform,
                            polygon.shape.vertices().clone(),
                            polygon.color,
                        ),
                        polygon.z_index,
                        polygon.layer
                    );
                } else {
                    let mut border_vertices: Vec<glm::Vec2> = Vec::new();

                    for vertice in polygon.shape.vertices() {
                        let new_length = glm::length(&vertice) + polygon.border;
                        border_vertices.push(glm::vec2(
                            vertice.x * new_length / glm::length(&vertice),
                            vertice.y * new_length / glm::length(&vertice),
                        ));
                    }

                    renderer.push_render_command(
                        RenderCommand::DrawPolygon(
                            model_transform,
                            border_vertices.clone(),
                            polygon.border_color,
                        ),
                        polygon.z_index,
                        polygon.layer
                    );
                    renderer.push_render_command(
                        RenderCommand::DrawPolygon(
                            model_transform,
                            polygon.shape.vertices().clone(),
                            polygon.color,
                        ),
                        polygon.z_index,
                        polygon.layer
                    );
                }
            }
        }
    }

    fn handle_events(&mut self, _bundle: &mut DataBundle, _event: &Event) {}
}

impl AsyncSystem for Polygon2DSystem {}
