/*************************************************************************/
/*  sprite2D_system.rs                                                   */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::common::FloatRect;
use crate::core::event::Event;
use crate::core::DataBundle;
use crate::core::InitArgs;
use crate::ecs::{System, AsyncSystem};
use crate::graphics::{
    render_queue::RenderCommand, Renderer2D, Sprite2DComponent, SpriteBatch2DComponent,
    TransformComponent,
};

pub struct Sprite2DSystem;

impl Default for Sprite2DSystem {
    fn default() -> Self {
        Sprite2DSystem
    }
}

impl System for Sprite2DSystem {
    fn init(&mut self, _bundle: &mut DataBundle, _args: &InitArgs) {}

    fn update(&mut self, _bundle: &DataBundle, _delta: f32) {}

    fn render(&self, bundle: &DataBundle, renderer: &mut Renderer2D) {
        let world = bundle.world();
        let entities = world.entities();
        let sprites = world.component_storage::<Sprite2DComponent>();
        let sprite_batches = world.component_storage::<SpriteBatch2DComponent>();
        let transforms = world.component_storage::<TransformComponent>();

        // Update sprites
        for entity in entities {
            if let Some(sprite_and_transform) = sprites.join_read_both(&transforms, *entity) {
                let sprite = sprite_and_transform.0;
                let transform = sprite_and_transform.1;

                let rect = FloatRect::new(
                    (sprite.rect_index % sprite.spritesheet_columns) as f32 * sprite.rect.w,
                    (sprite.rect_index / sprite.spritesheet_columns) as f32 * sprite.rect.h,
                    sprite.rect.w,
                    sprite.rect.h,
                );

                let model_transform = glm::translate(
                    &transform.matrix(),
                    &glm::vec3(-(rect.w / 2.0), -(rect.h / 2.0), 0.0),
                );
                if let Some(shader) = &sprite.shader {
                    renderer.push_render_command(
                        RenderCommand::DrawTexturedQuadWithShader(
                            model_transform,
                            rect,
                            sprite.texture.clone(),
                            sprite.color,
                            shader.clone(),
                        ),
                        sprite.z_index,
                        sprite.layer
                    );
                } else {
                    renderer.push_render_command(
                        RenderCommand::DrawTexturedQuad(
                            model_transform,
                            rect,
                            sprite.texture.clone(),
                            sprite.color,
                        ),
                        sprite.z_index,
                        sprite.layer
                    );
                }
            }

            if let Some(batch_and_transform) = sprite_batches.join_read_both(&transforms, *entity) {
                let batch = batch_and_transform.0;
                let transform = batch_and_transform.1;

                /*renderer.push_render_command(
                    RenderCommand::BeginSpriteBatch(transform.matrix().clone(), batch.texture.clone()),
                    batch.z_index,
                    batch.layer
                );

                for rect in &*batch.rects {
                    renderer.push_render_command(
                        RenderCommand::AddBatchQuad(
                            rect.x,
                            rect.y,
                            rect.rect,
                            batch.color
                        ),
                        batch.z_index,
                        batch.layer
                    )
                }

                renderer.push_render_command(
                    RenderCommand::DispatchSpriteBatch(),
                    batch.z_index,
                    batch.layer,
                );*/

                renderer.push_render_command(
                    RenderCommand::DrawSpriteBatch(
                        transform.matrix().clone(),
                        batch.texture.clone(),
                        batch.rects.clone(),
                        batch.color,
                    ),
                    batch.z_index,
                    batch.layer,
                );
            }
        }
    }

    fn handle_events(&mut self, _bundle: &mut DataBundle, _event: &Event) {}
}

impl AsyncSystem for Sprite2DSystem {}
