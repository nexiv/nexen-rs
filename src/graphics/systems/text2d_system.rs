/*************************************************************************/
/*  text2D_system.rs                                                     */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::core::DataBundle;
use crate::ecs::{System, AsyncSystem};
use crate::graphics::{
    render_queue::RenderCommand, Renderer2D, Text2DComponent, TransformComponent,
};

pub struct Text2DSystem;

impl Default for Text2DSystem {
    fn default() -> Self {
        Text2DSystem
    }
}

impl System for Text2DSystem {
    fn update(&mut self, _bundle: &DataBundle, _delta: f32) {}

    fn render(&self, bundle: &DataBundle, renderer: &mut Renderer2D) {
        let world = bundle.world();
        let entities = world.entities();
        let texts = world.component_storage::<Text2DComponent>();
        let transforms = world.component_storage::<TransformComponent>();

        // Update texts
        for entity in entities {
            if let Some(text) = texts.read(*entity) {
                let model_transform;
                if let Some(transform) = transforms.read(*entity) {
                    model_transform = transform.matrix().clone();
                } else {
                    model_transform = glm::identity();
                }

                renderer.push_render_command(
                    RenderCommand::DrawText(
                        model_transform,
                        text.text.clone(),
                        text.size,
                        text.font.clone(),
                        text.color,
                    ),
                    text.z_index,
                    text.layer
                );
            }
        }
    }
}

impl AsyncSystem for Text2DSystem {}
