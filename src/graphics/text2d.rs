/*************************************************************************/
/*  text2d.rs                                                              */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::ecs::Component;
use super::{
    color::{Color, colors},
    font::Font,
};
use std::sync::Arc;
use atomic_refcell::AtomicRefCell;

pub struct Text2DComponent {
    pub font: Arc<AtomicRefCell<Font>>,
    pub text: String,
    pub size: u32,
    pub color: Color,
    pub z_index: i16,
    pub layer: i8,
}

impl Component for Text2DComponent {}

impl Text2DComponent {
    pub fn new(font: Arc<AtomicRefCell<Font>>) -> Self {
        Text2DComponent {
            font,
            text: String::new(),
            size: 16,
            color: colors::WHITE,
            z_index: 0,
            layer: 0,
        }
    }
}
