/*************************************************************************/
/*  texture.rs                                                           */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::common::Size2D;
use crate::resources::{ResourceLoader, Resource};
use super::image::Image;
use log::{info, error};

#[derive(Debug, Copy, Clone)]
pub enum FilterMode {
    Nearest = 0,
    Bilinear,
    Trilinear,
    Anisotropic,
    NearestAnisotropic,
}

#[derive(Debug, Copy, Clone)]
pub enum WrapMode {
    Wrap = 0,
    Mirror,
    Clamp,
    Border,
}

#[derive(Debug, Copy, Clone)]
pub enum Usage {
    Static = 0,
    Dynamic,
    RenderTarget,
    DepthStencil,
}

#[derive(Debug, Copy, Clone)]
pub enum Format {
    Rgba,
    Bgra,
    Red,
    Green,
    Blue,
    Alpha,
}

struct GLFilter {
    min: gl::types::GLenum,
    mag: gl::types::GLenum,
}

struct GLWrap {
    s: gl::types::GLenum,
    t: gl::types::GLenum,
}

#[derive(Default)]
pub struct DefaultTextureLoader;

impl ResourceLoader<Texture> for DefaultTextureLoader {
    fn from_file(&self, path: &str) -> Texture {
        Texture::from_image(&Image::from_file(path))
    }
}

pub struct Texture {
    id: gl::types::GLuint,
    filter_mode: FilterMode,
    wrap_mode: WrapMode,
    _usage: Usage,
    format: Format,
    width: u32,
    height: u32,
}

impl Resource for Texture {}

impl Texture {

    pub fn new() -> Self {
        let mut id: gl::types::GLuint = 0;
        unsafe {
            gl::GenTextures(1, &mut id);
            gl::BindTexture(gl::TEXTURE_2D, id);
            gl::PixelStorei(gl::UNPACK_ALIGNMENT, 1);
            gl::BindTexture(gl::TEXTURE_2D, 0);
        }
        let mut tex = Texture {
            id,
            filter_mode: FilterMode::Nearest,
            wrap_mode: WrapMode::Wrap,
            _usage: Usage::Static,
            format: Format::Rgba,
            width: 0,
            height: 0,
        };
        tex.set_filter_mode(FilterMode::Nearest);
        tex.set_wrap_mode(WrapMode::Wrap);
        tex
    }

    pub fn with_size(width: u32, height: u32) -> Self {
        let mut texture = Texture::new();
        texture.resize(width, height);
        texture
    }

    pub fn from_image(image: &Image) -> Self {
        let mut texture = Texture::new();
        texture.load_from_image(&image);
        texture
    }

    pub fn load_from_image(&mut self, image: &Image) {
        self.width = image.width();
        self.height = image.height();

        unsafe {
            gl::BindTexture(gl::TEXTURE_2D, self.id);
            gl::TexImage2D(
                gl::TEXTURE_2D,
                0,
                get_gl_format(self.format) as i32,
                self.width as i32,
                self.height as i32,
                0,
                get_gl_format(self.format),
                gl::UNSIGNED_BYTE,
                image.data().as_ptr() as *const gl::types::GLvoid
            );
            //gl::GenerateMipmap(gl::TEXTURE_2D);
            gl::BindTexture(gl::TEXTURE_2D, 0);
        }
    }

    pub fn fill(&mut self, x_offset: u32, y_offset: u32, image: &Image) {
        if image.width() > self.width || image.height() > self.height {
            error!("Can't fill texture: Source is too big");
            return;
        }
        unsafe {
            gl::BindTexture(gl::TEXTURE_2D, self.id);
            gl::TexSubImage2D(
                gl::TEXTURE_2D,
                0,
                x_offset as i32,
                y_offset as i32,
                image.width() as i32,
                image.height() as i32,
                get_gl_format(self.format),
                gl::UNSIGNED_BYTE,
                image.data().as_ptr() as *const gl::types::GLvoid
            );
            //gl::GenerateMipmap(gl::TEXTURE_2D);
            gl::BindTexture(gl::TEXTURE_2D, 0);
        }
    }

    pub fn resize(&mut self, width: u32, height: u32) {
        self.width = width;
        self.height = height;
        unsafe {
            gl::BindTexture(gl::TEXTURE_2D, self.id);
            gl::TexImage2D(
                gl::TEXTURE_2D,
                0,
                get_gl_format(self.format) as i32,
                width as i32,
                height as i32,
                0,
                get_gl_format(self.format),
                gl::UNSIGNED_BYTE,
                std::ptr::null() as *const gl::types::GLvoid
            );
            gl::BindTexture(gl::TEXTURE_2D, 0);
        }
    }

    pub fn set_filter_mode(&mut self, mode: FilterMode) {
        self.filter_mode = mode;
        let gl_filter = get_gl_filter(mode);
        unsafe {
            gl::BindTexture(gl::TEXTURE_2D, self.id);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl_filter.min as i32);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl_filter.mag as i32);
            gl::BindTexture(gl::TEXTURE_2D, 0);
        }
    }

    pub fn set_wrap_mode(&mut self, mode: WrapMode) {
        self.wrap_mode = mode;
        let gl_wrap = get_gl_wrap(mode);
        unsafe {
            gl::BindTexture(gl::TEXTURE_2D, self.id);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl_wrap.s as i32);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl_wrap.t as i32);
            gl::BindTexture(gl::TEXTURE_2D, 0);
        }
    }

    pub fn map_to_image(&self, image: &mut Image) {
        image.resize(self.width, self.height);
        unsafe {
            gl::BindTexture(gl::TEXTURE_2D, self.id);
            gl::GetTexImage(
                gl::TEXTURE_2D,
                0,
                get_gl_format(self.format),
                gl::UNSIGNED_BYTE,
                image.mut_data().as_ptr() as *mut gl::types::GLvoid
            );
            gl::BindTexture(gl::TEXTURE_2D, 0);
        }
    }

    pub fn bind(&self) {
        unsafe {
            gl::ActiveTexture(gl::TEXTURE0);
            gl::BindTexture(gl::TEXTURE_2D, self.id);
        }
    }

    pub fn width(&self) -> u32 {
        self.width
    }

    pub fn height(&self) -> u32 {
        self.height
    }

    pub fn size(&self) -> Size2D {
        Size2D::new(self.width, self.height)
    }

    pub(super) fn internal_id(&self) -> u32 {
        self.id
    }
}

impl Drop for Texture {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteTextures(1, &self.id);
        }
    }
}

fn get_gl_format(fmt: Format) -> gl::types::GLenum {
    match fmt {
        Format::Rgba => gl::RGBA,
        Format::Bgra => gl::BGRA,
        Format::Red => gl::RED,
        Format::Green => gl::GREEN,
        Format::Blue => gl::BLUE,
        Format::Alpha => gl::ALPHA,
    }
}

fn get_gl_filter(mode: FilterMode) -> GLFilter {
    match mode {
        FilterMode::Nearest => GLFilter{ min: gl::NEAREST, mag: gl::NEAREST },
        FilterMode::Bilinear => GLFilter{ min: gl::LINEAR, mag: gl::LINEAR},
        FilterMode::Trilinear => GLFilter{ min: gl::LINEAR_MIPMAP_LINEAR, mag: gl::LINEAR},
        _ => {
            info!("Unsupported FilterMode: {:?}", mode);
            GLFilter{ min: gl::NEAREST, mag: gl::NEAREST }
        }
    }
}

// This implementation depends on always having mipmaps
/*fn get_gl_filter(mode: FilterMode) -> GLFilter {
    match mode {
        FilterMode::Nearest => GLFilter{ min: gl::NEAREST_MIPMAP_NEAREST, mag: gl::NEAREST },
        FilterMode::Bilinear => GLFilter{ min: gl::LINEAR_MIPMAP_NEAREST, mag: gl::LINEAR},
        FilterMode::Trilinear => GLFilter{ min: gl::LINEAR_MIPMAP_LINEAR, mag: gl::LINEAR},
        _ => {
            info!("Unsupported FilterMode: {:?}", mode);
            GLFilter{ min: gl::NEAREST, mag: gl::NEAREST }
        }
    }
}*/

fn get_gl_wrap(mode: WrapMode) -> GLWrap {
    match mode {
        WrapMode::Wrap => GLWrap{ s: gl::REPEAT, t: gl::REPEAT },
        WrapMode::Clamp => GLWrap{ s: gl::CLAMP_TO_EDGE, t: gl::CLAMP_TO_EDGE },
        WrapMode::Mirror => GLWrap{ s: gl::MIRRORED_REPEAT, t: gl::MIRRORED_REPEAT },
        WrapMode::Border => GLWrap{ s: gl::CLAMP_TO_BORDER, t: gl::CLAMP_TO_BORDER },
    }
}

fn _get_pixel_size(fmt: Format) -> u32 {
    match fmt {
        Format::Rgba => 4,
        Format::Bgra => 4,
        Format::Red => 1,
        Format::Green => 1,
        Format::Blue => 1,
        Format::Alpha => 1,
    }
}
