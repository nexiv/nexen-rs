/*************************************************************************/
/*  transform.rs                                                         */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::core::application::InitArgs;
use crate::core::event::Event;
use crate::core::DataBundle;
use crate::ecs::{System, Component, Entity};
use crate::ecs::components::HierarchyComponent;
use rayon::iter::{
    //IntoParallelRefIterator,
    IntoParallelRefMutIterator,
    ParallelIterator,
};
#[cfg(feature = "profile_transform")]
use crate::profile::Profiler;

pub struct TransformComponent {
    matrix: glm::Mat4,

    pub(crate) dirty: bool,
    pub(crate) translation: glm::Vec2,
    pub(crate) scale: glm::Vec2,
    pub(crate) rotation: f32,

    pub(crate) parent_translation: glm::Vec2,
    pub(crate) parent_rotation: f32,

    pub inherit_parent_rotation: bool,
    pub inherit_parent_scale: bool,
}

impl Component for TransformComponent {}

impl TransformComponent {
    pub fn new() -> Self {
        TransformComponent {
            matrix: glm::identity(),
            dirty: false,
            translation: glm::vec2(0.0, 0.0),
            scale: glm::vec2(1.0, 1.0),
            rotation: 0.0,
            parent_translation: glm::vec2(0.0, 0.0),
            parent_rotation: 0.0,
            inherit_parent_rotation: true,
            inherit_parent_scale: true,
        }
    }

    pub fn set_translation(&mut self, translation: glm::Vec2) -> &mut Self {
        self.translation = translation;
        self.dirty = true;
        self
    }

    pub fn set_scale(&mut self, scale: glm::Vec2) -> &mut Self {
        self.scale = scale;
        self.dirty = true;
        self
    }

    pub fn set_rotation(&mut self, rotation: f32) -> &mut Self {
        self.rotation = rotation;
        self.dirty = true;
        self
    }

    pub fn translate(&mut self, translation: glm::Vec2) -> &mut Self {
        self.translation += translation;
        self.dirty = true;
        self
    }

    pub fn scale(&mut self, scale: glm::Vec2) -> &mut Self {
        self.scale += scale;
        self.dirty = true;
        self
    }

    pub fn rotate(&mut self, rotation: f32) -> &mut Self {
        self.rotation += rotation;
        self.dirty = true;
        self
    }

    pub fn matrix(&self) -> &glm::TMat4<f32> {
        &self.matrix
    }

    pub fn translation(&self) -> &glm::Vec2 {
        &self.translation
    }

    pub fn global_translation(&self) -> glm::Vec2 {
        self.translation + self.parent_translation
    }

    pub fn global_rotation(&self) -> f32 {
        self.rotation + self.parent_rotation
    }

    pub fn scaling(&self) -> &glm::Vec2 {
        &self.scale
    }

    pub fn rotation(&self) -> f32 {
        self.rotation
    }

    pub(crate) fn update(&mut self) {
        if self.dirty {
            self.matrix = glm::translation(&glm::vec3(self.translation.x, self.translation.y, 0.0)) *
                glm::rotation(glm::radians(&glm::vec1(self.rotation)).x, &glm::vec3(0.0, 0.0, 1.0)) *
                glm::scaling(&glm::vec3(self.scale.x, self.scale.y, 1.0));
            self.dirty = false;
        }
    }

    fn update_from_relative(&mut self, relative_matrix: &glm::Mat4) {
        self.dirty = true;
        self.update();
        self.matrix = relative_matrix * self.matrix;
    }
}

pub struct TransformSystem {
    stack_matrix: smallvec::SmallVec<[glm::Mat4;12]>,
    stack_pos: smallvec::SmallVec<[glm::Vec2;12]>,
    stack_scale: smallvec::SmallVec<[glm::Vec2;12]>,
    stack_rot: smallvec::SmallVec<[f32;12]>,
}

impl Default for TransformSystem {
    fn default() -> Self {
        TransformSystem {
            stack_matrix: smallvec::SmallVec::from_vec(vec![glm::identity()]),
            stack_pos: smallvec::SmallVec::from_vec(vec![glm::vec2(0.0, 0.0)]),
            stack_scale: smallvec::SmallVec::from_vec(vec![glm::vec2(1.0, 1.0)]),
            stack_rot: smallvec::SmallVec::from_vec(vec![0.0]),
        }
    }
}

impl TransformSystem {
    fn recurse_hierarchy_update(&mut self, bundle: &DataBundle, entity: Entity) {
        let transforms = bundle.world().component_storage::<TransformComponent>();
        let hierarchies = bundle.world().component_storage::<HierarchyComponent>();
        let hierarchy = hierarchies.read(entity).unwrap();

        let mut this_was_dirty = false;

        { // scope for transform mutable borrow
            let mut transform = transforms.write(entity).unwrap();
            if transform.dirty {
                this_was_dirty = true;
                if !transform.inherit_parent_rotation {
                    let stack_pos = *self.stack_pos.last().unwrap();
                    let stack_scale = *self.stack_scale.last().unwrap();
                    let final_transform;
                    if transform.inherit_parent_scale {
                        final_transform = glm::translation(&glm::vec3(stack_pos.x, stack_pos.y, 1.0)) *
                            glm::scaling(&glm::vec3(stack_scale.x, stack_scale.y, 1.0));
                    } else {
                        final_transform = glm::translation(&glm::vec3(stack_pos.x, stack_pos.y, 1.0)) *
                            glm::scaling(&glm::vec3(1.0, 1.0, 1.0));
                    }
                    transform.update_from_relative(&final_transform);
                    transform.parent_translation = stack_pos;
                } else if !transform.inherit_parent_scale {
                    let stack_pos = *self.stack_pos.last().unwrap();
                    let stack_rot = *self.stack_rot.last().unwrap();
                    let final_transform;
                    final_transform = glm::translation(&glm::vec3(stack_pos.x, stack_pos.y, 1.0)) *
                        glm::rotation(stack_rot, &glm::vec3(0.0, 0.0, 1.0));
                    transform.update_from_relative(&final_transform);
                    transform.parent_rotation = stack_rot;
                    transform.parent_translation = stack_pos;
                } else {
                    transform.update_from_relative(self.stack_matrix.last().unwrap());
                    transform.parent_translation = *self.stack_pos.last().unwrap();
                    transform.parent_rotation = *self.stack_rot.last().unwrap();
                }
            }
                
            self.stack_matrix.push(transform.matrix().clone());
            self.stack_pos.push(transform.global_translation());
            self.stack_scale.push(transform.scaling().clone());
            self.stack_rot.push(transform.global_rotation());
        } // scope for transform mutable borrow

        for child in &hierarchy.children {
            // If this entity was dirty, mark children as dirty
            // to update relative to this entity
            if this_was_dirty {
                transforms.write(*child).unwrap().dirty = true;
            }
            self.recurse_hierarchy_update(bundle, *child);
        }
        // Clear stack
        self.stack_matrix.pop();
        self.stack_pos.pop();
        self.stack_scale.pop();
        self.stack_rot.pop();
    }
}

impl System for TransformSystem {
    fn init(&mut self, _bundle: &mut DataBundle, _args: &InitArgs) {
    }

    fn update(&mut self, bundle: &DataBundle, _delta: f32) {
        let hierarchies = bundle.world().component_storage::<HierarchyComponent>();
        let transforms = bundle.world().component_storage::<TransformComponent>();

        for hierarchy in &*hierarchies.data() {
            if hierarchy.parent == Entity::null() {
                self.recurse_hierarchy_update(bundle, hierarchy.entity);
            }
        }
        transforms.data_mut().par_iter_mut().for_each(|transform| {
            transform.update()
        });


            /*if !transforms.read(hierarchy.parent).unwrap().dirty {
                continue;
            }

            let parent_matrix;
            let parent_translation;
            let parent_scaling;
            let parent_rotation;
            {
                let mut parent = transforms.write(hierarchy.parent).unwrap();
                parent.update();

                // We reset the dirty flag for simbling entities
                parent.dirty = true;

                parent_matrix = parent.matrix;
                parent_translation = parent.translation().clone();
                parent_scaling = parent.scaling().clone();
                parent_rotation = parent.rotation;
            }

            let mut child = transforms.write(hierarchy.entity).unwrap();

            if !child.inherit_parent_rotation {
                let final_transform;
                if child.inherit_parent_scale {
                    final_transform = glm::translation(&glm::vec3(parent_translation.x, parent_translation.y, 1.0)) *
                        glm::scaling(&glm::vec3(parent_scaling.x, parent_scaling.y, 1.0));
                } else {
                    final_transform = glm::translation(&glm::vec3(parent_translation.x, parent_translation.y, 1.0)) *
                        glm::scaling(&glm::vec3(1.0, 1.0, 1.0));
                }
                child.update_from_relative(&final_transform);
                child.parent_translation = parent_translation;
            } else if !child.inherit_parent_scale {
                let final_transform;
                final_transform = glm::translation(&glm::vec3(parent_translation.x, parent_translation.y, 1.0)) *
                    glm::rotation(parent_rotation, &glm::vec3(0.0, 0.0, 1.0));
                child.update_from_relative(&final_transform);
                child.parent_rotation = parent_rotation;
                child.parent_translation = parent_translation;
            } else {
                child.update_from_relative(&parent_matrix);
                child.parent_translation = parent_translation;
                child.parent_rotation = parent_rotation;
            }
        }*/
    }

    fn handle_events(&mut self, _bundle: &mut DataBundle, _event: &Event) {}
}
