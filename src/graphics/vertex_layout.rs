/*************************************************************************/
/*  vertex_layout.rs                                                     */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
#[derive(Copy, Clone)]
pub enum VertexDataType {
    None = 0,
    Float,
    Float2,
    Float3,
    Float4,
    Int,
    Int2,
    Int3,
    Int4,
    Bool,
    Mat3,
    Mat4,
}

fn get_data_type_size(data_type: VertexDataType) -> u32 {
    match data_type {
        VertexDataType::None => 0,
        VertexDataType::Float => 4,
        VertexDataType::Float2 => 4 * 2,
        VertexDataType::Float3 => 4 * 3,
        VertexDataType::Float4 => 4 * 4,
        VertexDataType::Int => 4,
        VertexDataType::Int2 => 4 * 2,
        VertexDataType::Int3 => 4 * 3,
        VertexDataType::Int4 => 4 * 4,
        VertexDataType::Bool => 1,
        VertexDataType::Mat3 => 4 * 3 * 3,
        VertexDataType::Mat4 => 4 * 4 *4,
    }
}

#[derive(Copy, Clone)]
pub struct VertexElement {
    pub data_type: VertexDataType,
    pub size: u32,
    pub offset: u32,
    pub normalized: bool,
}

impl VertexElement {
    pub fn new(data_type: VertexDataType, normalized: bool) -> Self {
        VertexElement {
            data_type,
            size: get_data_type_size(data_type),
            offset: 0,
            normalized
        }
    }

    pub fn component_count(&self) -> u32 {
        match self.data_type {
            VertexDataType::None => 0,
            VertexDataType::Float => 1,
            VertexDataType::Float2 => 2,
            VertexDataType::Float3 => 3,
            VertexDataType::Float4 => 4,
            VertexDataType::Int => 1,
            VertexDataType::Int2 => 2,
            VertexDataType::Int3 => 3,
            VertexDataType::Int4 => 4,
            VertexDataType::Bool => 1,
            VertexDataType::Mat3 => 3 * 3,
            VertexDataType::Mat4 => 4 *4,
        }
    }
}

impl From<(VertexDataType, bool)> for VertexElement {
    fn from(other: (VertexDataType, bool)) -> Self {
        VertexElement::new(other.0, other.1)
    }
}

pub struct VertexLayout {
    elements: Vec<VertexElement>,
    stride: u32,
}

impl VertexLayout {
    pub fn new() -> Self {
        VertexLayout{ elements: Vec::new(), stride: 0 }
    }

    pub fn from_elements(elements: &[VertexElement]) -> Self {
        let mut vertex_layout = VertexLayout{ elements: elements.to_vec(), stride: 0 };
        vertex_layout.calculate_offset_and_stride();
        vertex_layout
    }

    pub fn set_from_elements(&mut self, elements: &[VertexElement]) {
        self.elements = elements.to_vec();
        self.calculate_offset_and_stride();
    }

    pub fn stride(&self) -> u32 {
        self.stride
    }

    pub fn elements(&self) -> &Vec<VertexElement> {
        &self.elements
    }

    fn calculate_offset_and_stride(&mut self) {
        let mut offset: u32 = 0;
        self.stride = 0;
        for element in &mut self.elements {
            element.offset = offset;
            offset += element.size;
            self.stride += element.size;
        }
    }
}

pub fn get_gl_data_type(data_type: VertexDataType) -> u32 {
    match data_type {
        VertexDataType::None => gl::NONE,
        VertexDataType::Float => gl::FLOAT,
        VertexDataType::Float2 => gl::FLOAT,
        VertexDataType::Float3 => gl::FLOAT,
        VertexDataType::Float4 => gl::FLOAT,
        VertexDataType::Int => gl::INT,
        VertexDataType::Int2 => gl::INT,
        VertexDataType::Int3 => gl::INT,
        VertexDataType::Int4 => gl::INT,
        VertexDataType::Bool => gl::BOOL,
        VertexDataType::Mat3 => gl::FLOAT,
        VertexDataType::Mat4 => gl::FLOAT,
    }
}
