/*************************************************************************/
/*  gui.rs                                                               */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::common::{Size2D, Point2D};
use crate::core::{DataBundle, event::Event, application::InitArgs};
use crate::ecs::{Component, System, AsyncSystem};

pub mod button;
pub use button::Button as Button;
pub mod label;
pub use label::Label as Label;
pub mod container;
pub use container::Container as Container;

pub trait Widget: Component {
    fn build(&mut self, bundle: &DataBundle);

    fn needs_rebuild(&self) -> bool;

    fn handle_event(&mut self, event: &Event);

    fn get_minimum_size(&self) -> Size2D;

    fn set_final_position(&mut self, pos: Point2D);
}

#[derive(Default)]
pub struct GUISystem {}

impl System for GUISystem {
    fn init(&mut self, _bundle: &mut DataBundle, _init_args: &InitArgs) {

    }

    fn update(&mut self, bundle: &DataBundle, _delta: f32) {
        let containers = bundle.world().component_storage::<Container>();

        for container in &mut *containers.data_mut() {
            if container.needs_rebuild() {
                container.build(bundle);
            }
        }
    }

    fn handle_events(&mut self, bundle: &mut DataBundle, event: &Event) {
        let containers = bundle.world().component_storage::<Container>();

        for container in &mut *containers.data_mut() {
            container.handle_event(event);
        }
    }
}

impl AsyncSystem for GUISystem {}
