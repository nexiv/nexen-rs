/*************************************************************************/
/*  button.rs                                                            */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use super::Widget;
use crate::common::{Size2D, Point2D};
use crate::core::{DataBundle, event::Event};
use crate::platform::WindowEvent;
use crate::ecs::{Entity, Component};
use crate::graphics::{
    Polygon2DComponent,
    TransformComponent,
    Polygon2DShape,
    Text2DComponent,
    Font,
    DefaultFontLoader
};

pub struct Button {
    entity: Entity,
    pressed: bool,
    rebuild: bool,
    pos: Point2D,
    pub label: String,
    pub on_pressed: fn()->(),
}

impl Component for Button {}

impl Widget for Button {
    fn build(&mut self, bundle: &DataBundle) {
        let mut shapes = bundle.world().component_storage::<Polygon2DComponent>();
        let mut texts = bundle.world().component_storage::<Text2DComponent>();
        let mut transforms = bundle.world().component_storage::<TransformComponent>();

        if self.entity == Entity::null() {
            self.entity = bundle.world_mut().create_entity();
        }

        if transforms.read(self.entity).is_none() {
            let mut transform = TransformComponent::new();
            transform.set_translation(glm::vec2(self.pos.x as f32, self.pos.y as f32));
            transforms.insert(self.entity, transform);
        } else {
            let mut transform = transforms.write(self.entity).unwrap();
            transform.set_translation(glm::vec2(self.pos.x as f32, self.pos.y as f32));
        }

        if shapes.read(self.entity).is_none() {
            let mut shape = Polygon2DComponent::new();
            shape.shape = Polygon2DShape::Rectangle(glm::vec2(140.0, 40.0));
            shape.color = (40, 40, 40, 255).into();
            shapes.insert(self.entity, shape);
        } else {
            let mut shape = shapes.write(self.entity).unwrap();
            if self.pressed {
                shape.color = (80, 80, 80, 255).into();
            } else {
                shape.color = (40, 40, 40, 255).into();
            }
        }

        if texts.read(self.entity).is_none() {
            let mut text = Text2DComponent::new(bundle
                .resource_manager_mut().get_storage_for::<Font>()
                .get::<DefaultFontLoader>("res/fonts/mono.ttf").unwrap());
            text.text = self.label.clone();
            texts.insert(self.entity, text);
        }
        self.rebuild = false;
    }

    fn needs_rebuild(&self) -> bool {
        self.rebuild
    }

    fn get_minimum_size(&self) -> Size2D {
        Size2D::new(140, 40)
    }

    fn set_final_position(&mut self, pos: Point2D) {
        self.pos = pos;
    }

    fn handle_event(&mut self, event: &Event) {
        match event {
            Event::Window(window_event) => match window_event {
                WindowEvent::MouseButtonPressed(_, x, y) => {
                    if *x <= 140 && *y <= 40 {
                        self.pressed = true;
                        self.rebuild = true;
                    }
                },
                WindowEvent::MouseButtonReleased(_, x, y) => {
                    if *x <= 140 && *y <= 40 {
                        self.pressed = false;
                        (self.on_pressed)();
                        self.rebuild = true;
                    }
                },
                _ => (),
            },
            _ => (),
        }
    }
}

impl Button {
    pub fn new() -> Self {
        Button {
            entity: Entity::null(),
            label: "Button".to_owned(),
            pressed: false,
            rebuild: true,
            pos: Point2D::new(0, 0),
            on_pressed: || {},
        }
    }
}
