/*************************************************************************/
/*  container.rs                                                         */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use super::Widget;
use crate::common::{Size2D, Point2D};
use crate::core::{DataBundle, event::Event};
use crate::ecs::{Component, Entity};
use crate::graphics::{
    Polygon2DComponent,
    TransformComponent,
    Polygon2DShape,
};

pub struct Container {
    entity: Entity,
    pub size: Size2D,

    rebuild: bool,
    children: Vec<Box<dyn Widget>>,
}

impl Component for Container {}

impl Widget for Container {
    fn build(&mut self, bundle: &DataBundle) {
        let mut shapes = bundle.world().component_storage::<Polygon2DComponent>();
        let mut transforms = bundle.world().component_storage::<TransformComponent>();

        if self.entity == Entity::null() {
            self.entity = bundle.world_mut().create_entity();
        }

        if transforms.read(self.entity).is_none() {
            let mut transform = TransformComponent::new();
            transform.set_translation(glm::vec2(0.0, 0.0));
            transforms.insert(self.entity, transform);
        } else {
            let mut transform = transforms.write(self.entity).unwrap();
            transform.set_translation(glm::vec2(0.0, 0.0));
        }

        if shapes.read(self.entity).is_none() {
            let mut shape = Polygon2DComponent::new();
            shape.shape = Polygon2DShape::Rectangle(glm::vec2(self.size.x as f32, self.size.y as f32));
            shape.color = (20, 20, 20, 255).into();
            shapes.insert(self.entity, shape);
        }

        let mut pos = Point2D::new(0, 40);
        for child in &mut self.children {
            child.set_final_position(pos);
            pos.y += child.get_minimum_size().y as i32;
            child.build(bundle);
        }

        self.rebuild = false;
    }

    fn needs_rebuild(&self) -> bool {
        self.rebuild
    }

    fn get_minimum_size(&self) -> Size2D {
        let mut size = Size2D::new(0, 0);
        for child in &self.children {
            if child.get_minimum_size().x > size.x {
                size.x = child.get_minimum_size().x;
            }
            size.y += child.get_minimum_size().y;
        }
        size
    }

    fn set_final_position(&mut self, _pos: Point2D) {

    }

    fn handle_event(&mut self, event: &Event) {
        for child in &mut self.children {
            child.handle_event(event);
        }
    }
}

impl Container {
    pub fn new() -> Self {
        Container {
            entity: Entity::null(),
            size: Size2D::new(400, 400),
            rebuild: true,
            children: Vec::new(),
        }
    }

    pub fn add_child(&mut self, widget: impl Widget) {
        self.children.push(Box::new(widget));
    }
}
