/*************************************************************************/
/*  label.rs                                                            */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use super::Widget;
use crate::common::{Size2D, Point2D};
use crate::core::{DataBundle, event::Event};
use crate::ecs::{Entity, Component};
use crate::graphics::{Text2DComponent, Font, DefaultFontLoader};

pub struct Label {
    entity: Entity,
    rebuild: bool,
    pub label: String,
}

impl Component for Label {}

impl Widget for Label {
    fn build(&mut self, bundle: &DataBundle) {
        let mut texts = bundle.world().component_storage::<Text2DComponent>();

        if self.entity == Entity::null() {
            self.entity = bundle.world_mut().create_entity();
        }

        if texts.read(self.entity).is_none() {
            let mut text = Text2DComponent::new(bundle
                .resource_manager_mut().get_storage_for::<Font>()
                .get::<DefaultFontLoader>("res/fonts/mono.ttf").unwrap());
            text.text = self.label.clone();
            texts.insert(self.entity, text);
        }
        self.rebuild = false;
    }

    fn needs_rebuild(&self) -> bool {
        self.rebuild
    }

    fn get_minimum_size(&self) -> Size2D {
        Size2D::new(140, 40)
    }

    fn set_final_position(&mut self, _pos: Point2D) {

    }

    fn handle_event(&mut self, _event: &Event) {}
}

impl Label {
    pub fn new() -> Self {
        Label {
            entity: Entity::null(),
            label: "Label".to_owned(),
            rebuild: true,
        }
    }
}
