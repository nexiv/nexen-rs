/*************************************************************************/
/*  lib.rs                                                               */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
pub extern crate nalgebra_glm as glm;
pub extern crate sdl2;
pub extern crate log;
pub mod common;
pub mod core;
pub mod ecs;
pub mod platform;
pub mod resources;
pub mod animation;
pub mod graphics;
pub mod gui;
pub mod physics;
pub mod audio;
pub mod script;
pub mod time;
pub mod profile;

pub use platform::keyboard as keyboard;

pub mod logger{
    use log::{Record, Level, Metadata};

    struct SimpleLogger;

    impl log::Log for SimpleLogger {
        fn enabled(&self, metadata: &Metadata) -> bool {
            metadata.level() <= Level::Trace
        }

        fn log(&self, record: &Record) {
            if self.enabled(record.metadata()) {
                println!("[{}] {}", record.level(), record.args());
            }
        }

        fn flush(&self) {}
    }

    //use log::{SetLoggerError, LevelFilter};
    use log::LevelFilter;

    static LOGGER: SimpleLogger = SimpleLogger;

    pub fn init() {
        log::set_logger(&LOGGER)
            .map(|()| log::set_max_level(LevelFilter::Info)).unwrap()
    }

    pub fn set_level(filter: LevelFilter) {
        log::set_max_level(filter);
    }
}
