/*************************************************************************/
/*  joint.rs                                                             */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::ecs::Component;
use nphysics2d::joint::DefaultJointConstraintHandle;

#[derive(Copy, Clone, PartialEq, Debug, serde::Deserialize)]
pub enum JointType {
    Prismatic,
    Revolute,
    Undefined,
}

pub struct JointComponent {
    pub joint_type: JointType,
    pub limit: f32,
    pub(super) handle: Option<DefaultJointConstraintHandle>,
}

impl JointComponent {
    pub fn new() -> Self {
        JointComponent {
            joint_type: JointType::Undefined,
            limit: 0.0,
            handle: None,
        }
    }
}

impl Component for JointComponent {}
