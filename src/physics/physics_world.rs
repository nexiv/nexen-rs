/*************************************************************************/
/*  physics_world.rs                                                     */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use super::{
    physics_event::PhysicsEvent,
    raycast::RaycastComponent,
    rigid_body::{RigidBodyComponent, RigidBodyType},
    joint::{JointType, JointComponent},
};
use crate::core::{application::InitArgs, event::Event, DataBundle};
use crate::ecs::{Entity, System, AsyncSystem, components::HierarchyComponent};
use crate::graphics::TransformComponent;

use ncollide2d::math::Point;
use ncollide2d::pipeline::{CollisionGroups, CollisionObjectSet, ContactEvent};
use ncollide2d::query::{Proximity, Ray};
use ncollide2d::shape::{ConvexPolygon, ShapeHandle};
use nphysics2d::{
    algebra::Force2,
    force_generator::DefaultForceGeneratorSet,
    joint::DefaultJointConstraintSet,
    joint::PrismaticConstraint,
    material::{BasicMaterial, MaterialHandle},
    math::Isometry,
    object::{
        Body, BodyPartHandle, BodyStatus, ColliderDesc, DefaultBodySet, DefaultColliderSet,
        RigidBodyDesc,
    },
    world::{DefaultGeometricalWorld, DefaultMechanicalWorld},
};

pub struct PhysicsWorld {
    mech_world: DefaultMechanicalWorld<f32>,
    geom_world: DefaultGeometricalWorld<f32>,
    bodies: DefaultBodySet<f32>,
    colliders: DefaultColliderSet<f32>,
    joint_constraints: DefaultJointConstraintSet<f32>,
    force_generators: DefaultForceGeneratorSet<f32>,
    collision_events: Vec<PhysicsEvent>,
    pixel_scale: u32,

    #[cfg(debug_assertions)]
    _draw_debug_shapes: bool,
}

impl Default for PhysicsWorld {
    fn default() -> Self {
        PhysicsWorld::new()
    }
}

impl PhysicsWorld {
    pub fn new() -> Self {
        let mut mech_world = DefaultMechanicalWorld::new(glm::vec2(0.0, -9.81));
        //TODO: Expose this to the outer API
        mech_world.set_timestep(1.0 / 45.0);
        PhysicsWorld {
            mech_world,
            geom_world: DefaultGeometricalWorld::new(),
            bodies: DefaultBodySet::new(),
            colliders: DefaultColliderSet::new(),
            joint_constraints: DefaultJointConstraintSet::new(),
            force_generators: DefaultForceGeneratorSet::new(),
            collision_events: Vec::new(),
            pixel_scale: 64,

            #[cfg(debug_assertions)]
            _draw_debug_shapes: false,
        }
    }

    pub fn set_pixel_scale(&mut self, pixel_scale: u32) {
        self.pixel_scale = pixel_scale;
    }

    pub fn poll_events(&mut self) -> Vec<Event> {
        let mut vec = Vec::with_capacity(self.collision_events.len());
        for physics_event in &self.collision_events {
            vec.push(Event::Physics(*physics_event));
        }
        self.collision_events.clear();
        vec
    }
}

impl System for PhysicsWorld {
    fn init(&mut self, _bundle: &mut DataBundle, _args: &InitArgs) {}

    #[cfg(debug_assertions)]
    fn render(&self, bundle: &DataBundle, renderer: &mut crate::graphics::Renderer2D) {
        if !self._draw_debug_shapes {
            return;
        }

        let world = bundle.world();
        let entities = world.entities();
        let transforms = world.component_storage::<TransformComponent>();
        let bodies = world.component_storage::<RigidBodyComponent>();
        let raycasts = world.component_storage::<RaycastComponent>();

        use crate::graphics::render_queue::RenderCommand;
        for entity in entities {
            if let Some(body) = bodies.read(*entity) {
                let color: crate::graphics::Color = match body.sensor {
                    true => (60, 60, 200, 200).into(),
                    false => (255, 255, 255, 200).into(),
                };
                if let Some(transform) = transforms.read(*entity) {
                    renderer.push_render_command(
                        RenderCommand::DrawPolygon(
                            glm::scale(&transform.matrix(), &glm::vec3(1.0, 1.0, 1.0)),
                            body.mesh().clone(),
                            color,
                        ),
                        0,
                        126,
                    );
                } else {
                    renderer.push_render_command(
                        RenderCommand::DrawPolygon(glm::identity(), body.mesh().clone(), color),
                        0,
                        126,
                    );
                }
            }

            if let Some(raycast) = raycasts.read(*entity) {
                if let Some(transform) = transforms.read(*entity) {
                    renderer.push_render_command(
                        RenderCommand::DrawLine(
                            glm::identity(),
                            transform.global_translation(),
                            (raycast.direction * raycast.length) + transform.global_translation(),
                            (255, 0, 0, 200).into(),
                        ),
                        0,
                        126,
                    );
                }
            }
        }
    }

    fn update(&mut self, bundle: &DataBundle, _delta: f32) {
        let world = bundle.world();
        let body_components = world.component_storage::<RigidBodyComponent>();
        let joints = world.component_storage::<JointComponent>();
        let hierarchies = world.component_storage::<HierarchyComponent>();
        let entities = world.entities();
        let transforms = world.component_storage::<TransformComponent>();
        let raycasts = world.component_storage::<RaycastComponent>();

        for entity in entities {
            let mut body_handle = None;
            let mut parent = Entity::null();

            if let Some(mut body_component) = body_components.write(*entity) {
                if body_component.handle == Option::None {
                    let status = match body_component.body_type {
                        RigidBodyType::Static => BodyStatus::Static,
                        RigidBodyType::Dynamic => BodyStatus::Dynamic,
                        RigidBodyType::Kinematic => BodyStatus::Kinematic,
                    };
                    let mut body = RigidBodyDesc::<f32>::new()
                        .mass(1.0)
                        .angular_inertia(body_component.angular_inertia)
                        .max_angular_velocity(body_component.max_angular_velocity)
                        .max_linear_velocity(body_component.max_linear_velocity)
                        .status(status);
                    if let Some(transform) = transforms.read(*entity) {
                        body.set_translation(glm::vec2(
                            transform.global_translation().x / self.pixel_scale as f32,
                            -transform.global_translation().y / self.pixel_scale as f32,
                        ));
                    }
                    let handle = self.bodies.insert(body.build());

                    if body_component.body_type == RigidBodyType::Kinematic {
                        let body = self.bodies.rigid_body_mut(handle).unwrap();
                        body.set_velocity(body_component.velocity);
                    }
                    if body_component.mesh_needs_update() {
                        let mut vertices = vec![Point::new(0.0, 0.0); body_component.mesh().len()];
                        for i in 0..body_component.mesh().len() {
                            vertices[i].x = body_component.mesh()[i].x / self.pixel_scale as f32;
                            vertices[i].y = -body_component.mesh()[i].y / self.pixel_scale as f32;
                        }
                        let shape =
                            ShapeHandle::new(ConvexPolygon::try_from_points(&vertices).unwrap());
                        let collider = ColliderDesc::new(shape)
                            .material(MaterialHandle::new(BasicMaterial::new(
                                0.0,
                                body_component.friction,
                            )))
                            .user_data((body_component.entity, body_component.one_way_collision))
                            .ccd_enabled(
                                body_component.enable_ccd || body_component.one_way_collision,
                            )
                            .sensor(body_component.sensor || body_component.one_way_collision)
                            .build(BodyPartHandle(handle, 0));
                        body_component.coll_handle = Some(self.colliders.insert(collider));
                        body_component.confirm_mesh_update();
                    }
                    body_component.handle = Some(handle);
                } else {
                    if body_component.mesh_needs_update() {
                        if let Some(handle) = body_component.coll_handle {
                            self.colliders.remove(handle);
                        }
                        let mut vertices = vec![Point::new(0.0, 0.0); body_component.mesh().len()];
                        for i in 0..body_component.mesh().len() {
                            vertices[i].x = body_component.mesh()[i].x / self.pixel_scale as f32;
                            vertices[i].y = -body_component.mesh()[i].y / self.pixel_scale as f32;
                        }
                        let shape =
                            ShapeHandle::new(ConvexPolygon::try_from_points(&vertices).unwrap());
                        let collider = ColliderDesc::new(shape)
                            .material(MaterialHandle::new(BasicMaterial::new(
                                0.0,
                                body_component.friction,
                            )))
                            .user_data((body_component.entity, body_component.one_way_collision))
                            .ccd_enabled(
                                body_component.enable_ccd || body_component.one_way_collision,
                            )
                            .sensor(body_component.sensor)
                            .build(BodyPartHandle(body_component.handle.unwrap(), 0));
                        body_component.coll_handle = Some(self.colliders.insert(collider));
                        body_component.confirm_mesh_update();
                    }
                    if let Some(body) = self
                        .bodies
                        .rigid_body_mut(body_component.handle.unwrap())
                    {
                        // Update nphysics position if TransformComponent was set manually
                        if let Some(transform) = transforms.read(*entity) {
                            if !body_component.passive {
                                if (transform.global_translation().x / self.pixel_scale as f32
                                    - body.position().translation.x)
                                    .abs()
                                    > 0.0001
                                    || (-transform.global_translation().y / self.pixel_scale as f32
                                        - body.position().translation.y)
                                        .abs()
                                        > 0.0001
                                {
                                    body.set_position(Isometry::new(
                                        glm::vec2(
                                            transform.global_translation().x / self.pixel_scale as f32,
                                            -transform.global_translation().y / self.pixel_scale as f32,
                                        ),
                                        0.0,
                                    ));
                                }
                            }
                        }

                        // Update properties
                        if body.angular_damping() != body_component.angular_inertia {
                            body.set_angular_damping(body_component.angular_inertia);
                        }
                        // Set Velocity if it was set manually
                        if body.velocity().linear != body_component.velocity.linear
                            || body.velocity().angular != body_component.velocity.angular
                        {
                            body.set_velocity(body_component.velocity);
                        }

                        // Set forces if they were set by the Component
                        if (body_component.force.linear.x != 0.0
                            || body_component.force.linear.y != 0.0
                            || body_component.force.angular != 0.0)
                            && body_component.body_type == RigidBodyType::Dynamic
                        {
                            if body_component.force_point.x != 0.0
                                || body_component.force_point.y != 0.0
                            {
                                body.apply_local_force_at_local_point(
                                    0,
                                    &body_component.force.linear,
                                    &body_component.force_point,
                                    body_component.force_type,
                                    true,
                                );
                            } else {
                                body.apply_force(
                                    0,
                                    &body_component.force,
                                    body_component.force_type,
                                    true,
                                );
                            }
                            body_component.force = Force2::zero();
                            body_component.force_point = Point::new(0.0, 0.0);
                        }
                    }
                }
                // Get handle and parent for joint creation
                if let Some(hierarchy) = hierarchies.read(*entity) {
                    body_handle = body_component.handle;
                    parent = hierarchy.parent;
                }

            }

            // Create Joints if there's any
            if let Some(mut joint) = joints.write(*entity) {
                if joint.handle.is_none() && joint.joint_type != JointType::Undefined {
                    // Check for parent entity body
                    if parent != Entity::null() && body_handle.is_some() {
                        if let Some(parent_body_component) = body_components.read(parent) {
                            // Check parent handle cause it might not have been created
                            if let Some(parent_body) = parent_body_component.handle {
                                match joint.joint_type {
                                    JointType::Prismatic => {
                                        let mut constraint = PrismaticConstraint::new(
                                            // Unwrap cause it's guaranteed to exist at this point
                                            BodyPartHandle(body_handle.unwrap(), 0),
                                            BodyPartHandle(parent_body, 0),
                                            nphysics2d::nalgebra::Point2::new(0.0, 0.0),
                                            nphysics2d::nalgebra::Vector2::y_axis(),
                                            nphysics2d::nalgebra::Point2::new(0.0, 0.0),
                                        );

                                        constraint.enable_min_offset(joint.limit / self.pixel_scale as f32);

                                        joint.handle = Some(self.joint_constraints.insert(constraint));
                                    }
                                    _ => unimplemented!(),
                                }
                            }
                        }
                    }
                }
            }
        }

        self.mech_world.step(
            &mut self.geom_world,
            &mut self.bodies,
            &mut self.colliders,
            &mut self.joint_constraints,
            &mut self.force_generators,
        );

        for entity in entities {
            if let Some(mut body_and_transform) =
                body_components.join_write_both(&transforms, *entity)
            {
                if body_and_transform.0.body_type == RigidBodyType::Static {
                    continue;
                }
                if let Some(body) = self
                    .bodies
                    .rigid_body(body_and_transform.0.handle.unwrap())
                {
                    let mut transform = body_and_transform.1;
                    let pixel_scale = self.pixel_scale as f32;

                    body_and_transform.0.velocity = *body.velocity();
                    let pos = body.position().translation;
                    let rot = body.position().rotation.angle();
                    transform.set_translation(glm::vec2(pos.x * pixel_scale, -pos.y * pixel_scale));
                    transform.set_rotation(glm::degrees(&glm::vec1(-rot)).x);
                    if body_and_transform.0.passive {
                        transform.update();
                    }
                }
            }

            if let Some(raycast_and_transform) = raycasts.join_write_read(&transforms, *entity) {
                let mut raycast = raycast_and_transform.0;
                let transform = raycast_and_transform.1;

                raycast.clear_entities();
                let ray = Ray::new(
                    nphysics2d::nalgebra::Point2::new(
                        transform.global_translation().x,
                        -transform.global_translation().y,
                    ) / self.pixel_scale as f32,
                    glm::vec2(raycast.direction.x, -raycast.direction.y),
                );

                for (_, collider, intersection) in self.geom_world.interferences_with_ray(
                    &self.colliders,
                    &ray,
                    f32::MAX,
                    &CollisionGroups::new(),
                ) {
                    if (intersection.toi * self.pixel_scale as f32) < raycast.length {
                        raycast.add_entity(
                            collider
                                .user_data()
                                .unwrap_or(&(Entity::null(), false))
                                .downcast_ref::<(Entity, bool)>()
                                .unwrap_or(&(Entity::null(), false))
                                .0,
                        );
                    }
                }
            }
        }

        for contact in self.geom_world.contact_events() {
            match contact {
                ContactEvent::Started(handle1, handle2) => {
                    let fail_result = (Entity::null(), false);
                    let body1_user_data = self
                        .colliders
                        .collision_object(*handle1)
                        .unwrap()
                        .user_data()
                        .unwrap_or(&fail_result)
                        .downcast_ref::<(Entity, bool)>()
                        .unwrap_or(&fail_result)
                        .clone();

                    let body2_user_data = self
                        .colliders
                        .collision_object(*handle2)
                        .unwrap()
                        .user_data()
                        .unwrap_or(&fail_result)
                        .downcast_ref::<(Entity, bool)>()
                        .unwrap_or(&fail_result)
                        .clone();

                    // Populate event
                    self.collision_events.push(PhysicsEvent::ContactStart(
                        body1_user_data.0,
                        body2_user_data.0,
                    ));
                }
                ContactEvent::Stopped(handle1, handle2) => {
                    let fail_result = (Entity::null(), false);
                    let body1_user_data = self
                        .colliders
                        .collision_object(*handle1)
                        .unwrap()
                        .user_data()
                        .unwrap_or(&fail_result)
                        .downcast_ref::<(Entity, bool)>()
                        .unwrap_or(&fail_result)
                        .clone();

                    let body2_user_data = self
                        .colliders
                        .collision_object(*handle2)
                        .unwrap()
                        .user_data()
                        .unwrap_or(&fail_result)
                        .downcast_ref::<(Entity, bool)>()
                        .unwrap_or(&fail_result)
                        .clone();

                    if body1_user_data.1 {
                        if let Some(mut rigid_body) = bundle
                            .world()
                            .get_write_component::<RigidBodyComponent>(body1_user_data.0)
                        {
                            rigid_body.set_sensor(true);
                        }
                    }

                    if body2_user_data.1 {
                        if let Some(mut rigid_body) = bundle
                            .world()
                            .get_write_component::<RigidBodyComponent>(body2_user_data.0)
                        {
                            rigid_body.set_sensor(true);
                        }
                    }
                    // Populate event
                    self.collision_events.push(PhysicsEvent::ContactEnd(
                        body1_user_data.0,
                        body2_user_data.0,
                    ));
                }
            }
        }
        for proximity in self.geom_world.proximity_events() {
            let fail_result = (Entity::null(), false);

            let body1_user_data = self
                .colliders
                .collision_object(proximity.collider1)
                .unwrap()
                .user_data()
                .unwrap_or(&fail_result)
                .downcast_ref::<(Entity, bool)>()
                .unwrap_or(&fail_result)
                .clone();

            let body2_user_data = self
                .colliders
                .collision_object(proximity.collider2)
                .unwrap()
                .user_data()
                .unwrap_or(&fail_result)
                .downcast_ref::<(Entity, bool)>()
                .unwrap_or(&fail_result)
                .clone();

            match proximity.new_status {
                Proximity::WithinMargin => {
                    // Check for one-way collision
                    let mut this_body = None;
                    let mut other_body = None;
                    let mut this_body_entity = Entity::null();
                    if body1_user_data.1 {
                        this_body_entity = body1_user_data.0;

                        let this_body_h = self.colliders.get(proximity.collider1).unwrap().body();
                        this_body = self.bodies.rigid_body(this_body_h);

                        let other_body_h = self.colliders.get(proximity.collider2).unwrap().body();
                        other_body = self.bodies.rigid_body(other_body_h);
                    } else if body2_user_data.1 {
                        this_body_entity = body2_user_data.0;

                        let this_body_h = self.colliders.get(proximity.collider2).unwrap().body();
                        this_body = self.bodies.rigid_body(this_body_h);
                        let other_body_h = self.colliders.get(proximity.collider1).unwrap().body();
                        other_body = self.bodies.rigid_body(other_body_h);
                    }

                    if let Some(this_body) = this_body {
                        if let Some(other_body) = other_body {
                            // If coming up, turn this body into sensor
                            if other_body.position().translation.y
                                > this_body.position().translation.y
                            {
                                if let Some(mut rigid_body) = bundle
                                    .world()
                                    .get_write_component::<RigidBodyComponent>(this_body_entity)
                                {
                                    rigid_body.set_sensor(false);
                                }
                            }
                        }
                    }
                    self.collision_events.push(PhysicsEvent::ProximityWithin(
                        body1_user_data.0,
                        body2_user_data.0,
                    ));
                }
                Proximity::Intersecting => {
                    // Check for one-way collision
                    let mut this_body = None;
                    let mut other_body = None;
                    let mut this_body_entity = Entity::null();
                    if body1_user_data.1 {
                        this_body_entity = body1_user_data.0;

                        let this_body_h = self.colliders.get(proximity.collider1).unwrap().body();
                        this_body = self.bodies.rigid_body(this_body_h);

                        let other_body_h = self.colliders.get(proximity.collider2).unwrap().body();
                        other_body = self.bodies.rigid_body(other_body_h);
                    } else if body2_user_data.1 {
                        this_body_entity = body2_user_data.0;

                        let this_body_h = self.colliders.get(proximity.collider2).unwrap().body();
                        this_body = self.bodies.rigid_body(this_body_h);
                        let other_body_h = self.colliders.get(proximity.collider1).unwrap().body();
                        other_body = self.bodies.rigid_body(other_body_h);
                    }

                    if let Some(this_body) = this_body {
                        if let Some(other_body) = other_body {
                            // If coming up, turn this body into sensor
                            if other_body.position().translation.y
                                > this_body.position().translation.y
                            {
                                if let Some(mut rigid_body) = bundle
                                    .world()
                                    .get_write_component::<RigidBodyComponent>(this_body_entity)
                                {
                                    rigid_body.set_sensor(false);
                                }
                            }
                        }
                    }
                    self.collision_events.push(PhysicsEvent::ProximityWithin(
                        body1_user_data.0,
                        body2_user_data.0,
                    ));
                }
                Proximity::Disjoint => {
                    self.collision_events.push(PhysicsEvent::ProximityDisjoint(
                        body1_user_data.0,
                        body2_user_data.0,
                    ));
                }
            }
        }
    }

    #[cfg(debug_assertions)]
    fn handle_events(&mut self, _bundle: &mut DataBundle, event: &Event) {
        use crate::platform::keyboard::Keycode;
        use crate::platform::WindowEvent;

        if let Event::Window(window_event) = event {
            if let WindowEvent::KeyReleased(key) = window_event {
                if key.code == Keycode::P {
                    self._draw_debug_shapes = !self._draw_debug_shapes;
                }
            }
        }
    }
}

impl AsyncSystem for PhysicsWorld {}
