/*************************************************************************/
/*  raycast.rs                                                           */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::ecs::{Component, Entity};

pub struct RaycastComponent {
    pub direction: glm::Vec2,
    pub length: f32,
    pub(super) entities: [Entity;8],
}

impl Component for RaycastComponent {}

impl RaycastComponent {
    pub fn new() -> Self {
        RaycastComponent {
            direction: glm::vec2(0.0, 0.0),
            length: 1.0,
            entities: [Entity::null();8],
        }
    }

    pub(super) fn add_entity(&mut self, entity: Entity) {
        for i in 0..self.entities.len() {
            if self.entities[i] == Entity::null() {
                self.entities[i] = entity;
                return;
            }
        }
    }

    pub(super) fn clear_entities(&mut self) {
        for entity in self.entities.iter_mut() {
            *entity = Entity::null();
        }
    }

    pub(crate) fn colliding_entities(&self) -> &[Entity;8]{
        &self.entities
    }
}