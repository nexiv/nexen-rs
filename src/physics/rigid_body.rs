/*************************************************************************/
/*  rigid_body.rs                                                        */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::ecs::{Component, Entity};
use nphysics2d::object::{DefaultBodyHandle, DefaultColliderHandle};
use nphysics2d::algebra::{ForceType, Force2};
use nphysics2d::math::{Point, Velocity};

#[derive(Debug, Copy, Clone, PartialEq, serde::Deserialize)]
pub enum RigidBodyType {
    Static,
    Dynamic,
    Kinematic,
}

impl Component for RigidBodyComponent {}

pub struct RigidBodyComponent {
    pub(crate) entity: Entity,
    pub(super) handle: Option<DefaultBodyHandle>,
    pub(super) coll_handle: Option<DefaultColliderHandle>,
    pub body_type: RigidBodyType,
    pub mass: f32,
    pub friction: f32,
    pub angular_inertia: f32,
    pub max_angular_velocity: f32,
    pub max_linear_velocity: f32,
    pub velocity: Velocity<f32>,
    pub force: Force2<f32>,
    pub force_point: Point<f32>,
    pub force_type: ForceType,
    pub enable_ccd: bool,
    pub sensor: bool,
    pub one_way_collision: bool,
    pub passive: bool,
    mesh: Vec<glm::Vec2>,
    update_mesh: bool,
}

impl RigidBodyComponent {
    pub fn new() -> Self {
        RigidBodyComponent {
            entity: Entity::null(),
            handle: Option::None,
            coll_handle: Option::None,
            body_type: RigidBodyType::Static,
            mass: 1.0,
            friction: 0.0,
            angular_inertia: 0.1, // Allow some inertia by default
            max_angular_velocity: 0.0,
            max_linear_velocity: 0.0,
            velocity: Velocity::zero(),
            force: Force2::zero(),
            force_point: Point::new(0.0, 0.0),
            force_type: ForceType::Force,
            enable_ccd: false,
            update_mesh: false,
            sensor: false,
            one_way_collision: false,
            passive: false,
            mesh: Vec::new(),
        }
    }

    pub fn linear_impulse(&mut self, impulse: &glm::Vec2) {
        self.force.linear += impulse;
        self.force_type = ForceType::Impulse;
    }

    pub fn linear_impulse_from(&mut self, impulse: &glm::Vec2, point: &Point<f32>) {
        self.force.linear += impulse;
        self.force_point = *point;
        self.force_type = ForceType::Impulse;
    }

    pub fn angular_impulse(&mut self, angle: f32) {
        self.force.angular += angle;
        self.force_type = ForceType::Impulse;
    }

    pub fn linear_force(&mut self, force: &glm::Vec2) {
        self.force += Force2::new(*force, 0.0);
        self.force_type = ForceType::Force;
    }

    pub fn angular_force(&mut self, angle: f32) {
        self.force += Force2::new(glm::vec2(0.0, 0.0), angle);
        self.force_type = ForceType::Force;
    }

    pub fn set_angular_inertia(&mut self, inertia: f32) {
        self.angular_inertia = inertia;
    }

    pub fn set_mesh(&mut self, mesh: Vec<glm::Vec2>) {
        self.mesh = mesh;
        self.update_mesh = true;
    }

    pub fn set_sensor(&mut self, b: bool) {
        self.sensor = b;
        self.update_mesh = true;
    }

    pub fn mesh(&self) -> &Vec<glm::Vec2> {
        &self.mesh
    }

    pub fn mesh_needs_update(&self) -> bool {
        self.update_mesh
    }

    pub fn confirm_mesh_update(&mut self) {
        self.update_mesh = false;
    }
}
