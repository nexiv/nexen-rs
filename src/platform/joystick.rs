/*************************************************************************/
/*  joystick.rs                                                          */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
//pub use sdl2::controller::Button as JoystickButton;
pub use sdl2::joystick::HatState as HatState;

#[derive(Debug, Clone)]
pub enum JoystickEvent {
    ButtonPressed{id: u32, button: u8},
    ButtonReleased{id: u32, button: u8},
    AxisMotion{id: u32, axis_id: u8, value: i16},
    HatMotion{id: u32, state: HatState},
}

pub fn hat_state_to_string(state: HatState) -> String {
    match state {
        HatState::Up => "Up".to_owned(),
        HatState::Down => "Down".to_owned(),
        HatState::Left => "Left".to_owned(),
        HatState::Right => "Right".to_owned(),
        _ => String::new(),
    }
}