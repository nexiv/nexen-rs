/*************************************************************************/
/*  window.rs                                                            */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
#![allow(dead_code)]
use super::joystick::JoystickEvent;
use super::keyboard::Key;
use super::mouse::{MouseButton, MouseMove, MouseWheelDirection};
use crate::common::Size2D;
use crate::core::event::Event;
use log::{error, info};
use sdl2;
use sdl2::video::FullscreenType;

#[derive(Debug, Clone)]
pub enum WindowEvent {
    Quit,
    MouseMoved(MouseMove),
    MouseButtonPressed(MouseButton, u32, u32),
    MouseButtonReleased(MouseButton, u32, u32),
    MouseWheelMoved(MouseWheelDirection, i32, i32),
    KeyPressed(Key),
    KeyReleased(Key),
    TextEntered(String),
}

pub struct SDLWindow {
    sdl2_context: sdl2::Sdl,
    sdl2_window: sdl2::video::Window,
    gl_context: sdl2::video::GLContext,
    fucking_joystick: Option<sdl2::joystick::Joystick>,
    preserved_size: Size2D,
    exit_requested: bool,
}

impl SDLWindow {
    pub fn new(title: &str, size: Size2D) -> Self {
        let sdl2_context = sdl2::init().unwrap();

        info!("Initialized SDL v{}", sdl2::version::version());

        let joy_sys = sdl2_context.joystick().unwrap();
        let mut joy = None;

        if let Ok(joy_index) = joy_sys.num_joysticks() {
            for i in 0..joy_index {
                match joy_sys.open(i) {
                    Ok(joystick) => {
                        info!("Detected Game Controller: {}\n\t{} axes\n\t{} buttons\n\t{} hats\n\t{} balls",
                            joystick.name(),
                            joystick.num_axes(),
                            joystick.num_buttons(),
                            joystick.num_hats(),
                            joystick.num_balls()
                        );
                        joy = Some(joystick);
                    }
                    Err(error) => {
                        error!("Error opening controller device: {}", error);
                    }
                }
            }
        }

        let video_sys = sdl2_context.video().unwrap();
        let sdl2_window = video_sys
            .window(title, size.x, size.y)
            .position_centered()
            .opengl()
            .build()
            .unwrap();

        let gl_context = sdl2_window.gl_create_context().unwrap();

        video_sys.gl_attr().set_context_major_version(3);
        video_sys.gl_attr().set_context_minor_version(3);
        video_sys
            .gl_attr()
            .set_context_profile(sdl2::video::GLProfile::Core);

        let gl_version = video_sys.gl_attr().context_version();
        let gl_profile = video_sys.gl_attr().context_profile();
        video_sys.gl_attr().set_context_flags().debug().set();

        info!("OpenGL {}.{} {:?}", gl_version.0, gl_version.1, gl_profile);
        info!("Driver: {}", video_sys.current_video_driver());

        gl::load_with(|s| video_sys.gl_get_proc_address(s) as *const std::os::raw::c_void);

        SDLWindow {
            sdl2_context,
            sdl2_window,
            gl_context,
            fucking_joystick: joy,
            exit_requested: false,
            preserved_size: size,
        }
    }

    pub fn exit_request(&self) -> bool {
        self.exit_requested
    }

    pub fn poll_events(&mut self) -> Vec<Event> {
        let mut event_pump = self.sdl2_context.event_pump().unwrap();
        let mut events: Vec<Event> = Vec::new();
        for sdl_event in event_pump.poll_iter() {
            events.push(self.convert_event(&sdl_event));
        }
        events
    }

    fn convert_event(&mut self, sdl_event: &sdl2::event::Event) -> Event {
        match sdl_event {
            sdl2::event::Event::Quit { .. } => {
                self.exit_requested = true;
                Event::None
            }
            sdl2::event::Event::MouseMotion {
                x, y, xrel, yrel, ..
            } => Event::Window(WindowEvent::MouseMoved(MouseMove {
                x: *x,
                y: *y,
                x_rel: *xrel,
                y_rel: *yrel,
            })),
            sdl2::event::Event::MouseButtonDown {
                mouse_btn, x, y, ..
            } => Event::Window(WindowEvent::MouseButtonPressed(
                *mouse_btn, *x as u32, *y as u32,
            )),
            sdl2::event::Event::MouseButtonUp {
                mouse_btn, x, y, ..
            } => Event::Window(WindowEvent::MouseButtonReleased(
                *mouse_btn, *x as u32, *y as u32,
            )),
            sdl2::event::Event::MouseWheel {
                direction, x, y, ..
            } => Event::Window(WindowEvent::MouseWheelMoved(*direction, *x, *y)),
            sdl2::event::Event::KeyDown {
                keycode, keymod, ..
            } => {
                use sdl2::keyboard::Mod;
                let key = Key {
                    code: keycode.unwrap(),
                    alt: keymod.intersects(Mod::LALTMOD | Mod::RALTMOD),
                    ctrl: keymod.intersects(Mod::LCTRLMOD | Mod::RCTRLMOD),
                    meta: keymod.intersects(Mod::LGUIMOD | Mod::RGUIMOD),
                    shift: keymod.intersects(Mod::LSHIFTMOD | Mod::RSHIFTMOD),
                };
                Event::Window(WindowEvent::KeyPressed(key))
            }
            sdl2::event::Event::KeyUp {
                keycode, keymod, ..
            } => {
                use sdl2::keyboard::Mod;
                let key = Key {
                    code: keycode.unwrap(),
                    alt: keymod.intersects(Mod::LALTMOD | Mod::RALTMOD),
                    ctrl: keymod.intersects(Mod::LCTRLMOD | Mod::RCTRLMOD),
                    meta: keymod.intersects(Mod::LGUIMOD | Mod::RGUIMOD),
                    shift: keymod.intersects(Mod::LSHIFTMOD | Mod::RSHIFTMOD),
                };
                Event::Window(WindowEvent::KeyReleased(key))
            }
            sdl2::event::Event::TextInput{text, ..} => {
                Event::Window(WindowEvent::TextEntered(text.clone()))
            } 
            sdl2::event::Event::JoyButtonDown {
                which, button_idx, ..
            } => Event::Joystick(JoystickEvent::ButtonPressed {
                id: *which,
                button: *button_idx,
            }),
            sdl2::event::Event::JoyButtonUp {
                which, button_idx, ..
            } => Event::Joystick(JoystickEvent::ButtonReleased {
                id: *which,
                button: *button_idx,
            }),
            sdl2::event::Event::JoyAxisMotion {
                which,
                axis_idx,
                value,
                ..
            } => Event::Joystick(JoystickEvent::AxisMotion {
                id: *which,
                axis_id: *axis_idx,
                value: *value,
            }),
            sdl2::event::Event::JoyHatMotion { which, state, .. } => {
                Event::Joystick(JoystickEvent::HatMotion {
                    id: *which,
                    state: *state,
                })
            }
            _ => Event::None,
        }
    }

    pub fn swap_buffers(&self) {
        self.sdl2_window.gl_swap_window();
    }

    pub fn set_vsync(&self, value: bool) {
        if value {
            self.sdl2_context
                .video()
                .unwrap()
                .gl_set_swap_interval(sdl2::video::SwapInterval::VSync)
                .unwrap();
        } else {
            self.sdl2_context
                .video()
                .unwrap()
                .gl_set_swap_interval(sdl2::video::SwapInterval::Immediate)
                .unwrap();
        }
    }

    pub fn set_fullscreen(&mut self, full: bool) {
        if full {
            self.sdl2_window
                .set_fullscreen(FullscreenType::Desktop)
                .unwrap();
        } else {
            self.sdl2_window
                .set_fullscreen(FullscreenType::Off)
                .unwrap();
        }
    }

    pub fn is_fullscreen(&self) -> bool {
        if self.sdl2_window.fullscreen_state() == FullscreenType::Desktop {
            true
        } else {
            false
        }
    }

    pub fn size(&self) -> Size2D {
        if self.is_fullscreen() {
            let size = self.sdl2_window.size();
            Size2D::new(size.0, size.1)
        } else {
            self.preserved_size
        }
    }
}
