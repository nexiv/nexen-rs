/*************************************************************************/
/*  profile.rs                                                           */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use std::time::{Instant, Duration};
use log::debug;

pub struct Profiler<'a> {
    start_time: Instant,
    duration: Duration,
    tag: &'a str,
}

impl<'a> Profiler<'a> {
    pub fn new(tag: &'a str) -> Self {
        Profiler {
            start_time: Instant::now(),
            duration: Duration::new(0, 0),
            tag
        }
    }

    pub fn reset(&mut self) {
        self.start_time = Instant::now();
    }

    pub fn collect(&mut self) {
        self.duration += Instant::now() - self.start_time;
    }

    pub fn stamp_collected(&self) {
        debug!("Profiler: {}: {}s", self.tag, self.duration.as_secs_f32());
    }

    pub fn stamp(&mut self) {
        self.collect();
        debug!("Profiler: {}: {}s", self.tag, self.duration.as_secs_f32());
    }
}
