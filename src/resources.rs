/*************************************************************************/
/*  resources.rs                                                         */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
pub mod resource_storage;
pub use resource_storage::ResourceStorage as ResourceStorage;
pub use resource_storage::AnyResourceStorage as AnyResourceStorage;

pub mod resource_manager;
pub use resource_manager::ResourceManager as ResourceManager;

pub trait Resource: std::any::Any + Sized + 'static + Send + Sync {}

pub trait ResourceLoader<R: Resource>: Default + Sized + 'static {
    fn from_file(&self, path: &str) -> R;
}
