/*************************************************************************/
/*  resources_manager.rs                                                 */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use super::{Resource, ResourceStorage, AnyResourceStorage};
use std::{
    collections::HashMap,
    any::TypeId,
};

pub struct ResourceManager {
    storages: HashMap<TypeId, Box<dyn AnyResourceStorage>>,
}

impl ResourceManager {
    pub fn new() -> Self {
        ResourceManager {
            storages: HashMap::new(),
        }
    }

    pub fn get_storage_for<R: Resource>(&mut self) -> &mut ResourceStorage<R> {
        self.storages.entry(TypeId::of::<ResourceStorage<R>>())
            .or_insert_with(|| {
                Box::new(ResourceStorage::<R>::new())
            })
            .as_any_mut()
            .downcast_mut::<ResourceStorage<R>>()
            .unwrap()
    }
}
