/*************************************************************************/
/*  resources_storage.rs                                                 */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use super::{Resource, ResourceLoader};
use std::{
    collections::HashMap,
    sync::Arc,
    any::Any,
};
use atomic_refcell::AtomicRefCell;

pub trait AnyResourceStorage: Any + Send + Sync {
    fn as_any(&self) -> &(dyn Any + Send + Sync);
    fn as_any_mut(&mut self) -> &mut (dyn Any + Send + Sync);
}

pub struct ResourceStorage<R: Resource> {
    //loader: L,
    cache: HashMap<String, Arc<AtomicRefCell<R>>>,
}

impl<R: Resource> ResourceStorage<R> {
    pub fn new() -> Self {
        ResourceStorage {
            //loader: L::default(),
            cache: HashMap::new(),
        }
    }

    pub fn get<L: ResourceLoader<R>>(&mut self, path: &str) -> Result<Arc<AtomicRefCell<R>>, String> {
        let owned_path = path.to_string();
        self.cache
            .get(&owned_path)
            .cloned()
            .map_or_else(|| {
                let resource = Arc::new(AtomicRefCell::new(L::default().from_file(&path)));
                self.cache.insert(owned_path, resource.clone());
                Ok(resource)
            }, Ok)
    }
}

impl<R: Resource> AnyResourceStorage for ResourceStorage<R> {
    fn as_any(&self) -> &(dyn Any + Send + Sync) {
        self
    }

    fn as_any_mut(&mut self) -> &mut (dyn Any + Send + Sync) {
        self
    }
}
