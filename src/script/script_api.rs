/*************************************************************************/
/*  script_api.rs                                                        */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::animation::AnimationComponent;
use crate::audio::{SoundSourceComponent, SoundSourceStatus};
use crate::common::FloatRect;
use crate::ecs::{
    component_storage::Ref,
    components::NavigationComponent,
    Entity,
    World
};
use crate::graphics::{
    render_queue::RenderCommand,
    Camera2DComponent,
    Sprite2DComponent,
    Polygon2DComponent,
    TransformComponent,
    Color,
    Renderer2D,
};
use crate::physics::{PhysicsEvent, RaycastComponent, RigidBodyComponent};
use crate::platform::{joystick::*, WindowEvent};
use crate::time::TimerComponent;
use atomic_refcell::AtomicRefCell;
use std::sync::Arc;

pub fn register_constructors(lua_context: &rlua::Context) {
    let color_new = lua_context
        .create_function(|_, (r, g, b, a): (u8, u8, u8, u8)| Ok(Color::new(r, g, b, a)))
        .unwrap();

    let vec2_new = lua_context
        .create_function(|_, (x, y): (f32, f32)| {
            Ok(SVec2 { vec: glm::vec2(x, y) })
        })
        .unwrap();

    lua_context.globals().set("color", color_new).unwrap();
    lua_context.globals().set("vec2", vec2_new).unwrap();
}

#[derive(Copy, Clone)]
pub struct SVec2 {
    vec: glm::Vec2,
}

impl rlua::UserData for SVec2 {
    fn add_methods<'lua, M: rlua::UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method("x", |_, this, ()| Ok(this.vec.x));

        methods.add_method("y", |_, this, ()| Ok(this.vec.y));

        methods.add_method_mut("set_x", |_, this, x: f32| {
            this.vec.x = x;
            Ok(())
        });

        methods.add_method_mut("set_y", |_, this, y: f32| {
            this.vec.y = y;
            Ok(())
        });

        methods.add_method("normalized", |_, this, ()| {
            let normalized = glm::normalize(&this.vec);
            Ok(SVec2{vec: glm::vec2(normalized.x, normalized.y)})
        });

        methods.add_method("magnitude", |_, this, ()| {
            let mag_squared = this.vec.x * this.vec.x + this.vec.y * this.vec.y;
            Ok(mag_squared.sqrt())
        });

        methods.add_meta_function(rlua::MetaMethod::Add, |_, (vec1, vec2): (SVec2, SVec2)| {
            Ok(SVec2{vec: vec1.vec + vec2.vec})
        });

        methods.add_meta_function(rlua::MetaMethod::Sub, |_, (vec1, vec2): (SVec2, SVec2)| {
            Ok(SVec2{vec: vec1.vec - vec2.vec})
        });
    }
}

/*impl<'lua> rlua::FromLua<'lua> for SVec2 {
    fn from_lua(lua_value: rlua::Value<'lua>, _lua: rlua::Context<'lua>) -> rlua::Result<Self> {
        match lua_value {
            rlua::Value::UserData(user_data) => {
                Ok(user_data.get_user_value::<SVec2>().unwrap())
            }
            _ => Err(rlua::Error::FromLuaConversionError {
                from: "LuaVec",
                to: "userdata",
                message: Some("Error converting from SVec2".to_owned()),
            })
        }
    }
}*/

impl rlua::UserData for Color {
    fn add_methods<'lua, M: rlua::UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method("r", |_, this, ()| Ok(this.r));

        methods.add_method("g", |_, this, ()| Ok(this.g));

        methods.add_method("b", |_, this, ()| Ok(this.b));

        methods.add_method("a", |_, this, ()| Ok(this.b));
    }
}

impl rlua::UserData for FloatRect {
    fn add_methods<'lua, M: rlua::UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method("x", |_, this, ()| Ok(this.x));

        methods.add_method("y", |_, this, ()| Ok(this.y));

        methods.add_method("w", |_, this, ()| Ok(this.w));

        methods.add_method("h", |_, this, ()| Ok(this.h));
    }
}

impl rlua::UserData for WindowEvent {
    fn add_methods<'lua, M: rlua::UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method("type", |_, this, ()| match this {
            WindowEvent::MouseMoved(..) => Ok("MouseMoved".to_string()),
            WindowEvent::MouseButtonPressed(..) => Ok("MouseButtonPressed".to_string()),
            WindowEvent::MouseButtonReleased(..) => Ok("MouseButtonReleased".to_string()),
            WindowEvent::MouseWheelMoved(..) => Ok("MouseWheelMoved".to_string()),
            WindowEvent::KeyPressed(..) => Ok("KeyPressed".to_string()),
            WindowEvent::KeyReleased(..) => Ok("KeyReleased".to_string()),
            WindowEvent::TextEntered(..) => Ok("TextEntered".to_string()),
            WindowEvent::Quit => Ok("Undefined".to_string()),
        });

        methods.add_method("key_released", |_, this, ()| match this {
            WindowEvent::KeyReleased(key) => Ok(key.code.to_string()),
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method("key_pressed", |_, this, ()| match this {
            WindowEvent::KeyPressed(key) => Ok(key.code.to_string()),
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });
    }
}

impl rlua::UserData for JoystickEvent {
    fn add_methods<'lua, M: rlua::UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method("type", |_, this, ()| {
            match this {
                JoystickEvent::ButtonPressed { .. } => Ok("ButtonPressed".to_string()),
                JoystickEvent::ButtonReleased { .. } => Ok("ButtonReleased".to_string()),
                JoystickEvent::AxisMotion { .. } => Ok("AxisMotion".to_string()),
                JoystickEvent::HatMotion { .. } => Ok("HatMotion".to_string()),
                //_ => Err(rlua::Error::UserDataTypeMismatch),
            }
        });

        methods.add_method("button_pressed", |ctx, this, ()| match this {
            JoystickEvent::ButtonPressed { id, button } => {
                let event = ctx.create_table()?;
                event.set("id", *id)?;
                event.set("button", *button)?;
                Ok(event)
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method("button_released", |ctx, this, ()| match this {
            JoystickEvent::ButtonReleased { id, button } => {
                let event = ctx.create_table()?;
                event.set("id", *id)?;
                event.set("button", *button)?;
                Ok(event)
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method("hat_motion", |ctx, this, ()| match this {
            JoystickEvent::HatMotion { id, state } => {
                let event = ctx.create_table()?;
                event.set("id", *id)?;
                event.set("state", hat_state_to_string(*state))?;
                Ok(event)
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });
    }
}

impl rlua::UserData for PhysicsEvent {
    fn add_methods<'lua, M: rlua::UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method("type", |_, this, ()| match this {
            PhysicsEvent::ContactStart(..) => Ok("ContactStart".to_string()),
            PhysicsEvent::ContactEnd(..) => Ok("ContactEnd".to_string()),
            PhysicsEvent::ProximityWithin(..) => Ok("ProximityWithin".to_string()),
            PhysicsEvent::ProximityDisjoint(..) => Ok("ProximityDisjoint".to_string()),
        });

        methods.add_method("contact_start", |_, this, ()| match this {
            PhysicsEvent::ContactStart(ent1, ent2) => Ok((*ent1, *ent2)),
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method("contact_end", |_, this, ()| match this {
            PhysicsEvent::ContactEnd(ent1, ent2) => Ok((*ent1, *ent2)),
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method("proximity_within", |_, this, ()| match this {
            PhysicsEvent::ProximityWithin(ent1, ent2) => Ok((*ent1, *ent2)),
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method("proximity_disjoint", |_, this, ()| match this {
            PhysicsEvent::ProximityDisjoint(ent1, ent2) => Ok((*ent1, *ent2)),
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });
    }
}

impl rlua::UserData for &mut Renderer2D {
    fn add_methods<'lua, M: rlua::UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method_mut(
            "draw_text",
            |_, this, (text, x, y, size, color): (String, f32, f32, u32, Color)| {
                this.push_render_command(
                    RenderCommand::DrawTextDefaultFont(
                        glm::translation(&glm::vec3(x, y, 0.0)),
                        text,
                        size,
                        color,
                    ),
                    0,
                    127,
                );
                Ok(())
            },
        );

        methods.add_method_mut("set_clear_color", |_, this, color: Color| {
            this.set_clear_color(&color);
            Ok(())
        });

        /*methods.add_method("y", |_, this, ()| {
            Ok(())
        });*/
    }
}

impl rlua::UserData for Entity {}

pub struct SWorld(pub Arc<AtomicRefCell<World>>);

impl rlua::UserData for SWorld {
    fn add_methods<'lua, M: rlua::UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method("make_entity", |_, this, entity: Entity| {
            Ok(SEntity {
                entity,
                world: this.0.clone(),
            })
        });
    }
}

pub struct SEntity {
    pub entity: Entity,
    pub world: Arc<AtomicRefCell<World>>,
}

impl rlua::UserData for SEntity {
    fn add_methods<'lua, M: rlua::UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method("id", |_, this, ()| Ok(this.entity.id));

        methods.add_method(
            "get_component",
            |_, this, component_type: String| match component_type.as_str() {
                "RigidBody" => Ok(SGenericComponent::RigidBody(
                    this.world
                        .borrow()
                        .get_component_ref::<RigidBodyComponent>(this.entity),
                )),
                "Sprite2D" => Ok(SGenericComponent::Sprite2D(
                    this.world
                        .borrow()
                        .get_component_ref::<Sprite2DComponent>(this.entity),
                )),
                "Polygon2D" => Ok(SGenericComponent::Polygon2D(
                    this.world
                        .borrow()
                        .get_component_ref::<Polygon2DComponent>(this.entity),
                )),
                "Camera2D" => Ok(SGenericComponent::Camera2D(
                    this.world
                        .borrow()
                        .get_component_ref::<Camera2DComponent>(this.entity),
                )),
                "Transform" => Ok(SGenericComponent::Transform(
                    this.world
                        .borrow()
                        .get_component_ref::<TransformComponent>(this.entity),
                )),
                "Animation" => Ok(SGenericComponent::Animation(
                    this.world
                        .borrow()
                        .get_component_ref::<AnimationComponent>(this.entity),
                )),
                "SoundSource" => Ok(SGenericComponent::SoundSource(
                    this.world
                        .borrow()
                        .get_component_ref::<SoundSourceComponent>(this.entity),
                )),
                "Raycast" => Ok(SGenericComponent::Raycast(
                    this.world
                        .borrow()
                        .get_component_ref::<RaycastComponent>(this.entity),
                )),
                "Timer" => Ok(SGenericComponent::Timer(
                    this.world
                        .borrow()
                        .get_component_ref::<TimerComponent>(this.entity),
                )),
                "Navigation" => Ok(SGenericComponent::Navigation(
                    this.world
                        .borrow()
                        .get_component_ref::<NavigationComponent>(this.entity),
                )),
                _ => Err(rlua::Error::MismatchedRegistryKey),
            },
        );

        methods.add_method("get_parent", |_, this, ()| {
            if let Some(parent) = this.world.borrow().entity_parent(this.entity) {
                Ok(parent)
            } else {
                Ok(Entity::null())
            }
        });

        methods.add_method("get_tags", |_, this, ()| {
            if let Some(tags) = this.world.borrow().entity_tags(this.entity) {
                Ok(tags)
            } else {
                Ok(Vec::new())
            }
        });

        methods.add_method("has_tag", |_, this, in_tag: String| {
            if let Some(tags) = this.world.borrow().entity_tags(this.entity) {
                for tag in tags {
                    if tag == in_tag {
                        return Ok(true);
                    }
                }
            }
            Ok(false)
        });

        methods.add_method("get_child_by_id", |_, this, id: usize| {
            let children = this.world.borrow().entity_children(this.entity);
            Ok(SEntity {
                entity: children[id],
                world: this.world.clone(),
            })
        });

        methods.add_method("get_first_child_by_tag", |_, this, tag: String| {
            if let Some(children) = this
                .world
                .borrow()
                .entity_children_by_tag(this.entity, tag.as_str())
            {
                Ok(Some(SEntity{
                    entity: children[0],
                    world: this.world.clone(),
                }))
            } else {
                Ok(None)
            }
        });

        methods.add_method("get_children_by_tag", |_, this, tag: String| {
            if let Some(children) = this
                .world
                .borrow()
                .entity_children_by_tag(this.entity, tag.as_str())
            {
                let mut return_vec = Vec::with_capacity(children.len());
                for child in children {
                    return_vec.push(SEntity{
                        entity: child,
                        world: this.world.clone(),
                    });
                }
                Ok(return_vec)
            } else {
                Ok(Vec::new())
            }
        });
    }
}

#[derive(Clone)]
pub enum SGenericComponent {
    Transform(Option<Ref<TransformComponent>>),
    RigidBody(Option<Ref<RigidBodyComponent>>),
    Sprite2D(Option<Ref<Sprite2DComponent>>),
    Polygon2D(Option<Ref<Polygon2DComponent>>),
    Camera2D(Option<Ref<Camera2DComponent>>),
    Animation(Option<Ref<AnimationComponent>>),
    SoundSource(Option<Ref<SoundSourceComponent>>),
    Raycast(Option<Ref<RaycastComponent>>),
    Timer(Option<Ref<TimerComponent>>),
    Navigation(Option<Ref<NavigationComponent>>),
}

impl rlua::UserData for SGenericComponent {
    fn add_methods<'lua, M: rlua::UserDataMethods<'lua, Self>>(methods: &mut M) {
        /* Timer */
        methods.add_method_mut("run", |_, this, ()| match this {
            SGenericComponent::Timer(timer) => {
                timer.as_ref().unwrap().write().run();
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method_mut("pause", |_, this, ()| match this {
            SGenericComponent::Timer(timer) => {
                timer.as_ref().unwrap().write().pause();
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method("finished", |_, this, ()| match this {
            SGenericComponent::Timer(timer) => {
                let finished = timer.as_ref().unwrap().write().finished();
                Ok(finished)
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        /* Transform */
        methods.add_method_mut("set_scale", |_, this, (x, y): (f32, f32)| match this {
            SGenericComponent::Transform(transform) => {
                transform
                    .as_ref()
                    .unwrap()
                    .write()
                    .set_scale(glm::vec2(x, y));
                Ok(this.clone())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method_mut("scale", |_, this, (x, y): (f32, f32)| match this {
            SGenericComponent::Transform(transform) => {
                transform.as_ref().unwrap().write().scale(glm::vec2(x, y));
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method_mut("set_position", |_, this, (x, y): (f32, f32)| match this {
            SGenericComponent::Transform(transform) => {
                transform
                    .as_ref()
                    .unwrap()
                    .write()
                    .set_translation(glm::vec2(x, y));
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method_mut("move", |_, this, (x, y): (f32, f32)| match this {
            SGenericComponent::Transform(transform) => {
                transform
                    .as_ref()
                    .unwrap()
                    .write()
                    .translate(glm::vec2(x, y));
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method_mut("set_rotation", |_, this, rotation: f32| match this {
            SGenericComponent::Transform(transform) => {
                transform.as_ref().unwrap().write().set_rotation(rotation);
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method_mut("rotate", |_, this, rotation: f32| match this {
            SGenericComponent::Transform(transform) => {
                transform.as_ref().unwrap().write().rotate(rotation);
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method_mut("get_position", |_, this, ()| match this {
            SGenericComponent::Transform(transform) => {
                let pos = transform.as_ref().unwrap().write().translation;
                Ok((pos.x, pos.y))
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method_mut("get_scale", |_, this, ()| match this {
            SGenericComponent::Transform(transform) => {
                let scale = transform.as_ref().unwrap().write().scale;
                Ok((scale.x, scale.y))
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        /* Sprite2DComponent */

        methods.add_method_mut("set_rect_index", |_, this, index: u32| match this {
            SGenericComponent::Sprite2D(sprite) => {
                sprite.as_ref().unwrap().write().rect_index = index;
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        /* Sprite2DComponent && Polygon2DComponent */

        methods.add_method_mut("set_color", |_, this, color: Color| match this {
            SGenericComponent::Sprite2D(sprite) => {
                sprite.as_ref().unwrap().write().color = color;
                Ok(this.clone())
            }
            SGenericComponent::Polygon2D(polygon) => {
                polygon.as_ref().unwrap().write().color = color;
                Ok(this.clone())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method_mut("set_border_color", |_, this, color: Color| match this {
            SGenericComponent::Polygon2D(polygon) => {
                polygon.as_ref().unwrap().write().border_color = color;
                Ok(this.clone())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        /* Camera2DComponent */

        methods.add_method_mut("limits", |_, this, ()| match this {
            SGenericComponent::Camera2D(camera) => {
                if let Some(limits) = camera.as_ref().unwrap().read().limits {
                    Ok(limits)
                } else {
                    Ok(FloatRect::zero())
                }
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        /* RigidBodyComponent */

        methods.add_method("linear_impulse", |_, this, (x, y): (f32, f32)| match this {
            SGenericComponent::RigidBody(body) => {
                body.as_ref()
                    .unwrap()
                    .write()
                    .linear_impulse(&glm::vec2(x, y));
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method_mut(
            "linear_impulse_from",
            |_, this, (x, y, px, py): (f32, f32, f32, f32)| match this {
                SGenericComponent::RigidBody(body) => {
                    body.as_ref().unwrap().write().linear_impulse_from(
                        &glm::vec2(x, y),
                        &nphysics2d::math::Point::new(px, py),
                    );
                    Ok(())
                }
                _ => Err(rlua::Error::UserDataTypeMismatch),
            },
        );

        methods.add_method_mut("angular_impulse", |_, this, angle: f32| match this {
            SGenericComponent::RigidBody(body) => {
                body.as_ref().unwrap().write().angular_impulse(angle);
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method_mut("linear_force", |_, this, (x, y): (f32, f32)| match this {
            SGenericComponent::RigidBody(body) => {
                body.as_ref()
                    .unwrap()
                    .write()
                    .linear_force(&glm::vec2(x, y));
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method_mut("angular_force", |_, this, angle: f32| match this {
            SGenericComponent::RigidBody(body) => {
                body.as_ref().unwrap().write().angular_force(angle);
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method("linear_velocity", |_, this, ()| match this {
            SGenericComponent::RigidBody(body) => {
                let vec = body.as_ref().unwrap().write().velocity.linear;
                Ok(SVec2{ vec })
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method(
            "set_linear_velocity",
            |_, this, (x_vel, y_vel): (f32, f32)| match this {
                SGenericComponent::RigidBody(body) => {
                    body.as_ref().unwrap().write().velocity.linear = glm::vec2(x_vel, y_vel);
                    Ok(())
                }
                _ => Err(rlua::Error::UserDataTypeMismatch),
            },
        );

        methods.add_method("angular_velocity", |_, this, ()| match this {
            SGenericComponent::RigidBody(body) => {
                Ok(body.as_ref().unwrap().write().velocity.angular)
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method("set_angular_velocity", |_, this, vel: f32| match this {
            SGenericComponent::RigidBody(body) => {
                body.as_ref().unwrap().write().velocity.angular = vel;
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method("set_angular_damping", |_, this, vel: f32| match this {
            SGenericComponent::RigidBody(body) => {
                body.as_ref().unwrap().write().angular_inertia = vel;
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method("set_sensor", |_, this, b: bool| match this {
            SGenericComponent::RigidBody(body) => {
                body.as_ref().unwrap().write().set_sensor(b);
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method("set_friction", |_, this, friction: f32| match this {
            SGenericComponent::RigidBody(body) => {
                body.as_ref().unwrap().write().friction = friction;
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        // RigidBody & Polygon2D

        methods.add_method("set_mesh", |_, this, mesh: Vec::<SVec2>| match this {
            SGenericComponent::RigidBody(body) => {
                let mut rust_vec: Vec<glm::Vec2> = Vec::with_capacity(mesh.len());
                for vec2 in mesh {
                    rust_vec.push(vec2.vec);
                }
                body.as_ref().unwrap().write().set_mesh(rust_vec);
                Ok(())
            }
            SGenericComponent::Polygon2D(polygon) => {
                let mut rust_vec: Vec<glm::Vec2> = Vec::with_capacity(mesh.len());
                for vec2 in mesh {
                    rust_vec.push(vec2.vec);
                }
                polygon.as_ref().unwrap().write().shape = crate::graphics::Polygon2DShape::Convex(rust_vec);
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        /* AnimationComponent && SoundSourceComponent */

        methods.add_method("play", |_, this, ()| match this {
            SGenericComponent::Animation(animation) => {
                animation.as_ref().unwrap().write().play();
                Ok(())
            }
            SGenericComponent::SoundSource(source) => {
                source.as_ref().unwrap().write().status = SoundSourceStatus::Playing;
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method("stop", |_, this, ()| match this {
            SGenericComponent::Animation(animation) => {
                animation.as_ref().unwrap().write().stop();
                Ok(())
            }
            SGenericComponent::SoundSource(source) => {
                source.as_ref().unwrap().write().status = SoundSourceStatus::Stopped;
                Ok(())
            }
            SGenericComponent::Timer(timer) => {
                timer.as_ref().unwrap().write().stop();
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method("playing", |_, this, ()| match this {
            SGenericComponent::Animation(animation) => {
                Ok(animation.as_ref().unwrap().write().playing())
            }
            SGenericComponent::SoundSource(source) => {
                Ok(source.as_ref().unwrap().write().status == SoundSourceStatus::Playing)
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method("reverse", |_, this, reverse: bool| match this {
            SGenericComponent::Animation(animation) => {
                animation.as_ref().unwrap().write().reverse = reverse;
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method("set_running_time", |_, this, time: f32| match this {
            SGenericComponent::Animation(animation) => {
                animation.as_ref().unwrap().write().set_running_time(time);
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method("set_length", |_, this, length: f32| match this {
            SGenericComponent::Animation(animation) => {
                animation.as_ref().unwrap().write().length = length;
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method("set_repeat", |_, this, repeat: bool| match this {
            SGenericComponent::Animation(animation) => {
                animation.as_ref().unwrap().write().repeat = repeat;
                Ok(())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method("elapsed", |_, this, ()| match this {
            SGenericComponent::Animation(animation) => {
                Ok(animation.as_ref().unwrap().write().elapsed())
            }
            SGenericComponent::Timer(timer) => {
                Ok(timer.as_ref().unwrap().write().elapsed())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        /* RaycastComponent */

        methods.add_method("colliding_entities", |_, this, ()| match this {
            SGenericComponent::Raycast(raycast) => {
                let coll_entities = raycast
                    .as_ref()
                    .unwrap()
                    .write()
                    .colliding_entities()
                    .to_vec();
                Ok(coll_entities)
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        /* NavigationComponent */

        methods.add_method("get", |_, this, index: usize| match this {
            SGenericComponent::Navigation(navigation) => {
                let point = navigation
                    .as_ref()
                    .unwrap()
                    .write()
                    .path[index];

                Ok((point.x, point.y))
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });

        methods.add_method("length", |_, this, ()| match this {
            SGenericComponent::Navigation(navigation) => {
                Ok(navigation
                    .as_ref()
                    .unwrap()
                    .write()
                    .path.len())
            }
            _ => Err(rlua::Error::UserDataTypeMismatch),
        });
    }
}
