/*************************************************************************/
/*  script_component.rs\                                                 */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::ecs::Component;

pub const PROCESS_UPDATE: u8 = 0x01;
pub const PROCESS_DRAW: u8 = 0x02;
pub const PROCESS_EVENT: u8 = 0x04;
pub const PROCESS_MESSAGE: u8 = 0x08;

pub struct ScriptComponent {
    pub path: String,
    pub(super) loaded: bool,
    pub(super) error: bool,
    pub(super) process_flags: u8,
}

impl ScriptComponent {
    pub fn new() -> Self {
        ScriptComponent {
            path: String::new(),
            loaded: false,
            error: false,
            process_flags: 0,
        }
    }

    pub fn from_file(path: &str) -> Self {
        ScriptComponent {
            path: path.to_string(),
            loaded: false,
            error: false,
            process_flags: 0,
        }
    }
}

impl Component for ScriptComponent {}