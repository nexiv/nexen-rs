/*************************************************************************/
/*  script_context.rs                                                    */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::core::{DataBundle, InitArgs, event::Event};
use crate::ecs::System;
use crate::graphics::Renderer2D;
use super::{ScriptComponent, script_component};

use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

use log::error;

pub struct ScriptContext {
    lua_state: rlua::Lua,
    //registry: Vec::<u64>,
}

impl Default for ScriptContext {
    fn default() -> Self {
        ScriptContext {
            lua_state: rlua::Lua::new(),
            //registry: Vec::new(),
        }
    }
}

impl ScriptContext {
    fn load_script_functions(&self, lua_ctx: &rlua::Context, component: &mut ScriptComponent) -> Result<(), rlua::Error> {
        let mut hasher = DefaultHasher::new();
        component.path.hash(&mut hasher);
        let script_hash = hasher.finish();

        let globals = lua_ctx.globals();
        if let Ok(init_func) = globals.get::<_, rlua::Function>("init") {
            globals.set(format!("init_{}", script_hash).as_str(), init_func)?;
            globals.set("init", rlua::Nil).unwrap();
        }

        if let Ok(update_func) = globals.get::<_, rlua::Function>("update") {
            globals.set(format!("update_{}", script_hash).as_str(), update_func)?;
            globals.set("update", rlua::Nil).unwrap();
            component.process_flags |= script_component::PROCESS_UPDATE;
        }

        if let Ok(draw_func) = globals.get::<_, rlua::Function>("draw") {
            globals.set(format!("draw_{}", script_hash).as_str(), draw_func)?;
            globals.set("draw", rlua::Nil).unwrap();
            component.process_flags |= script_component::PROCESS_DRAW;
        }

        if let Ok(event_func) = globals.get::<_, rlua::Function>("handle_event") {
            globals.set(format!("handle_event_{}", script_hash).as_str(), event_func)?;
            globals.set("handle_event", rlua::Nil).unwrap();
            component.process_flags |= script_component::PROCESS_EVENT;
        }

        if let Ok(on_msg_func) = globals.get::<_, rlua::Function>("on_message") {
            globals.set(format!("on_message_{}", script_hash).as_str(), on_msg_func)?;
            globals.set("on_message", rlua::Nil).unwrap();
            component.process_flags |= script_component::PROCESS_MESSAGE;
        }

        Ok(())
    }
}

impl System for ScriptContext {
    fn init(&mut self, _bundle: &mut DataBundle,  _args: &InitArgs) {

        self.lua_state.context(|lua_ctx| {

            super::script_api::register_constructors(&lua_ctx);

            lua_ctx.globals().set("messages", lua_ctx.create_table().unwrap()).unwrap();

            let post_message = lua_ctx.create_function(|lua_ctx, (message_id, message) : (String, rlua::Table)| {
                let m = lua_ctx.create_table()?;
                m.set(1, message_id)?;
                m.set(2, message)?;

                let messages: rlua::Table = lua_ctx.globals().get("messages")?;
                messages.set(messages.len()? + 1, m)?;
                Ok(())
            }).unwrap();
            lua_ctx.globals().set("post_message", post_message).unwrap();
        });
    }

    fn update(&mut self, bundle: &DataBundle, delta: f32) {
        let world = bundle.world();
        let entities = world.entities();
        let scripts = world.component_storage::<ScriptComponent>();

        for entity in entities {
            if let Some(mut script) = scripts.write(*entity) {
                if !script.loaded && !script.error {
                    self.lua_state.context(|lua_ctx| {
                        if let Ok(contents) = std::fs::read_to_string(&script.path) {
                            match lua_ctx.load(&contents).exec() {
                                Ok(_) => {
                                    match self.load_script_functions(&lua_ctx, &mut script) {
                                        Ok(_) => {
                                            script.loaded = true;

                                            let mut hasher = DefaultHasher::new();
                                            script.path.hash(&mut hasher);
                                            let script_hash = hasher.finish();

                                            let globals = lua_ctx.globals();

                                            let local_data = lua_ctx.create_table().unwrap();
                                            globals.set(format!("local_{}_{}", script_hash, entity.id), local_data)
                                            .expect("Could not create local data");

                                            if let Ok(init_func) = globals.get::<_, rlua::Function>(format!("init_{}", script_hash).as_str()) {
                                                lua_ctx.scope(|scope| {
                                                    let sentity = super::script_api::SEntity {
                                                        entity: *entity,
                                                        world: bundle.world_shared(),
                                                    };
                                                    let sworld = super::script_api::SWorld(
                                                        bundle.world_shared()
                                                    );
                                                    let local_data = globals.get::<_, rlua::Table>(format!("local_{}_{}",  script_hash, entity.id)).unwrap();
                                                    let this = scope.create_nonstatic_userdata(sentity).unwrap();
                                                    let world = scope.create_nonstatic_userdata(sworld).unwrap();
                                                    globals.set("self", this)
                                                        .expect("Could not expose SEntity");
                                                    globals.set("world", world)
                                                        .expect("Could not expose SWorld");
                                                    globals.set("self_data", local_data)
                                                        .expect("Could not expose local data");

                                                    init_func.call::<_, ()>(())
                                                        .expect(format!("Error executing init function: {} : {:?}", script.path, entity).as_str());
                                                });
                                            }
                                        },
                                        Err(error) => {
                                            error!(
                                                "Failed to load script functions: {} for entity {}\n\t{}",
                                                script.path,
                                                entity.id,
                                                error
                                            );
                                            script.error = true;
                                        },
                                    };
                                },
                                Err(error) => {
                                    error!("Failed to compile script: {} {}", script.path, error);
                                    script.error = true;
                                },
                            }
                        } else {
                            error!("Failed to load script: {}", script.path);
                            script.error = true;
                        }
                    });
                } else {
                    if (script.process_flags & script_component::PROCESS_UPDATE) == 0 {
                        continue;
                    }
                    self.lua_state.context(|lua_ctx| {
                        let mut hasher = DefaultHasher::new();
                        script.path.hash(&mut hasher);
                        let script_hash = hasher.finish();

                        let globals = lua_ctx.globals();

                        let mut update_func: Option<rlua::Function> = None;

                        if (script.process_flags & script_component::PROCESS_UPDATE) != 0 {
                            update_func = Some(globals.get(format!("update_{}", script_hash).as_str())
                                .expect(format!("Error loading instance update function: {} : {:?}", script.path, entity).as_str()));
                        }

                        lua_ctx.scope(|scope| {
                            let sentity = super::script_api::SEntity {
                                entity: *entity,
                                world: bundle.world_shared(),
                            };
                            let sworld = super::script_api::SWorld(
                                bundle.world_shared()
                            );

                            let local_data = globals.get::<_, rlua::Table>(format!("local_{}_{}",  script_hash, entity.id)).unwrap();
                            let this = scope.create_nonstatic_userdata(sentity).unwrap();
                            let world = scope.create_nonstatic_userdata(sworld).unwrap();
                            globals.set("self", this)
                                .expect("Could not expose SEntity");
                            globals.set("world", world)
                                .expect("Could not expose SWorld");
                            globals.set("self_data", local_data)
                                .expect("Could not expose local data");

                            if let Some(update_func) = update_func {
                                update_func.call::<_, ()>(delta)
                                .expect(format!("Error executing update function: {} : {:?}", script.path, entity).as_str());
                            }
                        });
                    });
                }
            }
        }

        let mut messages_empty = false;
        self.lua_state.context(|lua_ctx| {
            messages_empty = lua_ctx.globals().get::<_, rlua::Table>("messages")
                .unwrap()
                .len()
                .unwrap() == 0;
        });

        if messages_empty {
            return;
        }

        // Broadcast Messages
        for entity in entities {
            if let Some(script) = scripts.read(*entity) {
                if script.loaded && (script.process_flags & script_component::PROCESS_MESSAGE) != 0 {
                    self.lua_state.context(|lua_ctx| {
                        let mut hasher = DefaultHasher::new();
                        script.path.hash(&mut hasher);
                        let script_hash = hasher.finish();

                        let globals = lua_ctx.globals();

                        let mut on_msg_func: Option<rlua::Function> = None;

                        if (script.process_flags & script_component::PROCESS_MESSAGE) != 0 {
                            on_msg_func = Some(globals.get(format!("on_message_{}", script_hash).as_str())
                            .expect(format!("Error loading instance on_message function: {} : {:?}", script.path, entity.id).as_str()));
                        }

                        lua_ctx.scope(|scope| {
                            let sentity = super::script_api::SEntity {
                                entity: *entity,
                                world: bundle.world_shared(),
                            };
                            let sworld = super::script_api::SWorld(
                                bundle.world_shared()
                            );

                            let local_data = globals.get::<_, rlua::Table>(format!("local_{}_{}",  script_hash, entity.id)).unwrap();
                            let this = scope.create_nonstatic_userdata(sentity).unwrap();
                            let world = scope.create_nonstatic_userdata(sworld).unwrap();
                            globals.set("self", this)
                                .expect("Could not expose SEntity");
                            globals.set("world", world)
                                .expect("Could not expose SWorld");
                            globals.set("self_data", local_data)
                                .expect("Could not expose local data");

                            if let Some(on_msg_func) = on_msg_func {
                                let messages: rlua::Table = globals.get("messages").unwrap();

                                for pair in messages.pairs::<rlua::Value, rlua::Value>() {
                                    let (_, value) = pair.unwrap();
                                    if let rlua::Value::Table(m_table) = value {
                                        let message_id: String = m_table.get(1).unwrap();
                                        let message: rlua::Table = m_table.get(2).unwrap();
                                        on_msg_func.call::<_, ()>((message_id, message))
                                            .expect(format!("Error executing on_message function: {} : {:?}", script.path, entity.id).as_str());
                                    }
                                }
                            }
                        });
                    });
                }
            }
        }

        self.lua_state.context(|lua_ctx| {
            // Clear message queue
            lua_ctx.load("for k,v in pairs(messages) do messages[k]=nil end").exec().unwrap();
        });
    }

    fn render(&self, bundle: &DataBundle, renderer: &mut Renderer2D) {
        let world = bundle.world();
        let entities = world.entities();
        let scripts = world.component_storage::<ScriptComponent>();

        for entity in entities {
            if let Some(script) = scripts.write(*entity) {
                if script.loaded && (script.process_flags & script_component::PROCESS_DRAW) != 0 {
                    self.lua_state.context(|lua_ctx| {
                        let mut hasher = DefaultHasher::new();
                        script.path.hash(&mut hasher);
                        let script_hash = hasher.finish();

                        let globals = lua_ctx.globals();
                        let event_func: rlua::Function = globals.get(format!("draw_{}", script_hash).as_str())
                            .expect(format!("Error loading instance draw function: {} : {:?}", script.path, entity.id).as_str());

                        lua_ctx.scope(|scope| {
                            let this = scope.create_nonstatic_userdata(&mut *renderer).unwrap();
                            globals.set("Renderer", this)
                                .expect("Could not expose Renderer");

                            event_func.call::<_, ()>(())
                                .expect(format!("Error executing draw function: {} : {:?}", script.path, entity.id).as_str());
                        });
                    });
                }
            }
        }
    }

    fn handle_events(&mut self, bundle: &mut DataBundle, event: &Event) {
        let world = bundle.world();
        let entities = world.entities();
        let scripts = world.component_storage::<ScriptComponent>();

        // Hot Reload
        let mut hot_reload = false;
        if let Event::Window(window_event) = event {
            if let crate::platform::WindowEvent::KeyReleased(key) = window_event {
                if key.code == crate::platform::keyboard::Keycode::R && key.ctrl {
                    println!("ok..");
                    hot_reload = true;
                }
            }
        }

        for entity in entities {
            if let Some(mut script) = scripts.write(*entity) {
                if script.loaded && (script.process_flags & script_component::PROCESS_EVENT) != 0 {
                    self.lua_state.context(|lua_ctx| {
                        let mut hasher = DefaultHasher::new();
                        script.path.hash(&mut hasher);
                        let script_hash = hasher.finish();

                        let globals = lua_ctx.globals();
                        let event_func: rlua::Function = globals.get(format!("handle_event_{}", script_hash).as_str())
                            .expect(format!("Error loading instance handle_event function: {} : {:?}", script.path, entity.id).as_str());

                        lua_ctx.scope(|scope| {
                            let sentity = super::script_api::SEntity {
                                entity: *entity,
                                world: bundle.world_shared(),
                            };
                            let sworld = super::script_api::SWorld(
                                bundle.world_shared()
                            );
                            let local_data = globals.get::<_, rlua::Table>(format!("local_{}_{}",  script_hash, entity.id)).unwrap();
                            let this = scope.create_nonstatic_userdata(sentity).unwrap();
                            let world = scope.create_nonstatic_userdata(sworld).unwrap();
                            globals.set("self", this)
                                .expect("Could not expose SEntity");
                            globals.set("world", world)
                                .expect("Could not expose SWorld");
                            globals.set("self_data", local_data)
                                .expect("Could not expose self_data");

                            event_func.call::<_, ()>(event.clone())
                                .expect(format!("Error executing handle_event function: {} : {:?}", script.path, entity.id).as_str());
                        });
                    });
                }

                if hot_reload {
                    script.loaded = false;
                }
            }
        }
    }
}
