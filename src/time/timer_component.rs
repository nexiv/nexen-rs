/*************************************************************************/
/*  timer_component.rs                                                   */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           NEXEN Engine                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2019-2020 NexIV Group.                                  */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
use crate::core::{InitArgs, DataBundle};
use crate::ecs::{Component, System, AsyncSystem};

pub struct TimerComponent {
    pub length: f32,
    elapsed: f32,
    running: bool,
    start_marker: Option<std::time::Instant>,
}

impl TimerComponent {
    pub fn new() -> Self {
        TimerComponent {
            length: 0.0,
            elapsed: 0.0,
            running: false,
            start_marker: None,
        }
    }

    pub fn run(&mut self) {
        self.running = true;
    }

    pub fn pause(&mut self) {
        self.running = false;
    }

    pub fn stop(&mut self) {
        self.running = false;
        self.elapsed = 0.0;
        self.start_marker = None;
    }

    pub fn elapsed(&self) -> f32 {
        self.elapsed
    }

    pub fn finished(&self) -> bool {
        self.elapsed > self.length
    }

    pub fn running(&self) -> bool {
        self.running
    }
}

impl Component for TimerComponent {}

pub struct TimerSystem;

impl Default for TimerSystem {
    fn default() -> Self {
        TimerSystem
    }
}

impl System for TimerSystem {
    fn init(&mut self, _bundle: &mut DataBundle, _args: &InitArgs) {}

    fn update(&mut self, bundle: &DataBundle, _delta: f32) {
        for timer in &mut *bundle.world().component_storage::<TimerComponent>().data_mut() {
            if timer.running {
                if let Some(start_time) = timer.start_marker {
                    timer.elapsed = std::time::Instant::now().duration_since(start_time).as_secs_f32();
                } else {
                    timer.start_marker = Some(std::time::Instant::now());
                }
            }
        }
    }
}

impl AsyncSystem for TimerSystem {}
